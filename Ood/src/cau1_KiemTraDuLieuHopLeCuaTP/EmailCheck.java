package cau1_KiemTraDuLieuHopLeCuaTP;
// Kiểm Tra xem có phải email hay không?
public class EmailCheck implements Validation, Display{
	private boolean check;
	private String inputString;
	
	public EmailCheck() {
		
	}
	public boolean isCheck() {
		return check;
	}
	public void checkInput(String input) {
		this.inputString = input;
		if(input.contains("@")) {
			check = true;
		} else {
			check = false;
		}
		display();
	}
	public void display() {
		System.out.println("Phép Kiểm Tra "+inputString +" có phải là một mail: "+ check);
	}
}
