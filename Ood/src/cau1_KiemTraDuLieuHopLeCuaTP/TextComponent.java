package cau1_KiemTraDuLieuHopLeCuaTP;

import java.util.ArrayList;

public class TextComponent implements Component {
	private ArrayList<Validation> validations;
	private String input;
	
	public String getInput() {
		return input;
	}
	public TextComponent() {
		validations = new ArrayList<>();
		
	}
	public void registerValidation(Validation v) {
		validations.add(v);
	}
	public void removeValidation(Validation v) {
		validations.remove(v);
	}
	public void notifyValidation() {
		for(Validation v: validations) {
			v.checkInput(input);
		}
	}
	public void change() {
		notifyValidation();
	}
	public void inputStringToCheck(String data) {
		this.input = data;
		change();
		boolean checkAll = false;
		this.input = data;
		for(Validation v : validations) {
			if(v.isCheck() == false) {
				checkAll = false;
				break;
			}
			else {
				checkAll = true;
			}
		}
		System.out.println("Kiểm tra: "+checkAll);
	}
	@Override
	public void removevalidation(Validation v) {
		// TODO Auto-generated method stub		
	}
}