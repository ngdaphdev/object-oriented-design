package cau1_KiemTraDuLieuHopLeCuaTP;
// Kiểm Tra có phải là một số?
public class NumberCheck implements Validation,Display {
	private boolean check;
	private String inputString;
	public NumberCheck() {
		
	}
	public boolean isCheck() {
		return check;
	}
	public void checkInput(String input) {
		this.inputString = input;
		try {
			Double.parseDouble(input);
			check = true;
			
		} catch (Exception e) {
			check = false;
		}
		display();
	}
	public void display() {
		System.out.println("Phép kiểm tra " + inputString +" có phải là một số: "+ check);
		
	}
	
}

