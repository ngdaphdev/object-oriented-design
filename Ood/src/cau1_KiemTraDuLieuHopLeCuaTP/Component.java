package cau1_KiemTraDuLieuHopLeCuaTP;

public interface Component {
	public void registerValidation(Validation v);
	public void removevalidation(Validation v);
	public void notifyValidation();
}
