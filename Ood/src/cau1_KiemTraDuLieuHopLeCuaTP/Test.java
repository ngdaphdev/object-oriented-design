package cau1_KiemTraDuLieuHopLeCuaTP;

public class Test {
	public static void main(String[] args) {
		TextComponent text = new TextComponent();
		NumberComponent number = new NumberComponent();
		
		Validation numberCheck = new NumberCheck();
		Validation emailCheck = new EmailCheck();
		Validation phoneNumberCheck = new PhoneNumberCheck();
		Validation length = new Length();
		Validation value = new Value(number);
		
		text.registerValidation(length);
		text.registerValidation(emailCheck);
		text.registerValidation(phoneNumberCheck);
		
		number.registerValidation(numberCheck);
		number.registerValidation(value);
		number.registerValidation(length);
		
		System.out.println("---------------------------");
		text.inputStringToCheck("nguyenphuoc04112002@gmail.com");	
		System.out.println("----------------------------");
		number.inputStringToCheck("1234234.32");
		System.out.println("---------------------------");
		text.inputStringToCheck("0348417665");
	}
}
