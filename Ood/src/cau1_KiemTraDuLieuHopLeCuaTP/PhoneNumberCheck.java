package cau1_KiemTraDuLieuHopLeCuaTP;

// kiểm tra có phải là một số điện thoại?
public class PhoneNumberCheck implements Validation, Display {
	private boolean check;
	private String inputString;

	public PhoneNumberCheck() {
		super();
	}

	public boolean isCheck() {
		return check;
	}

	public void checkInput(String input) {
		this.inputString = input;
		String twoNumber = input.substring(0, 2);
		if (twoNumber.equals("08") || twoNumber.equals("03") || twoNumber.equals("07")
				|| twoNumber.equals("05") && input.length() == 10) {
			check = true;
		} else {
			check = false;
		}
		display();
	}

	public void display() {
		System.out.println("Kiểm Tra " + inputString + " có phải là một số điện thoại:  " + check);
	}

}
