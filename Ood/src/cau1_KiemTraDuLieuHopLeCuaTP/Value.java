package cau1_KiemTraDuLieuHopLeCuaTP;

// Min, Max
public class Value implements Validation, Display {
	private double min = 200;
	private double max = 0;

	public Value(Component numberComponent) {
		numberComponent.registerValidation(this);
	}

	public void checkInput(String input) {
		double number = Double.parseDouble(input);
		if (number > max) {
			max = number;
		}
		if (number < min) {
			min = number;
		}
		display();
	}

	public void display() {
		System.out.println("Giá Trị lớn nhất, Giá Trị nhỏ nhất: " + max + ", " + min);
	}

	public boolean isCheck() {
		return true;
	}
}