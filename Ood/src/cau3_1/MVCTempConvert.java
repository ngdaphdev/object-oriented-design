package cau3_1;

public class MVCTempConvert {
	public static void main(String args[]) {
		TemperatureModel temperature = new TemperatureModel();
		new FarenheitGUI(temperature, 200, 200);
		new GraphGUI(temperature, 100, 250);
	}
}
