package cau3_1;

import java.util.Observable;

@SuppressWarnings("deprecation")
interface Observer {
	void update(Observable t, Object o);
}
