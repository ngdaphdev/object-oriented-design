package cau4_DatTourDuLich;

public class Test {

	public static void main(String[] args) {
		Tour tour1 = new DomesticTour("Núi Bà Đen", "Tây Ninh", 1, 500000);
		System.out.println(tour1);
		tour1 = new SaleTour(tour1, 100.000);
		System.out.println(tour1);
		Tour tour2 = new DomesticTour("Núi Chúa", "Ninh Thuận", 2, 600000);
		System.out.println(tour2);
		tour2 = new SaleTour(tour2, 150.000);
		System.out.println(tour2);

		
		Tour VungTau = new DomesticTour("Hồ Chí Minh", "Vũng Tàu", 2, 1000000);
		System.out.println(VungTau);
		VungTau = new VisitFamilyTour(VungTau);
		System.out.println(VungTau);

		Tour BaDen = new DomesticTour("Bình Phước", "Núi Bà Đen", 2, 125000);
		System.out.println(BaDen);

		Tour France = new OverseasTour("France", "Climb eiffel tower", 1, 3600000,"France",500000);
		System.out.println(France);
		
		
		
	}

}
