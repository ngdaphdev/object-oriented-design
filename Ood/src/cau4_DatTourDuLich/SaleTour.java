package cau4_DatTourDuLich;


public class SaleTour extends TourDecorator {
	private double saleAmount;

	public SaleTour(Tour tour, double saleAmount) {
		super(tour);
		this.saleAmount = saleAmount;
	}

	@Override
	public double price() {
		return tour.getBasePrice() - saleAmount;
	}

	public String toString() {
		return tour.toString() + " saleAmount = " + this.price();
	}

	public static void main(String[] args) {
		Tour tour1 = new DomesticTour("Núi Bà Đen", "Tây Ninh", 1, 500.000);
		Tour tour2 = new DomesticTour("Núi Chúa", "Ninh Thuận", 2, 600.000);
		tour1 = new SaleTour(tour1, 100.000);
		System.out.println(tour1.price());
		tour2 = new SaleTour(tour2, 150.000);
		System.out.println(tour2.price());	
	}
}
