package cau4_DatTourDuLich;

public abstract class Tour {
	private String name;
	private String description;
	private int days;
	private double basePrice;

	public Tour(String name, String description, int days, double basePrice) {
		super();
		this.name = name;
		this.description = description;
		this.days = days;
		this.basePrice = basePrice;
	}

	public Tour() {
	}

	public abstract double price();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	@Override
	public String toString() {
		return "Tour " + getName() + ", description=" + getDescription() + ", days=" + getDays() + ", basePrice=" + getBasePrice();
	}

}