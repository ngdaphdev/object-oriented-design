package cau4_DatTourDuLich;

public class OverseasTour extends Tour{
	private String country;
	private double visaSurtax;

	public OverseasTour(String name, String description, int days, double basePrice, String country, double visaSurtax) {
		super(name, description, days, basePrice);
		this.country = country;
		this.visaSurtax = visaSurtax;
	}

	@Override
	public double price() {
		return visaSurtax + getBasePrice();
	}

	@Override
	public String toString() {
		return super.toString() + " country=" + country + ", visaSurtax=" + visaSurtax ;
	}
}
