package cau4_DatTourDuLich;

public class VisitFamilyTour extends TourDecorator{

	public VisitFamilyTour(Tour tour) {
		super(tour);
	}

	@Override
	public double price() {
		return tour.getBasePrice()*1.05;
	}

	public String toString() {
		return tour.toString() + " VisitFamilyCost = " + this.price();
	}
	
	public static void main(String[] args) {
		Tour tour1 = new DomesticTour("Núi Bà Đen", "Tây Ninh", 1, 500.000);
		Tour tour2 = new DomesticTour("Núi Chúa", "Ninh Thuận", 2, 600.000);
		tour1 = new VisitFamilyTour(tour1);
		System.out.println(tour1);
		tour2 = new VisitFamilyTour(tour2);
		System.out.println(tour2);
	}
}
