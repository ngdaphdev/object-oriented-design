package cau4_DatTourDuLich;


public class DomesticTour extends Tour{

	public DomesticTour(String name, String description, int days, double basePrice) {
		super(name, description, days, basePrice);
	}

	@Override
	public double price() {
		return getBasePrice();
	}
	
	public static void main(String[] args) {
		Tour tour1 = new DomesticTour("Núi Bà Đen", "Tây Ninh", 1, 500.000);
		Tour tour2 = new DomesticTour("Núi Chúa", "Ninh Thuận", 2, 600.000);
		System.out.println(tour1);
		System.out.println(tour2);
	}
}
