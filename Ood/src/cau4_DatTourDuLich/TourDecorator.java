package cau4_DatTourDuLich;


public abstract class TourDecorator extends Tour {
	protected Tour tour;

	public TourDecorator(Tour tour) {
		this.tour = tour;
	}

	public abstract String toString();
	
	public abstract double price();
}
