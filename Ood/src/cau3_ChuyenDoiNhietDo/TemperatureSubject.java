package cau3_ChuyenDoiNhietDo;

public interface TemperatureSubject {
	public void register(Observer o);
	public void unRegister(Observer o);
	public void noti();
}
