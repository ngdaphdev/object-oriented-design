package cau3_ChuyenDoiNhietDo;

import java.util.ArrayList;
import java.util.List;

public class TemperatureData implements TemperatureSubject {
	private List<Observer> observers;
	private double c, f;

	public TemperatureData() {
		this.observers = new ArrayList<Observer>();
	}

	public void register(Observer o) {
		observers.add(o);
	}

	public void unRegister(Observer o) {
		observers.remove(0);
	}

	public void noti() {
		for (Observer o : observers) {
			if (o.getClass() == CelsiusClient.class)
				o.update(c);
			else
				o.update(f);
		}
	}

	// f = (c*1.8) +32
	// c = (f -32) / 1.8
	public void setTemperature(Temperature t) {

		double index = t.getIndexTemp();

		if (t.getClass() == Celsius.class) {
			c = index;
			f = ((c * 1.8) + 32);
		} else {
			f = index;
			c = ((f - 32) / 1.8);
		}

		// thong bao
		noti();

	}

}
