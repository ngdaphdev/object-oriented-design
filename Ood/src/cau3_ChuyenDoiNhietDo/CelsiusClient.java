package cau3_ChuyenDoiNhietDo;

public class CelsiusClient implements Observer, DisplayElement{
	
	private TemperatureData temperatureData;
	private GUI gui;
	private double index;
	
	

	public CelsiusClient(TemperatureData temperatureData, GUI gui) {
		this.temperatureData = temperatureData;
		this.gui = gui;
		this.temperatureData.register(this);
	}

	@Override
	public void update(double index) {
		this.index = index;
		display();
	}

	@Override
	public void display() {
		this.gui.getTxtBottom().setText(index+"");
		this.gui.setIndexC((int) index);
	}

}
