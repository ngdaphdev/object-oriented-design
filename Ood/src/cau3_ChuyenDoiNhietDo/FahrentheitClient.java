package cau3_ChuyenDoiNhietDo;

public class FahrentheitClient implements Observer, DisplayElement {

	private GUI gui;
	private TemperatureData temperatureData;
	private double index;

	public FahrentheitClient(TemperatureData temperatureData, GUI gui) {
		this.gui = gui;
		this.temperatureData = temperatureData;
		this.temperatureData.register(this);
	}

	@Override
	public void update(double temp) {
		index = temp;
		display();
	}

	@Override
	public void display() {
		this.gui.getTxtTop().setText(index+"");		 // cap nhat do C
		this.gui.setIndexF((int) index);            // cap nhat do F
		this.gui.getBar().setValue((int) index);   // thanh do tinh theo do F
	}

}
