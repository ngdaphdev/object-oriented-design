package cau3_ChuyenDoiNhietDo;

public abstract class Temperature {
	protected double indexTemp;
	
	public Temperature(double indexTemp) {
		this.indexTemp = indexTemp;
	}
	public double getIndexTemp() {
		return indexTemp;
	}
	
	
}
