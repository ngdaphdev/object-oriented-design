package cau3_ChuyenDoiNhietDo;

public class Test {
	public static void main(String[] args) {
		TemperatureData data = new TemperatureData();
		GUI gui = new GUI(data);
		
		CelsiusClient celsiusClient = new CelsiusClient(data, gui);
		FahrentheitClient fahrentheitClient = new FahrentheitClient(data, gui);
		
		gui.setVisible(true);
	}

}