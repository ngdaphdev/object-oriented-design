package cau3_ChuyenDoiNhietDo;

public interface Observer {
	
	void update(double temp);
	
}
