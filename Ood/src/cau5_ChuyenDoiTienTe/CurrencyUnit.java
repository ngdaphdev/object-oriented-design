package cau5_ChuyenDoiTienTe;

public class CurrencyUnit {
	private static double VND = 22505;
	private static double JPY = 111.8;
	private static double EUR = 0.89;
	private static double GBP = 0.77;


	public CurrencyUnit(double vND, double jPY, double eUR, double gBP) {
		super();
		VND = vND;
		JPY = jPY;
		EUR = eUR;
		GBP = gBP;
		
	}

	public static double getVND() {
		return VND;
	}

	public static double getJPY() {
		return JPY;
	}

	public static double getEUR() {
		return EUR;
	}

	public static double getGBP() {
		return GBP;
	}

	

}
