package cau5_ChuyenDoiTienTe;

import java.util.Scanner;

public class CurrencyConversion {
	private static Scanner input;

	public static void main(String[] args) {
		double result = 0;
		input = new Scanner(System.in);
		System.out.println("Chọn kiểu bạn muốn chuyển đổi sau đây");
		System.out.println("1:USD -> Tiền tệ khác");
		System.out.println("2:Tiền tệ khác -> USD");
		int x = input.nextInt();
		
		if (x == 1 || x == 2) {
			System.out.println("Chọn loại tiền tệ khác");
			System.out.println("1 :VND");
			System.out.println("2 :JPY");
			System.out.println("3 :EUR");
			System.out.println("4 :GBP");
			int y = input.nextInt();
			
			if (y <= 4 && y >= 1) {
				System.out.println("Nhập số tiền bạn muốn chuyển đổi");
				double currency = input.nextDouble();

				if (x == 1) {
					switch (y) {
					case 1:
						result = currency * CurrencyUnit.getVND();
						System.out.println(currency + " $" + " = " + result + " VND");
						break;
					case 2:
						result = currency * CurrencyUnit.getJPY();
						System.out.println(currency + " $" + " = " + result + " JPY");
						break;
					case 3:
						result = currency * CurrencyUnit.getEUR();
						System.out.println(currency + " $" + " = " + result + " EUR");
						break;
					case 4:
						result = currency * CurrencyUnit.getGBP();
						System.out.println(currency + " $" + " = " + result + " GBP");
						break;

					}
				}
				if (x == 2) {
					switch (y) {
					case 1:
						result = currency / CurrencyUnit.getVND();
						System.out.println(currency + " VND" + " = " + result + " $");
						break;
					case 2:
						result = currency / CurrencyUnit.getJPY();
						System.out.println(currency + " JPY" + " = " + result + " $");
						break;
					case 3:
						result = currency / CurrencyUnit.getEUR();
						System.out.println(currency + " EUR" + " = " + result + " $");
						break;
					case 4:
						result = currency / CurrencyUnit.getGBP();
						System.out.println(currency + " GBP" + " = " + result + " $");
						break;

					}

				}

			} else {
				System.out.println("Lỗi vui lòng kiểm tra lại !!!!");
			}
		} else {
			System.out.println("Lỗi vui lòng kiểm tra lại !!!!");
		}
	}
}
