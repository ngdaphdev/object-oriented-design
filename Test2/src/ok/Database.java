package ok;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
	public static void main(String[] args) throws SQLException {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String url = "jdbc:sqlserver://localhost:1433;databaseName=QL_KHACHSAN;integratedSecurity=true;";
			String user = "sa";
			String pass = "sa";
			connection = (Connection) DriverManager.getConnection(url, user, pass);
			System.out.println("Thành công");
		} catch (ClassNotFoundException e) {
			System.out.println("Thất bại");
			e.printStackTrace();
		}
	}
}
