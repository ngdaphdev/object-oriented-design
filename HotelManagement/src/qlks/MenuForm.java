package qlks;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import qlks.Form.ServiceForm;
import qlks.Form.BillForm;
import qlks.Form.CustomerForm;
import qlks.Form.EmployeeForm;
import qlks.Form.Roomform;

public class MenuForm extends JFrame {
	private JButton jButton1;
	private JButton jButton2;
	private JButton jButton3;
	private JButton jButton4;
	private JButton jButtonEmployee;
	private JLabel jLabel1;
	private JPanel jPanel1;

	public MenuForm() {
		initComponents();
	}

	private void initComponents() {

		jPanel1 = new JPanel();
		jButtonEmployee = new JButton();
		jButton3 = new JButton();
		jButton1 = new JButton();
		jButton2 = new JButton();
		jButton4 = new JButton();
		jLabel1 = new JLabel();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setBackground(new Color(0, 170, 255));

		jButtonEmployee.setBackground(new Color(255, 255, 255));
		jButtonEmployee.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/employee.png")));
		jButtonEmployee.setText("Employee");
		jButtonEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonEmployeeActionPerformed(evt);
			}
		});

		jButton3.setBackground(new Color(255, 255, 255));
		jButton3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/service.png")));
		jButton3.setText("Service");
		jButton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});

		jButton1.setBackground(new Color(255, 255, 255));
		jButton1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/room.png")));
		jButton1.setText("Room");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jButton2.setBackground(new Color(255, 255, 255));
		jButton2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/customer.png")));
		jButton2.setText("Customer");
		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jButton4.setBackground(new Color(255, 255, 255));
		jButton4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/bill.png")));
		jButton4.setText("Bill");
		jButton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});

		jLabel1.setFont(new Font("Yu Gothic UI Light", 0, 44));
		jLabel1.setText("Hotel Management");

		GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(26, 26, 26).addGroup(jPanel1Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jButton1, GroupLayout.PREFERRED_SIZE, 145,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(jButtonEmployee))
								.addGap(44, 44, 44)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jButton2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(jButton3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(40, 40, 40))
						.addGroup(
								jPanel1Layout.createSequentialGroup()
										.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 375,
												GroupLayout.PREFERRED_SIZE)
										.addGap(8, 8, 8))))
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(115, 115, 115)
						.addComponent(jButton4, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(36, 36, 36).addComponent(jLabel1)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
										.addComponent(jButtonEmployee).addGap(18, 18, 18))
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(22, 22, 22)
										.addComponent(jButton3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jButton2).addComponent(jButton1))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jButton4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
						.addContainerGap()));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel1,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel1,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}

	private void jButtonEmployeeActionPerformed(ActionEvent evt) {
		EmployeeForm nvf = new EmployeeForm();
		nvf.setVisible(true);
		nvf.pack();
		nvf.setLocationRelativeTo(null);
		nvf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void jButton2ActionPerformed(ActionEvent evt) {

		CustomerForm khf = new CustomerForm();
		khf.setVisible(true);
		khf.pack();
		khf.setLocationRelativeTo(null);
		khf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	private void jButton1ActionPerformed(ActionEvent evt) {
		Roomform pf = new Roomform();
		pf.setVisible(true);
		pf.pack();
		pf.setLocationRelativeTo(null);
		pf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	private void jButton4ActionPerformed(ActionEvent evt) {
		BillForm hdf = new BillForm();
		hdf.setVisible(true);
		hdf.pack();
		hdf.setLocationRelativeTo(null);
		hdf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void jButton3ActionPerformed(ActionEvent evt) {

		ServiceForm dvf = new ServiceForm();
		dvf.setVisible(true);
		dvf.pack();
		dvf.setLocationRelativeTo(null);
		dvf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public static void main(String args[]) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(MenuForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(MenuForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(MenuForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(MenuForm.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MenuForm menu = new MenuForm();
//				menu.setVisible(true);
//				menu.setResizable(false);
			}
		});
	}

}
