package qlks;

import java.sql.DriverManager;

public class MyConnection {
	public static java.sql.Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			String url = "jdbc:sqlserver://localhost:1433;databaseName=QL_KHACHSAN;integratedSecurity=true;";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String username = "sa";
			String password = "sa";
			conn = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void closeConnection(java.sql.Connection c) {
		try {
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
