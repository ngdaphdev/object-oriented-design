package qlks;

import java.util.Date;

public class Employee {
	private String employeeID;
	private String employeeName;
	private String jobTitle;
	private double salary;
	private Date birthday;
	private String sex;
	private String note;

	public Employee(String employeeID, String employeeName, String jobTitle, double salary, Date birthday, String sex,
			String note) {
		super();
		this.employeeID = employeeID;
		this.employeeName = employeeName;
		this.jobTitle = jobTitle;
		this.salary = salary;
		this.birthday = birthday;
		this.sex = sex;
		this.note = note;
	}

	public Employee() {
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String toString() {
		return "Employee [employeeID=" + employeeID + ", employeeName=" + employeeName + ", jobTitle=" + jobTitle
				+ ", salary=" + salary + ", birthday=" + birthday + ", sex=" + sex + ", note=" + note + "]";
	}

}