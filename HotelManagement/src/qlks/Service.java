package qlks;

public class Service {
	private String serviceID;
	private String serviceName;
	private double servicePrice;

	public Service(String serviceID, String serviceName, double servicePrice) {
		super();
		this.serviceID = serviceID;
		this.serviceName = serviceName;
		this.servicePrice = servicePrice;
	}

	public Service() {
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public double getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(double servicePrice) {
		this.servicePrice = servicePrice;
	}

	@Override
	public String toString() {
		return "Service [serviceID=" + serviceID + ", serviceName=" + serviceName + ", servicePrice=" + servicePrice
				+ "]";
	}

}
