CREATE DATABASE  QL_KHACHSAN 
USE QL_KHACHSAN

--Drop database QL_KHACHSAN

CREATE TABLE NHANVIEN
(
 MANV VARCHAR(11) NOT NULL,
 TENNV VARCHAR(50) NOT NULL,
 CHUCVU VARCHAR(50),
 LUONGNV BIGINT,
 NGAYSINH DATE,
 GIOITINH CHAR(10),
 CHUTHICH VARCHAR(50),
 
 CONSTRAINT pk_NHANVIEN PRIMARY KEY (MANV)
 );


 CREATE TABLE  DICHVU
(
 MADV VARCHAR(11) NOT NULL,
 TENDV VARCHAR(100) NOT NULL,
 GIADV INT,
 CONSTRAINT pk_DICHVU PRIMARY KEY (MADV)
 );

 
 
 CREATE TABLE PHONG
(
 MAPHONG VARCHAR(11) NOT NULL,
 TENPHONG VARCHAR(50) NOT NULL,
 LOAIPHONG VARCHAR(20) NOT NULL,
 GIAPHONG INT,
 CHUTHICH TEXT,
 TINHTRANG VARCHAR(50),
 MANV VARCHAR(11),
 MADV VARCHAR(11),
 
 CONSTRAINT pk_PHONG PRIMARY KEY (MAPHONG),
 CONSTRAINT fk_NHANVIEN1 FOREIGN KEY (MANV) REFERENCES NHANVIEN(MANV),
 CONSTRAINT fk_DICHVU FOREIGN KEY (MADV) REFERENCES DICHVU(MADV)
 
);



CREATE TABLE KHACHHANG
(
 MAKH VARCHAR(11) NOT NULL,
 TENKH VARCHAR(50) NOT NULL,
 CMND VARCHAR(20) NOT NULL,
 QUOCTICH VARCHAR(50),
 GIOITINH CHAR(10),
 TUOI INT,
 SDT VARCHAR(20),
 MAPHONG VARCHAR(11),
 
 CONSTRAINT pk_KHACHHANG PRIMARY KEY (MAKH),
 CONSTRAINT fk_PHONG FOREIGN KEY (MAPHONG) REFERENCES PHONG(MAPHONG)
 
 );

 
 CREATE TABLE HOADON
(
 MAHD VARCHAR(11) NOT NULL,
 MANV VARCHAR(11),
 MAPHONG VARCHAR(11),
 NGAY DATE,
 GIAHD INT,
 CONSTRAINT pk_HOADON PRIMARY KEY (MAHD),
 CONSTRAINT fk_NHANVIEN2 FOREIGN KEY (MANV) REFERENCES NHANVIEN(MANV),
 CONSTRAINT fk_PHONG2 FOREIGN KEY (MAPHONG) REFERENCES PHONG(MAPHONG)
 );


INSERT INTO NHANVIEN VALUES ('NV1','Nguyen Le Hoai Thuy', 'Manager', 2575000, '1978-09-13','Female',''),
		                    ('NV2','Tran Manh Dung', 'CEO', 3775000, '1975-02-10','Male',''),
		                    ('NV3','Vu Quoc Dat', 'Staff service', 1500000, '1994-05-22','Male',''),
		                    ('NV4','Le Thi Diem', 'Staff service', 1300000, '1991-12-4','Female',''),
		                    ('NV5','Nguyen Vo Trong Hieu', 'Staff service', 135000, '1988-06-13','Male','')

INSERT INTO DICHVU VALUES ('DV1', 'Launder', 540000),
		                  ('DV2', 'Shuttle Bus', 1120000),
		                  ('DV3', 'Service Food and Drink', 300000),
		                  ('DV4', 'Car rental', 440000),
		                  ('DV5', 'Massaage', 340000);
INSERT INTO PHONG VALUES ('P001', 'Room 001', 'VIP', 782000,'Bon tam nong lanh, may suoi, may lanh, tu lanh, tivi phang,...','Not null', 'NV3','DV3'),
		                 ('P002', 'Room 002', 'VIP', 782000,'Bon tam nong lanh, may suoi, may lanh, tu lanh, tivi phang,...','Null', null,null),
		                 ('P003', 'Room 003', 'Single rooom',570000,'1 giuong don, tivi, tu lanh,...', 'Not Null', 'NV5','DV2'),
		                 ('P004', 'Room 004', 'Single room',570000,'1 giuong don, tivi, tu lanh,...', 'Null', null,null),
		                 ('P005', 'Room 005', 'Double room',700000,'1 giuong doi, tivi, tu lanh,...', 'Not null', 'NV4','DV1'),
		                 ('P006', 'Room 006', 'Double rooom',570000,'1 giuong doi, tivi, tu lanh,...', 'Not Null', 'NV5','DV5'),
		                 ('P007', 'Room 007', 'Floor rooom', 800000,'Bao gom 8 giuong, nha ve sinh, may lanh, tivi, may say,...','Not Null', 'NV3','DV4');

INSERT INTO KHACHHANG VALUES ('K01', 'Le Tin Thuong', '2727234565', 'VietNam','Male', 23,'0808009076','P001'),
		                     ('K02', 'Angella Deguilar', '2727323455', 'Singapore','Female', 35,'0812354333','P003'),
		                     ('K03', 'Victoria Kim', '2727234567', 'Australia','Female', 26,'0808009076','P005'),
		                     ('K04', 'TShou Shin', '2727234569', 'China','Male', 37,'0808765890','P006'),
		                     ('K05', 'Nguyen Manh Cuong', '2727234532', 'VietNam','Male', 43,'0808123123','P007');
INSERT INTO HOADON VALUES ('H01', 'NV3', 'P001', '2022-6-22',1320000),
		                  ('H02', 'NV3', 'P007', '2022-6-23',1200000),
		                  ('H03', 'NV4', 'P005', '2022-6-24',2020000),
		                  ('H04', 'NV5', 'P003', '2022-6-22',1690000),
		                  ('H05', 'NV5', 'P006', '2022-6-22',910000);