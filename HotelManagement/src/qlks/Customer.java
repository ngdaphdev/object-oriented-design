package qlks;

public class Customer {
	private String customerID;
	private String name;
	private String citizenID;
	private String nationality;
	private String sex;
	private int age;
	private String tel;
	private String roomID;

	public Customer(String customerID, String name, String citizenID, String nationality, String sex, int age,
			String tel, String roomID) {
		super();
		this.customerID = customerID;
		this.name = name;
		this.citizenID = citizenID;
		this.nationality = nationality;
		this.sex = sex;
		this.age = age;
		this.tel = tel;
		this.roomID = roomID;
	}

	public Customer() {
	}

	Customer(String string, String string0, String string1, String string2, String string3, int aInt, String string4) {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
																		// Tools | Templates.
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCitizenID() {
		return citizenID;
	}

	public void setCitizenID(String citizenID) {
		this.citizenID = citizenID;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", name=" + name + ", citizenID=" + citizenID + ", nationality="
				+ nationality + ", sex=" + sex + ", age=" + age + ", tel=" + tel + ", roomID=" + roomID + "]";
	}

}
