package qlks.Form;

import static qlks.MyConnection.getConnection;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import qlks.Employee;

public class EmployeeForm extends JFrame {
	private JButton jButtonclear;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel8;
	private JPanel jPanel3;
	private JPanel jPanel8;
	private JPanel jPanel9;
	private JScrollPane jScrollPane1;
	private JTable jTableEmployee;
	private JTextField jTextFieldJobTitle;
	private JTextField jTextFieldNote;
	private JTextField jTextFieldSex;
	private JTextField jTextFieldSalary;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldBirthday;
	private JTextField jTextFieldEmployeeName;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;

	public EmployeeForm() {
		initComponents();
		getConnection();
		hienThiDanhSachNhanVien();
	}

	Connection con = null;
	Statement st = null;

	public ArrayList<Employee> layDanhSachNhanVien() {
		ArrayList<Employee> dsnv = new ArrayList<Employee>();
		Connection con = getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM nhanvien";
			// Thực thi câu lệnh truy vấn
			ResultSet rs = st.executeQuery(sql);

			Employee nv;
			while (rs.next()) {
				nv = new Employee(rs.getString("MANV"), rs.getString("TENNV"), rs.getString("CHUCVU"),
						rs.getDouble("LUONGNV"), rs.getDate("NGAYSINH"), rs.getString("GIOITINH"),
						rs.getString("CHUTHICH"));

				// Thêm vào danh sách
				dsnv.add(nv);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsnv;
	}

	public void hienThiDanhSachNhanVien() {
		String colTieuDe1[] = new String[] { "Emp ID", "Emp Name", "Job Title", "Salary", "Birthday", "Sex", "Note" };
		ArrayList<Employee> dsnv = layDanhSachNhanVien();

		DefaultTableModel model = new DefaultTableModel(colTieuDe1, 0);

		Object[] row;

		for (int i = 0; i < dsnv.size(); i++) {

			row = new Object[7];

			// GÁN GIÁ TRỊ
			row[0] = dsnv.get(i).getEmployeeID();
			row[1] = dsnv.get(i).getEmployeeName();
			row[2] = dsnv.get(i).getJobTitle();
			row[3] = dsnv.get(i).getSalary();
			row[4] = dsnv.get(i).getBirthday();
			row[5] = dsnv.get(i).getSex();
			row[6] = dsnv.get(i).getNote();

			model.addRow(row);
		}

		// }catch(ArrayIndexOutOfBoundsException ex){

		jTableEmployee.setModel(model);

	}

	private void initComponents() {

		jPanel3 = new JPanel();
		jPanel8 = new JPanel();
		jLabel1 = new JLabel();
		jPanel9 = new JPanel();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();
		jLabel6 = new JLabel();
		jLabel7 = new JLabel();
		jLabel8 = new JLabel();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldEmployeeName = new JTextField();
		jTextFieldJobTitle = new JTextField();
		jTextFieldSalary = new JTextField();
		jTextFieldBirthday = new JTextField();
		jTextFieldSex = new JTextField();
		jTextFieldNote = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jButtonclear = new JButton();
		jScrollPane1 = new JScrollPane();
		jTableEmployee = new JTable();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel3.setBackground(new Color(0, 170, 255));

		jLabel1.setFont(new Font("Tahoma", 10, 23));
		jLabel1.setText("Employee Management");
		jPanel8.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel8Layout = new GroupLayout(jPanel8);
		jPanel8.setLayout(jPanel8Layout);
		jPanel8Layout.setHorizontalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel8Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabel1).addGap(87, 87, 87)));
		jPanel8Layout.setVerticalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel8Layout.createSequentialGroup().addGap(19, 19, 19)
						.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(21, Short.MAX_VALUE)));

		jPanel9.setBorder(BorderFactory.createTitledBorder("Employee Information"));

		jLabel2.setText("Employee ID");

		jLabel3.setText("Employee Name");

		jLabel4.setText("Job Title");

		jLabel5.setText("Salary");

		jLabel6.setText("Birthday       ");

		jLabel7.setText("Sex            ");

		jLabel8.setText("Note           ");

		jTextFieldEmployeeName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldEmployeeNameActionPerformed(evt);
			}
		});

		jTextFieldSalary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldSalaryActionPerformed(evt);
			}
		});

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				addActionPerformed(evt);
			}
		});

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				editActionPerformed(evt);
			}
		});

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				deleteActionPerformed(evt);
			}
		});

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exitActionPerformed(evt);
			}
		});

		jButtonclear.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear.setText("Clear");
		jButtonclear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonclearActionPerformed(evt);
			}
		});

		GroupLayout jPanel9Layout = new GroupLayout(jPanel9);
		jPanel9.setLayout(jPanel9Layout);
		jPanel9Layout.setHorizontalGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
						.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(jPanel9Layout.createSequentialGroup().addContainerGap()
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
										.addComponent(delete, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(add, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(18, 18, 18)
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(jPanel9Layout.createSequentialGroup().addComponent(exit).addGap(0, 0,
												Short.MAX_VALUE))
										.addGroup(jPanel9Layout.createSequentialGroup().addComponent(edit)
												.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(jButtonclear, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
						.addGroup(GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
								.addGap(19, 19, 19)
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addGroup(jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
												.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout
																.createSequentialGroup()
																.addGroup(jPanel9Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jLabel3).addComponent(jLabel2))
																.addGap(18, 18, 18))
														.addGroup(jPanel9Layout.createSequentialGroup()
																.addComponent(jLabel4).addGap(29, 29, 29)))
												.addGroup(jPanel9Layout.createSequentialGroup().addComponent(jLabel5)
														.addGap(39, 39, 39)))
												.addGroup(jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
														.addComponent(jTextFieldSalary)
														.addComponent(jTextFieldJobTitle, GroupLayout.Alignment.LEADING)
														.addComponent(jTextFieldEmployeeName)
														.addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
																150, GroupLayout.PREFERRED_SIZE)))
										.addGroup(GroupLayout.Alignment.LEADING,
												jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(jPanel9Layout
																.createParallelGroup(GroupLayout.Alignment.TRAILING,
																		false)
																.addComponent(jLabel7, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(jLabel6, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
														.addComponent(jLabel8)).addGap(39, 39, 39)
														.addGroup(jPanel9Layout
																.createParallelGroup(GroupLayout.Alignment.LEADING)
																.addComponent(jTextFieldSex)
																.addComponent(jTextFieldBirthday)
																.addComponent(jTextFieldNote))))))
						.addContainerGap()));
		jPanel9Layout.setVerticalGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel9Layout.createSequentialGroup().addGap(19, 19, 19)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2).addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel3).addComponent(jTextFieldEmployeeName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel4).addComponent(jTextFieldJobTitle, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel5).addComponent(jTextFieldSalary, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel6).addComponent(jTextFieldBirthday, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel7).addComponent(jTextFieldSex, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel8).addComponent(jTextFieldNote, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear))
						.addGap(18, 18, 18).addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(37, Short.MAX_VALUE)));

		jTableEmployee
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jTableEmployee.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jTableEmployeeMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(jTableEmployee);

		GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup().addGroup(jPanel3Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(49, 49, 49)
								.addComponent(jPanel9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(36, 36, 36).addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 568,
										GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(240, 240, 240).addComponent(jPanel8,
								GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(45, Short.MAX_VALUE)));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup().addGap(21, 21, 21)
						.addComponent(jPanel8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
								.addComponent(jPanel9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addContainerGap(16, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 1005, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 536, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup()
												.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jPanel3, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addContainerGap())));

		pack();
	}

	private void jTableEmployeeMouseClicked(MouseEvent evt) {
		int i = jTableEmployee.getSelectedRow();
		TableModel model = jTableEmployee.getModel();
		jTextFieldEmployeeID.setText(model.getValueAt(i, 0).toString());
		jTextFieldEmployeeName.setText(model.getValueAt(i, 1).toString());
		jTextFieldJobTitle.setText(model.getValueAt(i, 2).toString());
		jTextFieldSalary.setText(model.getValueAt(i, 3).toString());
		jTextFieldBirthday.setText(model.getValueAt(i, 4).toString());
		jTextFieldSex.setText(model.getValueAt(i, 5).toString());
		jTextFieldNote.setText(model.getValueAt(i, 6).toString());
	}

	// Clear
	private void jButtonclearActionPerformed(ActionEvent evt) {
		jTextFieldEmployeeID.setText("");
		jTextFieldEmployeeName.setText("");
		jTextFieldJobTitle.setText("");
		jTextFieldSalary.setText("");
		jTextFieldBirthday.setText("");
		jTextFieldSex.setText("");
		jTextFieldNote.setText("");
		jTextFieldEmployeeID.requestFocus();
	}

	// Exit
	private void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program ");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Thông báo",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	// Delete
	private void deleteActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "DELETE FROM NhanVien WHERE MANV = '" + jTextFieldEmployeeID.getText() + "'";
			st.executeUpdate(query);
			hienThiDanhSachNhanVien();

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Edit
	private void editActionPerformed(ActionEvent evt) {
		if (jTableEmployee.getSelectedRow() == -1) {
			if (jTableEmployee.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			DefaultTableModel model = (DefaultTableModel) jTableEmployee.getModel();
			model.setValueAt(jTextFieldEmployeeID.getText(), jTableEmployee.getSelectedRow(), 0);
			model.setValueAt(jTextFieldEmployeeName.getText().toString(), jTableEmployee.getSelectedRow(), 1);
			model.setValueAt(jTextFieldJobTitle.getText(), jTableEmployee.getSelectedRow(), 2);
			model.setValueAt(jTextFieldSalary.getText(), jTableEmployee.getSelectedRow(), 2);
			model.setValueAt(jTextFieldBirthday.getText(), jTableEmployee.getSelectedRow(), 2);
			model.setValueAt(jTextFieldSex.getText(), jTableEmployee.getSelectedRow(), 2);
			model.setValueAt(jTextFieldNote.getText(), jTableEmployee.getSelectedRow(), 2);
		}
	}

	private void addActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "INSERT INTO NhanVien(MANV,TENNV,CHUCVU, LUONGNV, NGAYSINH, GIOITINH, CHUTHICH) VALUES('"
					+ jTextFieldEmployeeID.getText() + "'," + "'" + jTextFieldEmployeeName.getText() + "','"
					+ jTextFieldJobTitle.getText() + "','" + jTextFieldSalary.getText() + "','"
					+ jTextFieldBirthday.getText() + "','" + jTextFieldSex.getText() + "','" + jTextFieldNote.getText()
					+ "')";

			st.execute(query);
			hienThiDanhSachNhanVien();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jTextFieldSalaryActionPerformed(ActionEvent evt) {

	}

	private void jTextFieldEmployeeNameActionPerformed(ActionEvent evt) {

	}

	public static void main(String args[]) {
		// Giao diện Nimbus
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(EmployeeForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(EmployeeForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(EmployeeForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(EmployeeForm.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				EmployeeForm frame = new EmployeeForm();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
