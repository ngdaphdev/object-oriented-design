package qlks.Form;

import static qlks.MyConnection.getConnection;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import qlks.Room;

public class Roomform extends JFrame {
	private JButton jButtonclear3;
	private JLabel jLabel22;
	private JLabel jLabel23;
	private JLabel jLabel24;
	private JLabel jLabel25;
	private JLabel jLabel26;
	private JLabel jLabel27;
	private JLabel jLabel28;
	private JLabel jLabel29;
	private JLabel jLabel30;
	private JPanel jPanel17;
	private JPanel jPanel18;
	private JPanel jPanel5;
	private JScrollPane jScrollPane4;
	private JTable jTableRoom;
	private JTextField jTextFieldRoom;
	private JTextField jTextFieldPrice;
	private JTextField jTextFieldKindOfRoom;
	private JTextField jTextFieldServiceID;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldRoomName;
	private JTextField jTextFieldCondition;
	private JButton edit3;
	private JButton add3;
	private JButton exit3;
	private JButton delete3;

	public Roomform() {
		initComponents();
		getConnection();
		hienThiDanhSachPhong();
	}

	Connection con = null;
	Statement st = null;

	public ArrayList<Room> layDanhSachPhong() {
		ArrayList<Room> dsp = new ArrayList<Room>();
		Connection con = getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM phong";
			// Thực thi câu lệnh truy vấn
			ResultSet rs = st.executeQuery(sql);

			Room p;
			while (rs.next()) {
				p = new Room(rs.getString("MAPHONG"), rs.getString("TENPHONG"), rs.getString("LOAIPHONG"),
						rs.getDouble("GIAPHONG"), rs.getString("CHUTHICH"), rs.getString("TINHTRANG"),
						rs.getString("MANV"), rs.getString("MADV"));

				// Thêm vào danh sách
				dsp.add(p);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsp;
	}

	public void hienThiDanhSachPhong() {
		String colTieuDe1[] = new String[] { "Room ID", "Room Name", "Kind Of Room", "Price", "Note", "Condition",
				"Employee ID", "Service ID" };
		ArrayList<Room> dsp = layDanhSachPhong();

		DefaultTableModel model = new DefaultTableModel(colTieuDe1, 0);

		Object[] row;

		for (int i = 0; i < dsp.size(); i++) {

			row = new Object[8];

			// GÁN GIÁ TRỊ
			row[0] = dsp.get(i).getRoomID();
			row[1] = dsp.get(i).getRoomName();
			row[2] = dsp.get(i).getKindOfRoom();
			row[3] = dsp.get(i).getPrice();
			row[4] = dsp.get(i).getNote();
			row[5] = dsp.get(i).getCondition();
			row[6] = dsp.get(i).getEmployeeID();
			row[7] = dsp.get(i).getServiceID();

			model.addRow(row);
		}

		// }catch(ArrayIndexOutOfBoundsException ex){

		jTableRoom.setModel(model);

	}

	private void initComponents() {

		jPanel5 = new JPanel();
		jPanel17 = new JPanel();
		jLabel22 = new JLabel();
		jScrollPane4 = new JScrollPane();
		jTableRoom = new JTable();
		jPanel18 = new JPanel();
		jLabel23 = new JLabel();
		jLabel24 = new JLabel();
		jLabel25 = new JLabel();
		jLabel26 = new JLabel();
		jLabel27 = new JLabel();
		jLabel28 = new JLabel();
		jTextFieldRoomID = new JTextField();
		jTextFieldRoomName = new JTextField();
		jTextFieldKindOfRoom = new JTextField();
		jTextFieldPrice = new JTextField();
		jTextFieldRoom = new JTextField();
		jTextFieldCondition = new JTextField();
		add3 = new JButton();
		edit3 = new JButton();
		delete3 = new JButton();
		exit3 = new JButton();
		jLabel29 = new JLabel();
		jLabel30 = new JLabel();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldServiceID = new JTextField();
		jButtonclear3 = new JButton();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel5.setBackground(new Color(0, 170, 255));

		jLabel22.setFont(new Font("Tahoma", 0, 22));
		jLabel22.setText("Room Management");
		jPanel17.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel17Layout = new GroupLayout(jPanel17);
		jPanel17.setLayout(jPanel17Layout);
		jPanel17Layout.setHorizontalGroup(jPanel17Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING,
						jPanel17Layout.createSequentialGroup()
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jLabel22)
								.addGap(155, 155, 155)));
		jPanel17Layout.setVerticalGroup(jPanel17Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel17Layout.createSequentialGroup().addGap(23, 23, 23)
						.addComponent(jLabel22, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGap(30, 30, 30)));

		jTableRoom
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jTableRoom.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jTableRoomMouseClicked(evt);
			}
		});
		jScrollPane4.setViewportView(jTableRoom);

		jPanel18.setBorder(BorderFactory.createTitledBorder("Room information"));

		jLabel23.setText("Room ID");

		jLabel24.setText("Room Name");

		jLabel25.setText("Kind Of Room");

		jLabel26.setText("Price");

		jLabel27.setText("Note");

		jLabel28.setText("Condition");

		jLabel29.setText("Employee ID");

		jLabel30.setText("Service ID");

		add3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add3.setText("Add");
		add3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				add3ActionPerformed(evt);
			}
		});

		edit3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit3.setText("Edit");
		edit3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				edit3ActionPerformed(evt);
			}
		});

		delete3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete3.setText("Delete");
		delete3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delete3ActionPerformed(evt);
			}
		});

		exit3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit3.setText("Exit");
		exit3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exit3ActionPerformed(evt);
			}
		});

		jButtonclear3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear3.setText("Clear");
		jButtonclear3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonclear3ActionPerformed(evt);
			}
		});

		GroupLayout jPanel18Layout = new GroupLayout(jPanel18);
		jPanel18.setLayout(jPanel18Layout);
		jPanel18Layout.setHorizontalGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel18Layout.createSequentialGroup().addGap(21, 21, 21).addGroup(jPanel18Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel18Layout.createSequentialGroup()
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(delete3).addComponent(add3))
								.addGap(6, 6, Short.MAX_VALUE)
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addComponent(edit3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(exit3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(18, 18, 18).addComponent(jButtonclear3))
						.addGroup(jPanel18Layout.createSequentialGroup()
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jLabel23).addComponent(jLabel24).addComponent(jLabel25)
										.addComponent(jLabel26).addComponent(jLabel27).addComponent(jLabel28)
										.addComponent(jLabel29).addComponent(jLabel30))
								.addGap(71, 71, 71)
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
										.addComponent(jTextFieldRoomID, GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
										.addComponent(jTextFieldRoomName).addComponent(jTextFieldKindOfRoom)
										.addComponent(jTextFieldPrice).addComponent(jTextFieldRoom)
										.addComponent(jTextFieldCondition).addComponent(jTextFieldEmployeeID)
										.addComponent(jTextFieldServiceID))))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel18Layout.setVerticalGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel18Layout.createSequentialGroup().addGap(22, 22, 22)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel23).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel24).addComponent(jTextFieldRoomName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel25).addComponent(jTextFieldKindOfRoom, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel26).addComponent(jTextFieldPrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel27).addComponent(jTextFieldRoom, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel28).addComponent(jTextFieldCondition, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel29).addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel30).addComponent(jTextFieldServiceID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(25, 25, 25)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add3)
								.addComponent(edit3).addComponent(jButtonclear3))
						.addGap(18, 18, 18).addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete3).addComponent(exit3))
						.addContainerGap(16, Short.MAX_VALUE)));

		GroupLayout jPanel5Layout = new GroupLayout(jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout.setHorizontalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jScrollPane4, GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE).addContainerGap())
				.addGroup(jPanel5Layout.createSequentialGroup().addGap(342, 342, 342)
						.addComponent(jPanel17, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel5Layout.setVerticalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup().addGroup(jPanel5Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel5Layout.createSequentialGroup().addContainerGap()
								.addComponent(jPanel17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(49, 49, 49).addComponent(jScrollPane4, GroupLayout.PREFERRED_SIZE, 285,
										GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel5Layout.createSequentialGroup().addGap(99, 99, 99).addComponent(jPanel18,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1094, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(layout
										.createSequentialGroup().addContainerGap().addComponent(jPanel5,
												GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addContainerGap())));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 510, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		pack();
	}

	private void jTableRoomMouseClicked(MouseEvent evt) {
		int i = jTableRoom.getSelectedRow();
		TableModel model = jTableRoom.getModel();
		jTextFieldRoomID.setText(model.getValueAt(i, 0).toString());
		jTextFieldRoomName.setText(model.getValueAt(i, 1).toString());
		jTextFieldKindOfRoom.setText(model.getValueAt(i, 2).toString());
		jTextFieldPrice.setText(model.getValueAt(i, 3).toString());
		jTextFieldRoom.setText(model.getValueAt(i, 4).toString());
		jTextFieldCondition.setText(model.getValueAt(i, 5).toString());
		jTextFieldEmployeeID.setText(model.getValueAt(i, 6).toString());
		jTextFieldServiceID.setText(model.getValueAt(i, 7).toString());
	}

	// Add
	private void add3ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "INSERT INTO phong(MAPHONG,TENPHONG, LOAIPHONG, GIAPHONG, CHUTHICH, TINHTRANG, MANV, MADV) VALUES('"
					+ jTextFieldRoomID.getText() + "'," + "'" + jTextFieldRoomName.getText() + "','"
					+ jTextFieldKindOfRoom.getText() + "', '" + jTextFieldPrice.getText() + "', '"
					+ jTextFieldRoom.getText() + "', '" + jTextFieldCondition.getText() + "', '"
					+ jTextFieldEmployeeID.getText() + "', '" + jTextFieldServiceID.getText() + "')";

			st.execute(query);
			hienThiDanhSachPhong();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Edit
	private void edit3ActionPerformed(ActionEvent evt) {
		if (jTableRoom.getSelectedRow() == -1) {
			if (jTableRoom.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			DefaultTableModel model = (DefaultTableModel) jTableRoom.getModel();
			model.setValueAt(jTextFieldRoomID.getText(), jTableRoom.getSelectedRow(), 0);
			model.setValueAt(jTextFieldRoomName.getText().toString(), jTableRoom.getSelectedRow(), 1);
			model.setValueAt(jTextFieldKindOfRoom.getText(), jTableRoom.getSelectedRow(), 2);
			model.setValueAt(jTextFieldPrice.getText(), jTableRoom.getSelectedRow(), 3);
			model.setValueAt(jTextFieldRoom.getText(), jTableRoom.getSelectedRow(), 4);
			model.setValueAt(jTextFieldCondition.getText(), jTableRoom.getSelectedRow(), 5);
			model.setValueAt(jTextFieldEmployeeID.getText(), jTableRoom.getSelectedRow(), 6);
			model.setValueAt(jTextFieldServiceID.getText(), jTableRoom.getSelectedRow(), 7);
		}
	}

	// Delete
	private void delete3ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "DELETE FROM phong WHERE MAPHONG = '" + jTextFieldRoomID.getText() + "'";
			st.executeUpdate(query);
			hienThiDanhSachPhong();

		} catch (Exception ex) {

			ex.printStackTrace();
		}

	}

	// Exit
	private void exit3ActionPerformed(ActionEvent evt) {
		exit3.setToolTipText("Click to exit the program ");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	// Clear
	private void jButtonclear3ActionPerformed(ActionEvent evt) {
		jTextFieldRoomID.setText("");
		jTextFieldRoomName.setText("");
		jTextFieldKindOfRoom.setText("");
		jTextFieldPrice.setText("");
		jTextFieldRoom.setText("");
		jTextFieldCondition.setText("");
		jTextFieldEmployeeID.setText("");
		jTextFieldServiceID.setText("");
		jTextFieldRoomID.requestFocus();

	}

	public static void main(String args[]) {
		// Giao diện Nimbus
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Roomform.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(Roomform.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(Roomform.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(Roomform.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				Roomform frame = new Roomform();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
