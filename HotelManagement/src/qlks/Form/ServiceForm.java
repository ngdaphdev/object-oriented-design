package qlks.Form;

import static qlks.MyConnection.getConnection;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import qlks.Service;

public class ServiceForm extends JFrame {
	private JButton jButtonclear1;
	private JLabel jLabel18;
	private JLabel jLabel19;
	private JLabel jLabel20;
	private JLabel jLabel21;
	private JPanel jPanel14;
	private JPanel jPanel15;
	private JPanel jPanel7;
	private JScrollPane jScrollPane3;
	private JTable jTableDichvu;
	private JTextField jTextFieldServicePrice;
	private JTextField jTextFieldServiceID;
	private JTextField jTextFieldServiceName;
	private JButton edit2;
	private JButton add2;
	private JButton exit2;
	private JButton delete2;

	public ServiceForm() {
		initComponents();
		getConnection();
		hienThiDanhSachDichVu();
	}

	Connection con = null;
	Statement st = null;

	public ArrayList<Service> layDanhSachDichVu() {
		ArrayList<Service> dsdv = new ArrayList<Service>();
		Connection con = getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM dichvu";
			// Thực thi câu lệnh truy vấn
			ResultSet rs = st.executeQuery(sql);

			Service dv;
			while (rs.next()) {
				dv = new Service(rs.getString("MADV"), rs.getString("TENDV"), rs.getDouble("GIADV"));

				// Thêm vào danh sách
				dsdv.add(dv);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsdv;
	}

	public void hienThiDanhSachDichVu() {
		String colTieuDe1[] = new String[] { "Service ID", "Service Name", "Service Price" };
		ArrayList<Service> dsdv = layDanhSachDichVu();

		DefaultTableModel model = new DefaultTableModel(colTieuDe1, 0);

		Object[] row;

		for (int i = 0; i < dsdv.size(); i++) {

			row = new Object[3];

			// GÁN GIÁ TRỊ
			row[0] = dsdv.get(i).getServiceID();
			row[1] = dsdv.get(i).getServiceName();
			row[2] = dsdv.get(i).getServicePrice();

			model.addRow(row);
		}

		// }catch(ArrayIndexOutOfBoundsException ex){

		jTableDichvu.setModel(model);

	}

	private void initComponents() {

		jPanel7 = new JPanel();
		jPanel14 = new JPanel();
		jLabel18 = new JLabel();
		jPanel15 = new JPanel();
		jLabel19 = new JLabel();
		jLabel20 = new JLabel();
		jLabel21 = new JLabel();
		jTextFieldServiceID = new JTextField();
		jTextFieldServiceName = new JTextField();
		jTextFieldServicePrice = new JTextField();
		add2 = new JButton();
		edit2 = new JButton();
		delete2 = new JButton();
		exit2 = new JButton();
		jButtonclear1 = new JButton();
		jScrollPane3 = new JScrollPane();
		jTableDichvu = new JTable();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel7.setBackground(new Color(0, 170, 255));
		jPanel14.setBackground(new Color(0, 170, 255));
		jLabel18.setFont(new Font("Tahoma", 0, 23));
		jLabel18.setText("Service Management");

		GroupLayout jPanel14Layout = new GroupLayout(jPanel14);
		jPanel14.setLayout(jPanel14Layout);
		jPanel14Layout.setHorizontalGroup(jPanel14Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel14Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabel18, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
						.addGap(87, 87, 87)));
		jPanel14Layout.setVerticalGroup(jPanel14Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel14Layout.createSequentialGroup().addContainerGap()
						.addComponent(jLabel18, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jPanel15.setBorder(BorderFactory.createTitledBorder("Service information"));

		jLabel19.setText("Service ID");

		jLabel20.setText("Service Name");

		jLabel21.setText("Service Price");

		add2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add2.setText("Add");
		add2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				add2ActionPerformed(evt);
			}
		});

		edit2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit2.setText("Edit");
		edit2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				edit2ActionPerformed(evt);
			}
		});

		delete2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete2.setText("Delete");
		delete2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delete2ActionPerformed(evt);
			}
		});

		exit2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit2.setText("Exit");
		exit2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exit2ActionPerformed(evt);
			}
		});

		jButtonclear1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear1.setText("Clear");
		jButtonclear1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonclear1ActionPerformed(evt);
			}
		});

		GroupLayout jPanel15Layout = new GroupLayout(jPanel15);
		jPanel15.setLayout(jPanel15Layout);
		jPanel15Layout.setHorizontalGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel15Layout.createSequentialGroup().addGap(26, 26, 26)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addComponent(jLabel21).addComponent(jLabel20).addComponent(jLabel19))
								.addComponent(add2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(delete2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel15Layout.createSequentialGroup().addGap(25, 25, 25).addGroup(
										jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(jTextFieldServiceID, GroupLayout.DEFAULT_SIZE, 72,
														Short.MAX_VALUE)
												.addComponent(jTextFieldServiceName)
												.addComponent(jTextFieldServicePrice)))
								.addGroup(jPanel15Layout.createSequentialGroup().addGap(18, 18, 18)
										.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(exit2)
												.addGroup(jPanel15Layout.createSequentialGroup()
														.addComponent(edit2, GroupLayout.PREFERRED_SIZE, 72,
																GroupLayout.PREFERRED_SIZE)
														.addGap(18, 18, 18).addComponent(jButtonclear1)))))
						.addContainerGap(31, Short.MAX_VALUE)));
		jPanel15Layout.setVerticalGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel15Layout.createSequentialGroup().addGap(21, 21, 21)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel19).addComponent(jTextFieldServiceID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel20).addComponent(jTextFieldServiceName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(23, 23, 23)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel21).addComponent(jTextFieldServicePrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(31, 31, 31)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add2)
								.addComponent(edit2).addComponent(jButtonclear1))
						.addGap(18, 18, 18).addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete2).addComponent(exit2))
						.addContainerGap(43, Short.MAX_VALUE)));

		jTableDichvu
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jTableDichvu.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jTableServiceMouseClicked(evt);
			}
		});
		jScrollPane3.setViewportView(jTableDichvu);

		GroupLayout jPanel7Layout = new GroupLayout(jPanel7);
		jPanel7.setLayout(jPanel7Layout);
		jPanel7Layout.setHorizontalGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel7Layout.createSequentialGroup().addGroup(jPanel7Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel7Layout.createSequentialGroup().addGap(46, 46, 46)
								.addComponent(jPanel15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jScrollPane3,
										GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel7Layout.createSequentialGroup().addGap(238, 238, 238).addComponent(jPanel14,
								GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(24, Short.MAX_VALUE)));
		jPanel7Layout
				.setVerticalGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel7Layout.createSequentialGroup().addContainerGap()
								.addComponent(jPanel14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jPanel15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 283,
												GroupLayout.PREFERRED_SIZE))
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout
								.createSequentialGroup().addComponent(jPanel7, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 14, Short.MAX_VALUE)));

		pack();
	}

	// Add
	private void add2ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "INSERT INTO DichVu(MADV,TENDV, GIADV) VALUES('" + jTextFieldServiceID.getText() + "'," + "'"
					+ jTextFieldServiceName.getText() + "','" + jTextFieldServicePrice.getText() + "')";

			st.execute(query);
			hienThiDanhSachDichVu();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Edit
	private void edit2ActionPerformed(ActionEvent evt) {
		if (jTableDichvu.getSelectedRow() == -1) {
			if (jTableDichvu.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			DefaultTableModel model = (DefaultTableModel) jTableDichvu.getModel();
			model.setValueAt(jTextFieldServiceID.getText(), jTableDichvu.getSelectedRow(), 0);
			model.setValueAt(jTextFieldServiceName.getText().toString(), jTableDichvu.getSelectedRow(), 1);
			model.setValueAt(jTextFieldServicePrice.getText(), jTableDichvu.getSelectedRow(), 2);
		}

	}

	// Delete
	private void delete2ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "DELETE FROM dichvu WHERE MADV = '" + jTextFieldServiceID.getText() + "'";
			st.executeUpdate(query);
			hienThiDanhSachDichVu();

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Exit
	private void exit2ActionPerformed(ActionEvent evt) {
		exit2.setToolTipText("Click to exit the program ");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}

	}

	// Clear
	private void jButtonclear1ActionPerformed(ActionEvent evt) {
		jTextFieldServiceID.setText("");
		jTextFieldServiceName.setText("");
		jTextFieldServicePrice.setText("");
		jTextFieldServiceID.requestFocus();
	}

	private void jTableServiceMouseClicked(MouseEvent evt) {
		int i = jTableDichvu.getSelectedRow();
		TableModel model = jTableDichvu.getModel();
		jTextFieldServiceID.setText(model.getValueAt(i, 0).toString());
		jTextFieldServiceName.setText(model.getValueAt(i, 1).toString());
		jTextFieldServicePrice.setText(model.getValueAt(i, 2).toString());

	}

	public static void main(String args[]) {
		// Giao diện Nimbus
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(ServiceForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(ServiceForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(ServiceForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(ServiceForm.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				ServiceForm frame = new ServiceForm();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
