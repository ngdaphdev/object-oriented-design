package qlks.Form;

import static qlks.MyConnection.getConnection;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import qlks.Bill;

public class BillForm extends JFrame {

	private JButton jButtonclear4;
	private JLabel jLabel31;
	private JLabel jLabel32;
	private JLabel jLabel33;
	private JLabel jLabel34;
	private JLabel jLabel35;
	private JLabel jLabel36;
	private JPanel jPanel20;
	private JPanel jPanel21;
	private JPanel jPanel22;
	private JPanel jPanel6;
	private JScrollPane jScrollPane5;
	private JTable jTableBill;
	private JTextField jTextFieldPrice;
	private JTextField jTextFieldBillID;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldDate;
	private JButton edit4;
	private JButton add4;
	private JButton exit4;
	private JButton delete4;

	public BillForm() {
		initComponents();
		getConnection();
		hienThiDanhSachHoaDon();
	}

	Connection con = null;
	Statement st = null;

	public ArrayList<Bill> layDanhSachHoaDon() {
		ArrayList<Bill> dshd = new ArrayList<Bill>();
		Connection con = getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM hoadon";
			// Thực thi câu lệnh truy vấn
			ResultSet rs = st.executeQuery(sql);

			Bill hd;
			while (rs.next()) {
				hd = new Bill(rs.getString("MAHD"), rs.getString("MANV"), rs.getString("MAPHONG"), rs.getDate("NGAY"),
						rs.getDouble("GIAHD"));

				// Thêm vào danh sách
				dshd.add(hd);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dshd;
	}

	public void hienThiDanhSachHoaDon() {
		String colTieuDe1[] = new String[] { "Bill ID", "Employee ID", "Room ID", "Date", "Price" };
		ArrayList<Bill> dshd = layDanhSachHoaDon();

		DefaultTableModel model = new DefaultTableModel(colTieuDe1, 0);

		Object[] row;

		for (int i = 0; i < dshd.size(); i++) {

			row = new Object[5];

			// GÁN GIÁ TRỊ
			row[0] = dshd.get(i).getBillID();
			row[1] = dshd.get(i).getEmployeeID();
			row[2] = dshd.get(i).getRoomID();
			row[3] = dshd.get(i).getDate();
			row[4] = dshd.get(i).getPrice();

			model.addRow(row);
		}

		// }catch(ArrayIndexOutOfBoundsException ex){

		jTableBill.setModel(model);

	}

	private void initComponents() {

		jPanel6 = new JPanel();
		jPanel20 = new JPanel();
		jLabel31 = new JLabel();
		jPanel21 = new JPanel();
		jLabel32 = new JLabel();
		jLabel33 = new JLabel();
		jLabel34 = new JLabel();
		jLabel35 = new JLabel();
		jLabel36 = new JLabel();
		jTextFieldBillID = new JTextField();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldRoomID = new JTextField();
		jTextFieldDate = new JTextField();
		jTextFieldPrice = new JTextField();
		add4 = new JButton();
		edit4 = new JButton();
		delete4 = new JButton();
		exit4 = new JButton();
		jButtonclear4 = new JButton();
		jPanel22 = new JPanel();
		jScrollPane5 = new JScrollPane();
		jTableBill = new JTable();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBackground(new Color(0, 204, 255));

		jPanel6.setBackground(new Color(0, 170, 255));
		jPanel20.setBackground(new Color(0, 170, 255));
		jLabel31.setFont(new Font("Tahoma", 0, 23));
		jLabel31.setText("List of Bill");

		GroupLayout jPanel20Layout = new GroupLayout(jPanel20);
		jPanel20.setLayout(jPanel20Layout);
		jPanel20Layout.setHorizontalGroup(jPanel20Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel20Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabel31, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
						.addGap(199, 199, 199)));
		jPanel20Layout.setVerticalGroup(jPanel20Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel20Layout.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jLabel31)));

		jPanel21.setBorder(BorderFactory.createTitledBorder("BillS Information"));

		jLabel32.setText("Bill ID ");

		jLabel33.setText("Employee ID ");

		jLabel34.setText("Room ID ");

		jLabel35.setText("Date ");

		jLabel36.setText("Price ");

		jTextFieldBillID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldBillIDActionPerformed(evt);
			}
		});

		jTextFieldRoomID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldRoomIDActionPerformed(evt);
			}
		});

		add4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add4.setText("Add");
		add4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				add4ActionPerformed(evt);
			}
		});

		edit4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit4.setText("Edit");
		edit4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				edit4ActionPerformed(evt);
			}
		});

		delete4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete4.setText("Delete");
		delete4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delete4ActionPerformed(evt);
			}
		});

		exit4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit4.setText("Exit");
		exit4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exit4ActionPerformed(evt);
			}
		});

		jButtonclear4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear4.setText("Clear");
		jButtonclear4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonclear4ActionPerformed(evt);
			}
		});

		GroupLayout jPanel21Layout = new GroupLayout(jPanel21);
		jPanel21.setLayout(jPanel21Layout);
		jPanel21Layout.setHorizontalGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel21Layout.createSequentialGroup().addGap(23, 23, 23)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel32).addComponent(jLabel33).addComponent(jLabel34)
								.addComponent(jLabel35).addComponent(jLabel36)
								.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addComponent(delete4, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(add4, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel21Layout.createSequentialGroup().addGap(13, 13, 13).addComponent(edit4)
										.addGap(18, 18, 18).addComponent(jButtonclear4))
								.addGroup(jPanel21Layout.createSequentialGroup().addGap(40, 40, 40)
										.addGroup(jPanel21Layout
												.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(jTextFieldPrice, GroupLayout.DEFAULT_SIZE, 124,
														Short.MAX_VALUE)
												.addComponent(jTextFieldDate).addComponent(jTextFieldRoomID)
												.addComponent(jTextFieldEmployeeID).addComponent(jTextFieldBillID)))
								.addGroup(
										jPanel21Layout.createSequentialGroup().addGap(32, 32, 32).addComponent(exit4)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel21Layout.setVerticalGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel21Layout.createSequentialGroup().addGap(26, 26, 26)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel32).addComponent(jTextFieldBillID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel33).addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel34).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel35).addComponent(jTextFieldDate, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel36).addComponent(jTextFieldPrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(34, 34, 34)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add4)
								.addComponent(edit4).addComponent(jButtonclear4))
						.addGap(26, 26, 26).addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete4).addComponent(exit4))
						.addContainerGap(32, Short.MAX_VALUE)));

		jTableBill
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jTableBill.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jTableHOADONMouseClicked(evt);
			}
		});
		jScrollPane5.setViewportView(jTableBill);

		GroupLayout jPanel22Layout = new GroupLayout(jPanel22);
		jPanel22.setLayout(jPanel22Layout);
		jPanel22Layout.setHorizontalGroup(jPanel22Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel22Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane5, GroupLayout.PREFERRED_SIZE, 402, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel22Layout.setVerticalGroup(jPanel22Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel22Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane5, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE).addContainerGap()));

		GroupLayout jPanel6Layout = new GroupLayout(jPanel6);
		jPanel6.setLayout(jPanel6Layout);
		jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel6Layout.createSequentialGroup()
						.addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel6Layout.createSequentialGroup().addGap(18, 18, 18)
										.addComponent(jPanel21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(42, 42, 42).addComponent(jPanel22, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel6Layout.createSequentialGroup().addGap(297, 297, 297).addComponent(
										jPanel20, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel6Layout.createSequentialGroup().addGap(30, 30, 30)
						.addComponent(jPanel20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(37, 37, 37)
						.addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addComponent(jPanel21, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(jPanel22, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 806, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(0, 5, Short.MAX_VALUE).addComponent(jPanel6,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 6, Short.MAX_VALUE))));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 490, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE).addComponent(jPanel6,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE))));

		pack();
	}

	private void jTextFieldBillIDActionPerformed(ActionEvent evt) {

	}

	private void jTextFieldRoomIDActionPerformed(ActionEvent evt) {

	}

	// Add
	private void add4ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "INSERT INTO hoadon(MAHD,MANV, MAPHONG, NGAY, GIAHD) VALUES('" + jTextFieldBillID.getText()
					+ "'," + "'" + jTextFieldEmployeeID.getText() + "','" + jTextFieldRoomID.getText() + "', '"
					+ jTextFieldDate.getText() + "', '" + jTextFieldPrice.getText() + "')";

			st.execute(query);
			hienThiDanhSachHoaDon();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Edit
	private void edit4ActionPerformed(ActionEvent evt) {
		if (jTableBill.getSelectedRow() == -1) {
			if (jTableBill.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			DefaultTableModel model = (DefaultTableModel) jTableBill.getModel();
			model.setValueAt(jTextFieldBillID.getText(), jTableBill.getSelectedRow(), 0);
			model.setValueAt(jTextFieldEmployeeID.getText().toString(), jTableBill.getSelectedRow(), 1);
			model.setValueAt(jTextFieldRoomID.getText(), jTableBill.getSelectedRow(), 2);
			model.setValueAt(jTextFieldDate.getText(), jTableBill.getSelectedRow(), 3);
			model.setValueAt(jTextFieldPrice.getText(), jTableBill.getSelectedRow(), 4);
		}

	}

	// Delete
	private void delete4ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "DELETE FROM hoadon WHERE MAHD = '" + jTextFieldBillID.getText() + "'";
			st.executeUpdate(query);
			hienThiDanhSachHoaDon();

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// exit
	private void exit4ActionPerformed(ActionEvent evt) {
		exit4.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	private void jButtonclear4ActionPerformed(ActionEvent evt) {
		jTextFieldBillID.setText("");
		jTextFieldEmployeeID.setText("");
		jTextFieldRoomID.setText("");
		jTextFieldDate.setText("");
		jTextFieldPrice.setText("");
		jTextFieldBillID.requestFocus();

	}

	private void jTableHOADONMouseClicked(MouseEvent evt) {
		int i = jTableBill.getSelectedRow();
		TableModel model = jTableBill.getModel();
		jTextFieldBillID.setText(model.getValueAt(i, 0).toString());
		jTextFieldEmployeeID.setText(model.getValueAt(i, 1).toString());
		jTextFieldRoomID.setText(model.getValueAt(i, 2).toString());
		jTextFieldDate.setText(model.getValueAt(i, 3).toString());
		jTextFieldPrice.setText(model.getValueAt(i, 4).toString());
	}

	public static void main(String args[]) {

		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(BillForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(BillForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(BillForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(BillForm.class.getName()).log(Level.SEVERE, null, ex);
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				BillForm frame = new BillForm();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
