package qlks.Form;

import static qlks.MyConnection.getConnection;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import qlks.Customer;

public class CustomerForm extends JFrame {
	private JButton jButtonclear2;
	private JLabel jLabel10;
	private JLabel jLabel11;
	private JLabel jLabel12;
	private JLabel jLabel13;
	private JLabel jLabel14;
	private JLabel jLabel15;
	private JLabel jLabel16;
	private JLabel jLabel17;
	private JLabel jLabel9;
	private JPanel jPanel11;
	private JPanel jPanel12;
	private JPanel jPanel4;
	private JScrollPane jScrollPane2;
	private JTable jTableCustomer;
	private JTextField jTextFieldCitizenID;
	private JTextField jTextFieldSex;
	private JTextField jTextFieldCustomerID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldNationality;
	private JTextField jTextFieldTel;
	private JTextField jTextFieldName;
	private JTextField jTextFieldAge;
	private JButton edit1;
	private JButton add1;
	private JButton exit1;
	private JButton delete1;

	public CustomerForm() {
		initComponents();
		getConnection();
		hienThiDanhSachKhachHang();
	}

	Connection con = null;
	Statement st = null;

	public ArrayList<Customer> layDanhSachKhachHang() {
		ArrayList<Customer> dskh = new ArrayList<Customer>();
		Connection con = getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM khachhang";
			// Thực thi câu lệnh truy vấn
			ResultSet rs = st.executeQuery(sql);

			Customer kh;
			while (rs.next()) {
				kh = new Customer(rs.getString("MAKH"), rs.getString("TENKH"), rs.getString("CMND"),
						rs.getString("QUOCTICH"), rs.getString("GIOITINH"), rs.getInt("TUOI"), rs.getString("SDT"),
						rs.getString("MAPHONG"));

				// Thêm vào danh sách
				dskh.add(kh);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dskh;
	}

	public void hienThiDanhSachKhachHang() {
		String colTieuDe1[] = new String[] { "Customer ID", "Name", "Citizen ID", "Nationality", "Sex", "Age", "Tel.",
				"Room ID" };
		ArrayList<Customer> dskh = layDanhSachKhachHang();

		DefaultTableModel model = new DefaultTableModel(colTieuDe1, 0);

		Object[] row;

		for (int i = 0; i < dskh.size(); i++) {

			row = new Object[8];

			// GÁN GIÁ TRỊ
			row[0] = dskh.get(i).getCustomerID();
			row[1] = dskh.get(i).getName();
			row[2] = dskh.get(i).getCitizenID();
			row[3] = dskh.get(i).getNationality();
			row[4] = dskh.get(i).getSex();
			row[5] = dskh.get(i).getAge();
			row[6] = dskh.get(i).getTel();
			row[7] = dskh.get(i).getRoomID();

			model.addRow(row);
		}

		// }catch(ArrayIndexOutOfBoundsException ex){

		jTableCustomer.setModel(model);

	}

	private void initComponents() {

		jPanel4 = new JPanel();
		jPanel12 = new JPanel();
		jLabel10 = new JLabel();
		jLabel11 = new JLabel();
		jLabel12 = new JLabel();
		jLabel13 = new JLabel();
		jLabel14 = new JLabel();
		jLabel15 = new JLabel();
		jLabel16 = new JLabel();
		jLabel17 = new JLabel();
		jTextFieldCustomerID = new JTextField();
		jTextFieldName = new JTextField();
		jTextFieldCitizenID = new JTextField();
		jTextFieldNationality = new JTextField();
		jTextFieldSex = new JTextField();
		jTextFieldAge = new JTextField();
		jTextFieldTel = new JTextField();
		jTextFieldRoomID = new JTextField();
		add1 = new JButton();
		edit1 = new JButton();
		delete1 = new JButton();
		exit1 = new JButton();
		jButtonclear2 = new JButton();
		jPanel11 = new JPanel();
		jLabel9 = new JLabel();
		jScrollPane2 = new JScrollPane();
		jTableCustomer = new JTable();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel4.setBackground(new Color(0, 170, 255));

		jPanel12.setBorder(BorderFactory.createTitledBorder("Customer Information"));

		jLabel10.setText("Customer ID ");

		jLabel11.setText("Name ");

		jLabel12.setText("Citizen ID ");

		jLabel13.setText("Nationality ");

		jLabel14.setText("Sex ");

		jLabel15.setText("Age ");

		jLabel16.setText("Telephone Number ");

		jLabel17.setText("Room ID ");

		jTextFieldCustomerID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldMAKHActionPerformed(evt);
			}
		});

		jTextFieldName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldTENKHActionPerformed(evt);
			}
		});

		jTextFieldRoomID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jTextFieldMAPHONGKHActionPerformed(evt);
			}
		});

		add1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add1.setText("Add");
		add1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				add1ActionPerformed(evt);
			}
		});

		edit1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit1.setText("Edit");
		edit1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				edit1ActionPerformed(evt);
			}
		});

		delete1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete1.setText("Delete");
		delete1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delete1ActionPerformed(evt);
			}
		});

		exit1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit1.setText("Exit");
		exit1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exit1ActionPerformed(evt);
			}
		});

		jButtonclear2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear2.setText("Clear");
		jButtonclear2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButtonclear2ActionPerformed(evt);
			}
		});

		GroupLayout jPanel12Layout = new GroupLayout(jPanel12);
		jPanel12.setLayout(jPanel12Layout);
		jPanel12Layout
				.setHorizontalGroup(
						jPanel12Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										jPanel12Layout
												.createSequentialGroup().addGap(22, 22, 22).addGroup(jPanel12Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(jPanel12Layout.createSequentialGroup().addGroup(
																jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING, false)
																		.addComponent(add1, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(delete1, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
																.addGap(32, 32, 32)
																.addGroup(jPanel12Layout.createParallelGroup(
																		GroupLayout.Alignment.LEADING).addComponent(
																				exit1)
																		.addGroup(jPanel12Layout.createSequentialGroup()
																				.addComponent(edit1).addGap(18, 18, 18)
																				.addComponent(jButtonclear2))))
														.addGroup(jPanel12Layout
																.createSequentialGroup().addGroup(jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jLabel10).addComponent(jLabel11)
																		.addComponent(jLabel12).addComponent(jLabel13)
																		.addComponent(jLabel14).addComponent(jLabel15)
																		.addComponent(jLabel16).addComponent(jLabel17))
																.addGap(18, 18, 18)
																.addGroup(jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jTextFieldNationality,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldCitizenID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldName,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldCustomerID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldSex,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldAge,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldTel,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldRoomID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE))))
												.addContainerGap(36, Short.MAX_VALUE)));
		jPanel12Layout.setVerticalGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel12Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel10).addComponent(jTextFieldCustomerID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel11).addComponent(jTextFieldName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel12).addComponent(jTextFieldCitizenID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel13).addComponent(jTextFieldNationality, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel14).addComponent(jTextFieldSex, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel15).addComponent(jTextFieldAge, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel16).addComponent(jTextFieldTel, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel17).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(41, 41, 41)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add1)
								.addComponent(edit1).addComponent(jButtonclear2))
						.addGap(28, 28, 28)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete1).addComponent(exit1))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jLabel9.setFont(new Font("Tahoma", 0, 23));
		jLabel9.setText("Customer Management");
		jPanel11.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel11Layout = new GroupLayout(jPanel11);
		jPanel11.setLayout(jPanel11Layout);
		jPanel11Layout.setHorizontalGroup(jPanel11Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel11Layout.createSequentialGroup().addGap(18, 18, 18)
						.addComponent(jLabel9, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE).addContainerGap()));
		jPanel11Layout.setVerticalGroup(jPanel11Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel11Layout.createSequentialGroup().addGap(27, 27, 27)
						.addComponent(jLabel9, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(27, Short.MAX_VALUE)));

		jTableCustomer
				.setModel(new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jTableCustomer.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				jTableCustomerMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(jTableCustomer);

		GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addGap(36, 36, 36)
						.addComponent(jPanel12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(27, 27, 27)
						.addGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 637, GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel4Layout.createSequentialGroup().addGap(162, 162, 162).addComponent(
										jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(22, Short.MAX_VALUE)));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addGap(27, 27, 27).addGroup(jPanel4Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(jPanel12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGroup(jPanel4Layout.createSequentialGroup()
								.addComponent(jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(22, 22, 22).addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 275,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1089, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup()
												.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addContainerGap())));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 461, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		pack();
	}

	private void jTextFieldMAKHActionPerformed(ActionEvent evt) {

	}

	private void jTextFieldTENKHActionPerformed(ActionEvent evt) {

	}

	private void jTextFieldMAPHONGKHActionPerformed(ActionEvent evt) {

	}

	// Add
	private void add1ActionPerformed(ActionEvent evt) {

		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "INSERT INTO khachhang(MAKH,TENKH, CMND, QUOCTICH, GIOITINH, TUOI, SDT, MAPHONG) VALUES('"
					+ jTextFieldCustomerID.getText() + "'," + "'" + jTextFieldName.getText() + "','"
					+ jTextFieldCitizenID.getText() + "', '" + jTextFieldNationality.getText() + "', '"
					+ jTextFieldSex.getText() + "', '" + jTextFieldAge.getText() + "', '" + jTextFieldTel.getText()
					+ "', '" + jTextFieldRoomID.getText() + "')";

			st.execute(query);
			hienThiDanhSachKhachHang();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Edit
	private void edit1ActionPerformed(ActionEvent evt) {

		if (jTableCustomer.getSelectedRow() == -1) {
			if (jTableCustomer.getRowCount() == 0) {

			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			DefaultTableModel model = (DefaultTableModel) jTableCustomer.getModel();
			model.setValueAt(jTextFieldCustomerID.getText(), jTableCustomer.getSelectedRow(), 0);
			model.setValueAt(jTextFieldName.getText().toString(), jTableCustomer.getSelectedRow(), 1);
			model.setValueAt(jTextFieldCitizenID.getText(), jTableCustomer.getSelectedRow(), 2);
			model.setValueAt(jTextFieldNationality.getText(), jTableCustomer.getSelectedRow(), 3);
			model.setValueAt(jTextFieldSex.getText(), jTableCustomer.getSelectedRow(), 4);
			model.setValueAt(jTextFieldAge.getText(), jTableCustomer.getSelectedRow(), 5);
			model.setValueAt(jTextFieldTel.getText(), jTableCustomer.getSelectedRow(), 6);
			model.setValueAt(jTextFieldRoomID.getText(), jTableCustomer.getSelectedRow(), 7);
		}
	}

	// Delete
	private void delete1ActionPerformed(ActionEvent evt) {
		Connection con = getConnection();
		try {
			// Tạo một đối tượng để thực hiện công việc
			st = (Statement) con.createStatement();
			String query = "DELETE FROM khachhang WHERE MAKH = '" + jTextFieldCustomerID.getText() + "'";
			st.executeUpdate(query);
			hienThiDanhSachKhachHang();

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Exit
	private void exit1ActionPerformed(ActionEvent evt) {
		exit1.setToolTipText("Click to exit the program ");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	// Clear
	private void jButtonclear2ActionPerformed(ActionEvent evt) {
		jTextFieldCustomerID.setText("");
		jTextFieldName.setText("");
		jTextFieldCitizenID.setText("");
		jTextFieldNationality.setText("");
		jTextFieldSex.setText("");
		jTextFieldAge.setText("");
		jTextFieldTel.setText("");
		jTextFieldRoomID.setText("");
		jTextFieldCustomerID.requestFocus();

	}

	private void jTableCustomerMouseClicked(MouseEvent evt) {
		int i = jTableCustomer.getSelectedRow();
		TableModel model = jTableCustomer.getModel();
		jTextFieldCustomerID.setText(model.getValueAt(i, 0).toString());
		jTextFieldName.setText(model.getValueAt(i, 1).toString());
		jTextFieldCitizenID.setText(model.getValueAt(i, 2).toString());
		jTextFieldNationality.setText(model.getValueAt(i, 3).toString());
		jTextFieldSex.setText(model.getValueAt(i, 4).toString());
		jTextFieldAge.setText(model.getValueAt(i, 5).toString());
		jTextFieldTel.setText(model.getValueAt(i, 6).toString());
		jTextFieldRoomID.setText(model.getValueAt(i, 7).toString());
	}

	public static void main(String args[]) {
		// Giao diện nimbus
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(CustomerForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(CustomerForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(CustomerForm.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(CustomerForm.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				CustomerForm frame = new CustomerForm();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
