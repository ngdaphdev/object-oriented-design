package qlks;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

public class Login extends JFrame {
	private JPasswordField password;
	private JButton jButton1;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JPanel jPanel1;
	private JTextField txtUser;

	public Login() {
		initComponents();
	}

	private void initComponents() {

		jPanel1 = new JPanel();
		txtUser = new JTextField();
		jButton1 = new JButton();
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		password = new JPasswordField();
		jLabel3 = new JLabel();
		jLabel4 = new JLabel();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLayout(null);

		jPanel1.setBackground(new Color(255, 255, 255));

		txtUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				txtUserActionPerformed(evt);
			}
		});

		jButton1.setFont(new Font("Tw Cen MT Condensed Extra Bold", 0, 18));
		jButton1.setText("LOGIN");
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jLabel1.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/user.png")));
		jLabel1.setText("      User");

		jLabel2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/pass.png")));
		jLabel2.setText("   Password");

		jLabel3.setFont(new Font("Tahoma", 0, 23));
		jLabel3.setText("Nong Lam Hotel");

		jLabel4.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/hotel.png")));

		GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(
						jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout
										.createSequentialGroup().addGap(127, 127, 127)
										.addComponent(jButton1, GroupLayout.PREFERRED_SIZE, 185,
												GroupLayout.PREFERRED_SIZE)
										.addGap(0, 0, Short.MAX_VALUE))
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(51, 51, 51)
										.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout
														.createSequentialGroup()
														.addComponent(
																jLabel4, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
														.addGap(18, 18, 18)
														.addComponent(
																jLabel3, GroupLayout.PREFERRED_SIZE, 194,
																GroupLayout.PREFERRED_SIZE)
														.addGap(42, 42, 42))
												.addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 107,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel1)).addGap(18, 18, 18)
														.addGroup(jPanel1Layout
																.createParallelGroup(GroupLayout.Alignment.LEADING,
																		false)
																.addComponent(password, GroupLayout.DEFAULT_SIZE, 125,
																		Short.MAX_VALUE)
																.addComponent(txtUser))
														.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(36, 36, 36).addComponent(jLabel3,
										GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(jLabel4,
										GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)))
						.addGap(18, 18, 18)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jLabel1)
								.addComponent(txtUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2).addComponent(password, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18).addComponent(jButton1).addGap(54, 54, 54)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel1,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}

	private void txtUserActionPerformed(ActionEvent evt) {

	}

	@SuppressWarnings("deprecation")
	private void jButton1ActionPerformed(ActionEvent evt) {

		if (txtUser.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please login your account");
		} else if (password.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please login password");
		} else if (txtUser.getText().equals("admin")) {
			if (password.getText().equals("admin")) {
				JOptionPane.showMessageDialog(this, "Logged in successfully");
				MenuForm ql = new MenuForm();
				ql.setVisible(true);
				ql.setResizable(false);
				this.setVisible(false);
			} else {
				JOptionPane.showMessageDialog(this, "Wrong password!!!");
			}
		} else {
			JOptionPane.showMessageDialog(this, "Login failed");
		}
	}

	public static void main(String args[]) {

		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				Login login = new Login();
				login.setVisible(true);
				login.setResizable(false);
				login.setLocationRelativeTo(null);
//				login.setSize(300, 300);

			}
		});
	}

}
