package qlks;

import java.sql.DriverManager;

public class MyConnection {
	public static java.sql.Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			String url = "jdbc:mysql://localhost:3306/QL_KHACHSAN?useSSL=false";
			Class.forName("com.mysql.jdbc.Driver");
			String username = "root";
			String password = "root";
			conn = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void closeConnection(java.sql.Connection c) {
		try {
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
