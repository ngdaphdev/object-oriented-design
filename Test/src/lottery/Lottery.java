package lottery;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Lottery {
	private JFrame mainFrame;
	private JLabel headerLabel;
	private JLabel statusLabel;
	private JPanel controlPanel;

	private String[] day = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
			"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
	private String[] month = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
	private String[] year = { "2018", "2019", "2020", "2021","2022" };

	private JComboBox<String> jcbD = new JComboBox<>(day);
	private JComboBox<String> jcbM = new JComboBox<>(month);
	private JComboBox<String> jcbY = new JComboBox<>(year);
	private JComboBox<String> jcbDT = new JComboBox<>(day);
	private JComboBox<String> jcbMT = new JComboBox<>(month);
	private JComboBox<String> jcbYT = new JComboBox<>(year);

	private JLabel jlseri = new JLabel("Serial number:");
	private JLabel jlday = new JLabel("DAY:");
	private JLabel jlmonth = new JLabel("MONTH:");
	private JLabel jlyear = new JLabel("YEAR:");
	private JButton jbClose = new JButton("Close");
	private JButton jbCheck = new JButton("Check");
	private JButton jbClear = new JButton("Clear");
	private JTextField jtfseri1 = new JTextField();
	private JTextField jtfseri2 = new JTextField();
	private JTextField jtfseri3 = new JTextField();
	private JTextField jtfseri4 = new JTextField();
	private JTextField jtfseri5 = new JTextField();
	private JTextField jtfseri6 = new JTextField();

	private JLabel jlseriT = new JLabel("Serial number:");
	private JLabel jldayT = new JLabel("DAY:");
	private JLabel jlmonthT = new JLabel("MONTH:");
	private JLabel jlyearT = new JLabel("YEAR:");
	private JButton jbCheckT = new JButton("Check");
	private JButton jbCloseT = new JButton("Close");
	private JButton jbClearT = new JButton("Clear");
	private JTextField jtfseriT = new JTextField();
	public Component rootPane;

	public Lottery() {
		prepareGUI();
	}

	public static void main(String[] args) {
		Lottery demo = new Lottery();

	}

	// CLOSE CLASSIC AND DIGITAL
	private class ExistAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);

		}
	}

	// CLEAR CLASSIC LOTTERY
	private class ClearAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			jtfseriT.setText("");

		}

	}

	// CLEAR DIGITAL LOTTERY
	private class ClearAction2 implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			jtfseri1.setText("");
			jtfseri2.setText("");
			jtfseri3.setText("");
			jtfseri4.setText("");
			jtfseri5.setText("");
			jtfseri6.setText("");

		}

	}

	// RANDOM CLASSIC
	public static String getRandom6() {
		Random ran = new Random();
		Calendar calendar = Calendar.getInstance();
		calendar.getTime();
		String result = "";
		for (int i = 0; i < 6; i++) {
			int k = ran.nextInt(10);
			result += k + "";
		}
		return result;
	}

	// CHECK CLASSIC LOTTERY
	private class PressButtonAction implements ActionListener {

		@SuppressWarnings("unlikely-arg-type")
		@Override
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = Calendar.getInstance();
			String mySerial = jtfseriT.getText();
			String date = jcbDT.getName();
			String month = jcbMT.getName();
			String year = jcbYT.getName();
			// Entry conditions
			if (mySerial.length() != 6) {
				JOptionPane.showMessageDialog(rootPane,
						"The serial number you entered is incorrect (6 serial numbers)!!!");
			}

			String t61 = getRandom6().substring(getRandom6().length() - 6);
			String t62 = mySerial.substring(mySerial.length() - 6);

			String t51 = getRandom6().substring(getRandom6().length() - 5);
			String t52 = mySerial.substring(mySerial.length() - 5);

			String t41 = getRandom6().substring(getRandom6().length() - 4);
			String t42 = mySerial.substring(mySerial.length() - 4);
			// check special prize
			if (t61.equals(t62) && date.equals(calendar.get(Calendar.DATE))
					&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the special prize (2 billion VND)");
				// check first prize
			} else if (t51.equals(t52) && date.equals(calendar.get(Calendar.DATE))
					&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (30 million VND)");
				// check secord prize
			} else if (t41.equals(t42) && date.equals(calendar.get(Calendar.DATE))
					&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the second prize (15 million VND)");

			}
			// Did not win the prize
			else {
				JOptionPane.showMessageDialog(rootPane, " Wish you luck next time");
			}

		}

	}

	// RANDOM DIGITAL LOTTERY 1
	public static String getRandomMega1_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// RANDOM DIGITAL LOTTERY 2
	public static String getRandomMega2_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// RANDOM DIGITAL LOTTERY 3
	public static String getRandomMega3_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// RANDOM DIGITAL LOTTERY 4
	public static String getRandomMega4_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// RANDOM DIGITAL LOTTERY 5
	public static String getRandomMega5_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// RANDOM DIGITAL LOTTERY 6
	public static String getRandomMega6_45() {
		Random ran = new Random();
		String result = "";
		for (int i = 0; i < 1; i++) {
			int k = ran.nextInt(45);
			if (k >= 10) {
				result += k + " ";

			} else
				result += "0" + k + " ";

		}

		return result;
	}

	// CHECK DIGITAL LOTTERY
	private class PressButtonAction2 implements ActionListener {

		@SuppressWarnings("unlikely-arg-type")
		@Override
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = Calendar.getInstance();
			String mySerial1 = jtfseri1.getText();
			String mySerial2 = jtfseri2.getText();
			String mySerial3 = jtfseri3.getText();
			String mySerial4 = jtfseri4.getText();
			String mySerial5 = jtfseri5.getText();
			String mySerial6 = jtfseri6.getText();
			String date = jcbD.getName();
			String month = jcbM.getName();
			String year = jcbY.getName();
			String t1 = getRandomMega1_45();
			String t2 = getRandomMega2_45();
			String t3 = getRandomMega3_45();
			String t4 = getRandomMega4_45();
			String t5 = getRandomMega5_45();
			String t6 = getRandomMega6_45();
			// Entry conditions
			if (mySerial1.length() != 2 || mySerial2.length() != 2 || mySerial3.length() != 2 || mySerial4.length() != 2
					|| mySerial5.length() != 2 || mySerial6.length() != 2) {
				JOptionPane.showMessageDialog(rootPane,
						"The serial number you entered is incorrect (pair of number)!!!");
			}
			// check special prize
			else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3)
					|| t3.equals(mySerial4) || t3.equals(mySerial5) || t3.equals(mySerial6) && t4.equals(mySerial1)
					|| t4.equals(mySerial2) || t4.equals(mySerial3) || t4.equals(mySerial4) || t4.equals(mySerial5)
					|| t4.equals(mySerial6) && t5.equals(mySerial1) || t5.equals(mySerial2) || t5.equals(mySerial3)
					|| t5.equals(mySerial4) || t5.equals(mySerial5) || t5.equals(mySerial6) && t6.equals(mySerial1)
					|| t6.equals(mySerial2) || t6.equals(mySerial3) || t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane,
						"Congratulations on winning the special prize (60 billion VND)");
				// check first prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3)
					|| t3.equals(mySerial4) || t3.equals(mySerial5) || t3.equals(mySerial6) && t4.equals(mySerial1)
					|| t4.equals(mySerial2) || t4.equals(mySerial3) || t4.equals(mySerial4) || t4.equals(mySerial5)
					|| t4.equals(mySerial6) && t5.equals(mySerial1) || t5.equals(mySerial2) || t5.equals(mySerial3)
					|| t5.equals(mySerial4) || t5.equals(mySerial5)
					|| t5.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check first prize
			} else if (t2.equals(mySerial1) || t2.equals(mySerial2) || t2.equals(mySerial3) || t2.equals(mySerial4)
					|| t2.equals(mySerial5) || t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2)
					|| t3.equals(mySerial3) || t3.equals(mySerial4) || t3.equals(mySerial5)
					|| t3.equals(mySerial6) && t4.equals(mySerial1) || t4.equals(mySerial2) || t4.equals(mySerial3)
					|| t4.equals(mySerial4) || t4.equals(mySerial5) || t4.equals(mySerial6) && t5.equals(mySerial1)
					|| t5.equals(mySerial2) || t5.equals(mySerial3) || t5.equals(mySerial4) || t5.equals(mySerial5)
					|| t5.equals(mySerial6) && t6.equals(mySerial1) || t6.equals(mySerial2) || t6.equals(mySerial3)
					|| t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check first prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2)
					|| t3.equals(mySerial3) || t3.equals(mySerial4) || t3.equals(mySerial5)
					|| t3.equals(mySerial6) && t4.equals(mySerial1) || t4.equals(mySerial2) || t4.equals(mySerial3)
					|| t4.equals(mySerial4) || t4.equals(mySerial5) || t4.equals(mySerial6) && t5.equals(mySerial1)
					|| t5.equals(mySerial2) || t5.equals(mySerial3) || t5.equals(mySerial4) || t5.equals(mySerial5)
					|| t5.equals(mySerial6) && t6.equals(mySerial1) || t6.equals(mySerial2) || t6.equals(mySerial3)
					|| t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check first prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t4.equals(mySerial1) || t4.equals(mySerial2) || t4.equals(mySerial3)
					|| t4.equals(mySerial4) || t4.equals(mySerial5) || t4.equals(mySerial6) && t5.equals(mySerial1)
					|| t5.equals(mySerial2) || t5.equals(mySerial3) || t5.equals(mySerial4) || t5.equals(mySerial5)
					|| t5.equals(mySerial6) && t6.equals(mySerial1) || t6.equals(mySerial2) || t6.equals(mySerial3)
					|| t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check first prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3)
					|| t3.equals(mySerial4) || t3.equals(mySerial5) || t3.equals(mySerial6) && t5.equals(mySerial1)
					|| t5.equals(mySerial2) || t5.equals(mySerial3) || t5.equals(mySerial4) || t5.equals(mySerial5)
					|| t5.equals(mySerial6) && t6.equals(mySerial1) || t6.equals(mySerial2) || t6.equals(mySerial3)
					|| t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check first prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3)
					|| t3.equals(mySerial4) || t3.equals(mySerial5) || t3.equals(mySerial6) && t4.equals(mySerial1)
					|| t4.equals(mySerial2) || t4.equals(mySerial3) || t4.equals(mySerial4) || t4.equals(mySerial5)
					|| t4.equals(mySerial6) && t6.equals(mySerial1) || t6.equals(mySerial2) || t6.equals(mySerial3)
					|| t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the first prize (10 million VND)");
				// check secord prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5)
					|| t2.equals(mySerial6) && t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3)
					|| t3.equals(mySerial4) || t3.equals(mySerial5) || t3.equals(mySerial6) || t4.equals(mySerial1)
					|| t4.equals(mySerial2) || t4.equals(mySerial3) || t4.equals(mySerial4) || t4.equals(mySerial5)
					|| t4.equals(mySerial6) || t5.equals(mySerial1) || t5.equals(mySerial2) || t5.equals(mySerial3)
					|| t5.equals(mySerial4) || t5.equals(mySerial5) || t5.equals(mySerial6) || t6.equals(mySerial1)
					|| t6.equals(mySerial2) || t6.equals(mySerial3) || t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane,
						"Congratulations on winning the second prize (300 thousand VND)");
				// check third prize
			} else if (t1.equals(mySerial1) || t1.equals(mySerial2) || t1.equals(mySerial3) || t1.equals(mySerial4)
					|| t1.equals(mySerial5) || t1.equals(mySerial6) && t2.equals(mySerial1) || t2.equals(mySerial2)
					|| t2.equals(mySerial3) || t2.equals(mySerial4) || t2.equals(mySerial5) || t2.equals(mySerial6)
					|| t3.equals(mySerial1) || t3.equals(mySerial2) || t3.equals(mySerial3) || t3.equals(mySerial4)
					|| t3.equals(mySerial5) || t3.equals(mySerial6) || t4.equals(mySerial1) || t4.equals(mySerial2)
					|| t4.equals(mySerial3) || t4.equals(mySerial4) || t4.equals(mySerial5) || t4.equals(mySerial6)
					|| t5.equals(mySerial1) || t5.equals(mySerial2) || t5.equals(mySerial3) || t5.equals(mySerial4)
					|| t5.equals(mySerial5) || t5.equals(mySerial6) || t6.equals(mySerial1) || t6.equals(mySerial2)
					|| t6.equals(mySerial3) || t6.equals(mySerial4) || t6.equals(mySerial5)
					|| t6.equals(mySerial6) && date.equals(calendar.get(Calendar.DATE))
							&& month.equals(calendar.get(Calendar.MONTH)) && year.equals(calendar.get(Calendar.YEAR))) {
				JOptionPane.showMessageDialog(rootPane, "Congratulations on winning the third prize (30 thousand VND)");
			}

			else {
				JOptionPane.showMessageDialog(rootPane, " Wish you luck next time");
			}

		}

	}

	// Window main
	private void prepareGUI() {
		mainFrame = new JFrame("LOTTERY");
		mainFrame.setSize(500, 300);
		mainFrame.setLayout(new GridLayout(3, 1));
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		headerLabel = new JLabel("", JLabel.CENTER);
		statusLabel = new JLabel("", JLabel.CENTER);
		statusLabel.setSize(350, 100);

		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());
		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(statusLabel);
		mainFrame.setVisible(true);
		headerLabel.setText("Choose lottery type : ");
		showClassic();
		showDigital();
	}

	// window classic lottery
	private void showClassic() {
		JFrame frame = new JFrame("CLASSIC LOTTERY");
		frame.setSize(500, 430);

		jlseriT.setBounds(20, 80, 150, 30);
		jtfseriT.setBounds(180, 80, 250, 30);
		jldayT.setBounds(20, 200, 150, 30);
		jcbDT.setBounds(70, 200, 50, 30);
		jlmonthT.setBounds(180, 200, 150, 30);
		jcbMT.setBounds(250, 200, 50, 30);
		jlyearT.setBounds(360, 200, 150, 30);
		jcbYT.setBounds(410, 200, 70, 30);
		jbCheckT.setBounds(263, 300, 80, 30);
		jbCloseT.setBounds(380, 300, 80, 30);
		jbClearT.setBounds(150, 300, 80, 30);
		jtfseriT.setDocument(new JTextFieldLimit(6));// giới hạn kí tự nhập trong jtextfield

		frame.add(jlseriT);
		frame.add(jtfseriT);
		frame.add(jldayT);
		frame.add(jcbDT);
		frame.add(jlmonthT);
		frame.add(jcbMT);
		frame.add(jlyearT);
		frame.add(jcbYT);
		frame.add(jbCloseT);
		frame.add(jbCheckT);
		frame.add(jbClearT);

		frame.setLayout(null);
		frame.setResizable(false);

		frame.setLocationRelativeTo(null);
		// CLEAR
		jbClearT.addActionListener(new ClearAction());
		// CLOSE
		jbCloseT.addActionListener(new ExistAction());
		// CHECK
		jbCheckT.addActionListener(new PressButtonAction());

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				frame.dispose();
			}
		});
		JButton btClassic = new JButton("Classic lottery");
		btClassic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				statusLabel.setText("CLASSIC LOTTERY IS OPEN");// khi mở CLASSIC LOTTERY sẽ hiện dòng chữ này ở màn hình
																// chính
				frame.setVisible(true);
			}
		});
		controlPanel.add(btClassic);
		mainFrame.setVisible(true);
	}

	// window Digital lottery
	private void showDigital() {

		JFrame frame2 = new JFrame("DIGITAL LOTTERY");
		frame2.setSize(500, 430);
		frame2.setLocationRelativeTo(null);

		jlseri.setBounds(20, 80, 150, 30);
		jtfseri1.setBounds(150, 80, 30, 30);
		jtfseri2.setBounds(200, 80, 30, 30);
		jtfseri3.setBounds(250, 80, 30, 30);
		jtfseri4.setBounds(300, 80, 30, 30);
		jtfseri5.setBounds(350, 80, 30, 30);
		jtfseri6.setBounds(400, 80, 30, 30);
		jtfseri1.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield
		jtfseri2.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield
		jtfseri3.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield
		jtfseri4.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield
		jtfseri5.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield
		jtfseri6.setDocument(new JTextFieldLimit(2));// giới hạn kí tự nhập trong jtextfield

		jlday.setBounds(20, 200, 150, 30);
		jcbD.setBounds(70, 200, 50, 30);
		jlmonth.setBounds(180, 200, 150, 30);
		jcbM.setBounds(250, 200, 50, 30);
		jlyear.setBounds(360, 200, 150, 30);
		jcbY.setBounds(410, 200, 70, 30);
		jbCheck.setBounds(263, 300, 80, 30);
		jbClose.setBounds(380, 300, 80, 30);
		jbClear.setBounds(150, 300, 80, 30);
		frame2.add(jlseri);
		frame2.add(jtfseri1);
		frame2.add(jtfseri2);
		frame2.add(jtfseri3);
		frame2.add(jtfseri4);
		frame2.add(jtfseri5);
		frame2.add(jtfseri6);
		frame2.add(jlday);
		frame2.add(jcbD);
		frame2.add(jlmonth);
		frame2.add(jcbM);
		frame2.add(jlyear);
		frame2.add(jcbY);
		frame2.add(jbClose);
		frame2.add(jbCheck);
		frame2.add(jbClear);
		frame2.setLayout(null);
		frame2.setResizable(false);
		// CLEAR
		jbClear.addActionListener(new ClearAction2());
		// CLOSE
		jbClose.addActionListener(new ExistAction());
		// CHECK
		jbCheck.addActionListener(new PressButtonAction2());

		frame2.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				frame2.dispose();
			}
		});
		JButton btDigital = new JButton("Digital lottery");
		btDigital.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				statusLabel.setText("DIGITAL LOTTERY IS OPEN");// khi mở DIGITAL LOTTERY sẽ hiện dòng chữ này ở màn hình
																// chính
				frame2.setVisible(true);
			}
		});
		controlPanel.add(btDigital);
		mainFrame.setVisible(true);
	}

}
