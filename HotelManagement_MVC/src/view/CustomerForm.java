package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.*;

import different.DisplayGUI;
import model.customer.MCustomerTable;

@SuppressWarnings("serial")
public class CustomerForm extends JFrame {
	private JButton jButtonclear2;
	private JLabel jLabel10;
	private JLabel jLabel11;
	private JLabel jLabel12;
	private JLabel jLabel13;
	private JLabel jLabel14;
	private JLabel jLabel15;
	private JLabel jLabel16;
	private JLabel jLabel17;
	private JLabel jLabel9;
	private JPanel jPanel11;
	private JPanel jPanel12;
	private JPanel jPanel4;
	private JScrollPane jScrollPane2;
	private JTable jTableCustomer;
	private JTextField jTextFieldCitizenID;
	private JTextField jTextFieldSex;
	private JTextField jTextFieldCustomerID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldNationality;
	private JTextField jTextFieldTel;
	private JTextField jTextFieldName;
	private JTextField jTextFieldAge;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;
	private MCustomerTable mCustomerTable = new MCustomerTable();

	public CustomerForm() {
		initComponents();
	}

	private void initComponents() {

		jPanel4 = new JPanel();
		jPanel12 = new JPanel();
		jLabel10 = new JLabel();
		jLabel11 = new JLabel();
		jLabel12 = new JLabel();
		jLabel13 = new JLabel();
		jLabel14 = new JLabel();
		jLabel15 = new JLabel();
		jLabel16 = new JLabel();
		jLabel17 = new JLabel();
		jTextFieldCustomerID = new JTextField();
		jTextFieldName = new JTextField();
		jTextFieldCitizenID = new JTextField();
		jTextFieldNationality = new JTextField();
		jTextFieldSex = new JTextField();
		jTextFieldAge = new JTextField();
		jTextFieldTel = new JTextField();
		jTextFieldRoomID = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jButtonclear2 = new JButton();
		jPanel11 = new JPanel();
		jLabel9 = new JLabel();
		jScrollPane2 = new JScrollPane();
		jTableCustomer = new JTable();

		DisplayGUI d = new DisplayGUI("Form Customer");
		d.display(this, jPanel4);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel4.setBackground(new Color(0, 170, 255));

		jPanel12.setBorder(BorderFactory.createTitledBorder("Customer Information"));

		jLabel10.setText("Customer ID ");

		jLabel11.setText("Name ");

		jLabel12.setText("Citizen ID ");

		jLabel13.setText("Nationality ");

		jLabel14.setText("Sex ");

		jLabel15.setText("Age ");

		jLabel16.setText("Telephone Number ");

		jLabel17.setText("Room ID ");

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");

		jButtonclear2.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear2.setText("Clear");

		GroupLayout jPanel12Layout = new GroupLayout(jPanel12);
		jPanel12.setLayout(jPanel12Layout);
		jPanel12Layout
				.setHorizontalGroup(
						jPanel12Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(
										jPanel12Layout
												.createSequentialGroup().addGap(22, 22, 22).addGroup(jPanel12Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(jPanel12Layout.createSequentialGroup().addGroup(
																jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING, false)
																		.addComponent(add, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(delete, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
																.addGap(32, 32, 32)
																.addGroup(jPanel12Layout.createParallelGroup(
																		GroupLayout.Alignment.LEADING).addComponent(
																				exit)
																		.addGroup(jPanel12Layout.createSequentialGroup()
																				.addComponent(edit).addGap(18, 18, 18)
																				.addComponent(jButtonclear2))))
														.addGroup(jPanel12Layout
																.createSequentialGroup().addGroup(jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jLabel10).addComponent(jLabel11)
																		.addComponent(jLabel12).addComponent(jLabel13)
																		.addComponent(jLabel14).addComponent(jLabel15)
																		.addComponent(jLabel16).addComponent(jLabel17))
																.addGap(18, 18, 18)
																.addGroup(jPanel12Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jTextFieldNationality,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldCitizenID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldName,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldCustomerID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldSex,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldAge,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldTel,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(jTextFieldRoomID,
																				GroupLayout.PREFERRED_SIZE, 84,
																				GroupLayout.PREFERRED_SIZE))))
												.addContainerGap(36, Short.MAX_VALUE)));
		jPanel12Layout.setVerticalGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel12Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel10).addComponent(jTextFieldCustomerID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel11).addComponent(jTextFieldName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel12).addComponent(jTextFieldCitizenID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel13).addComponent(jTextFieldNationality, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel14).addComponent(jTextFieldSex, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel15).addComponent(jTextFieldAge, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel16).addComponent(jTextFieldTel, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel17).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(41, 41, 41)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear2))
						.addGap(28, 28, 28)
						.addGroup(jPanel12Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jLabel9.setFont(new Font("Tahoma", 0, 23));
		jLabel9.setText("Customer Management");
		jPanel11.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel11Layout = new GroupLayout(jPanel11);
		jPanel11.setLayout(jPanel11Layout);
		jPanel11Layout.setHorizontalGroup(jPanel11Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel11Layout.createSequentialGroup().addGap(18, 18, 18)
						.addComponent(jLabel9, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE).addContainerGap()));
		jPanel11Layout.setVerticalGroup(jPanel11Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel11Layout.createSequentialGroup().addGap(27, 27, 27)
						.addComponent(jLabel9, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(27, Short.MAX_VALUE)));

		jTableCustomer.setModel(mCustomerTable);

		jScrollPane2.setViewportView(jTableCustomer);

		GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addGap(36, 36, 36)
						.addComponent(jPanel12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(27, 27, 27)
						.addGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 637, GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel4Layout.createSequentialGroup().addGap(162, 162, 162).addComponent(
										jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(22, Short.MAX_VALUE)));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addGap(27, 27, 27).addGroup(jPanel4Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(jPanel12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGroup(jPanel4Layout.createSequentialGroup()
								.addComponent(jPanel11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(22, 22, 22).addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 275,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1089, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup()
												.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addContainerGap())));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 461, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		pack();
	}

	public JTextField getjTextFieldCitizenID() {
		return jTextFieldCitizenID;
	}

	public void setjTextFieldCitizenID(JTextField jTextFieldCitizenID) {
		this.jTextFieldCitizenID = jTextFieldCitizenID;
	}

	public JTextField getjTextFieldSex() {
		return jTextFieldSex;
	}

	public void setjTextFieldSex(JTextField jTextFieldSex) {
		this.jTextFieldSex = jTextFieldSex;
	}

	public JTextField getjTextFieldCustomerID() {
		return jTextFieldCustomerID;
	}

	public void setjTextFieldCustomerID(JTextField jTextFieldCustomerID) {
		this.jTextFieldCustomerID = jTextFieldCustomerID;
	}

	public JTextField getjTextFieldRoomID() {
		return jTextFieldRoomID;
	}

	public void setjTextFieldRoomID(JTextField jTextFieldRoomID) {
		this.jTextFieldRoomID = jTextFieldRoomID;
	}

	public JTextField getjTextFieldNationality() {
		return jTextFieldNationality;
	}

	public void setjTextFieldNationality(JTextField jTextFieldNationality) {
		this.jTextFieldNationality = jTextFieldNationality;
	}

	public JTextField getjTextFieldTel() {
		return jTextFieldTel;
	}

	public void setjTextFieldTel(JTextField jTextFieldTel) {
		this.jTextFieldTel = jTextFieldTel;
	}

	public JTextField getjTextFieldName() {
		return jTextFieldName;
	}

	public void setjTextFieldName(JTextField jTextFieldName) {
		this.jTextFieldName = jTextFieldName;
	}

	public JTextField getjTextFieldAge() {
		return jTextFieldAge;
	}

	public void setjTextFieldAge(JTextField jTextFieldAge) {
		this.jTextFieldAge = jTextFieldAge;
	}

	public JButton getjButtonclear() {
		return jButtonclear2;
	}

	public JButton getEdit() {
		return edit;
	}

	public JButton getAdd() {
		return add;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getDelete() {
		return delete;
	}

	public JTable getjTableCustomer() {
		return jTableCustomer;
	}

	// exit
	public void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	public void setTableModel(MCustomerTable tableModel) {
		jTableCustomer.setModel(tableModel);

	}
}
