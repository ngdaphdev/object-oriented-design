package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.*;

import different.DisplayGUI;
import model.service.MServiceTable;

@SuppressWarnings("serial")
public class ServiceForm extends JFrame {
	private JButton jButtonclear;
	private JLabel jLabel18;
	private JLabel jLabel19;
	private JLabel jLabel20;
	private JLabel jLabel21;
	private JPanel jPanel14;
	private JPanel jPanel15;
	private JPanel jPanel7;
	private JScrollPane jScrollPane3;
	private JTable jTableDichvu;
	private JTextField jTextFieldServicePrice;
	private JTextField jTextFieldServiceID;
	private JTextField jTextFieldServiceName;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;
	private MServiceTable mServiceTable = new MServiceTable();

	public ServiceForm() {
		initComponents();
	}

	private void initComponents() {

		jPanel7 = new JPanel();
		jPanel14 = new JPanel();
		jLabel18 = new JLabel();
		jPanel15 = new JPanel();
		jLabel19 = new JLabel();
		jLabel20 = new JLabel();
		jLabel21 = new JLabel();
		jTextFieldServiceID = new JTextField();
		jTextFieldServiceName = new JTextField();
		jTextFieldServicePrice = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jButtonclear = new JButton();
		jScrollPane3 = new JScrollPane();
		jTableDichvu = new JTable();

		DisplayGUI d = new DisplayGUI("Form Service");
		d.display(this, jPanel15);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel7.setBackground(new Color(0, 170, 255));
		jPanel14.setBackground(new Color(0, 170, 255));
		jLabel18.setFont(new Font("Tahoma", 0, 23));
		jLabel18.setText("Service Management");

		GroupLayout jPanel14Layout = new GroupLayout(jPanel14);
		jPanel14.setLayout(jPanel14Layout);
		jPanel14Layout.setHorizontalGroup(jPanel14Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel14Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabel18, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
						.addGap(87, 87, 87)));
		jPanel14Layout.setVerticalGroup(jPanel14Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel14Layout.createSequentialGroup().addContainerGap()
						.addComponent(jLabel18, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jPanel15.setBorder(BorderFactory.createTitledBorder("Service information"));

		jLabel19.setText("Service ID");

		jLabel20.setText("Service Name");

		jLabel21.setText("Service Price");

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");

		jButtonclear.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear.setText("Clear");

		GroupLayout jPanel15Layout = new GroupLayout(jPanel15);
		jPanel15.setLayout(jPanel15Layout);
		jPanel15Layout.setHorizontalGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel15Layout.createSequentialGroup().addGap(26, 26, 26)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addComponent(jLabel21).addComponent(jLabel20).addComponent(jLabel19))
								.addComponent(add, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(delete, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel15Layout.createSequentialGroup().addGap(25, 25, 25).addGroup(
										jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(jTextFieldServiceID, GroupLayout.DEFAULT_SIZE, 72,
														Short.MAX_VALUE)
												.addComponent(jTextFieldServiceName)
												.addComponent(jTextFieldServicePrice)))
								.addGroup(jPanel15Layout.createSequentialGroup().addGap(18, 18, 18)
										.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(exit)
												.addGroup(jPanel15Layout.createSequentialGroup()
														.addComponent(edit, GroupLayout.PREFERRED_SIZE, 72,
																GroupLayout.PREFERRED_SIZE)
														.addGap(18, 18, 18).addComponent(jButtonclear)))))
						.addContainerGap(31, Short.MAX_VALUE)));
		jPanel15Layout.setVerticalGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel15Layout.createSequentialGroup().addGap(21, 21, 21)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel19).addComponent(jTextFieldServiceID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel20).addComponent(jTextFieldServiceName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(23, 23, 23)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel21).addComponent(jTextFieldServicePrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(31, 31, 31)
						.addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear))
						.addGap(18, 18, 18).addGroup(jPanel15Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(43, Short.MAX_VALUE)));

		jTableDichvu.setModel(mServiceTable);

		jScrollPane3.setViewportView(jTableDichvu);

		GroupLayout jPanel7Layout = new GroupLayout(jPanel7);
		jPanel7.setLayout(jPanel7Layout);
		jPanel7Layout.setHorizontalGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel7Layout.createSequentialGroup().addGroup(jPanel7Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel7Layout.createSequentialGroup().addGap(46, 46, 46)
								.addComponent(jPanel15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jScrollPane3,
										GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel7Layout.createSequentialGroup().addGap(238, 238, 238).addComponent(jPanel14,
								GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(24, Short.MAX_VALUE)));
		jPanel7Layout
				.setVerticalGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel7Layout.createSequentialGroup().addContainerGap()
								.addComponent(jPanel14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel7Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jPanel15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 283,
												GroupLayout.PREFERRED_SIZE))
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout
								.createSequentialGroup().addComponent(jPanel7, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 14, Short.MAX_VALUE)));

		pack();
	}

	public JTable getjTableDichvu() {
		return jTableDichvu;
	}

	public void setjTableDichvu(JTable jTableDichvu) {
		this.jTableDichvu = jTableDichvu;
	}

	public JTextField getjTextFieldServicePrice() {
		return jTextFieldServicePrice;
	}

	public void setjTextFieldServicePrice(JTextField jTextFieldServicePrice) {
		this.jTextFieldServicePrice = jTextFieldServicePrice;
	}

	public JTextField getjTextFieldServiceID() {
		return jTextFieldServiceID;
	}

	public void setjTextFieldServiceID(JTextField jTextFieldServiceID) {
		this.jTextFieldServiceID = jTextFieldServiceID;
	}

	public JTextField getjTextFieldServiceName() {
		return jTextFieldServiceName;
	}

	public void setjTextFieldServiceName(JTextField jTextFieldServiceName) {
		this.jTextFieldServiceName = jTextFieldServiceName;
	}

	public JButton getEdit() {
		return edit;
	}

	public JButton getAdd() {
		return add;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getDelete() {
		return delete;
	}

	public JButton getjButtonclear() {
		return jButtonclear;
	}

	// exit
	public void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	public void setTableModel(MServiceTable tableModel) {
		jTableDichvu.setModel(tableModel);
	}

}
