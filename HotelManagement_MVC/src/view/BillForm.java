package view;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

import different.DisplayGUI;
import model.bill.MBillTable;

@SuppressWarnings("serial")
public class BillForm extends JFrame {
	private JButton jButtonclear;
	private JPanel jPanel20;
	private JPanel jPanel21;
	private JPanel jPanel22;
	private JPanel jPanel6;
	private JScrollPane jScrollPane5;
	private JTable jTableBill;
	private JTextField jTextFieldPrice;
	private JTextField jTextFieldBillID;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldDate;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;

	public BillForm() {
		initComponents();
	}

	private void initComponents() {

		jPanel6 = new JPanel();
		jPanel20 = new JPanel();
		jPanel21 = new JPanel();
		jTextFieldBillID = new JTextField();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldRoomID = new JTextField();
		jTextFieldDate = new JTextField();
		jTextFieldPrice = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jButtonclear = new JButton();
		jPanel22 = new JPanel();
		jScrollPane5 = new JScrollPane();
		jTableBill = new JTable();

		DisplayGUI d = new DisplayGUI("Form Bill");
		d.display(this, jPanel20);
		setBackground(new Color(0, 204, 255));

		jPanel6.setBackground(new Color(0, 170, 255));
		jPanel20.setBackground(new Color(0, 170, 255));

		JLabel jLabelTitle = new JLabel();
		jLabelTitle.setFont(new Font("Tahoma", 0, 23));
		jLabelTitle.setText("List of Bill");

		GroupLayout jPanel20Layout = new GroupLayout(jPanel20);
		jPanel20.setLayout(jPanel20Layout);
		jPanel20Layout.setHorizontalGroup(jPanel20Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel20Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabelTitle, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
						.addGap(199, 199, 199)));
		jPanel20Layout.setVerticalGroup(jPanel20Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel20Layout.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jLabelTitle)));

		jPanel21.setBorder(BorderFactory.createTitledBorder("BillS Information"));

		JLabel jLabelBill = new JLabel();
		jLabelBill.setText("Bill ID ");
		JLabel jLabelEmployee = new JLabel();
		jLabelEmployee.setText("Employee ID ");
		JLabel jLabelRoom = new JLabel();
		jLabelRoom.setText("Room ID ");
		JLabel jLabelDate = new JLabel();
		jLabelDate.setText("Date ");
		JLabel jLabelPrice = new JLabel();
		jLabelPrice.setText("Price ");

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");

		jButtonclear.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear.setText("Clear");

		GroupLayout jPanel21Layout = new GroupLayout(jPanel21);
		jPanel21.setLayout(jPanel21Layout);
		jPanel21Layout.setHorizontalGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel21Layout.createSequentialGroup().addGap(23, 23, 23)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabelBill).addComponent(jLabelEmployee).addComponent(jLabelRoom)
								.addComponent(jLabelDate).addComponent(jLabelPrice)
								.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addComponent(delete, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(add, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel21Layout.createSequentialGroup().addGap(13, 13, 13).addComponent(edit)
										.addGap(18, 18, 18).addComponent(jButtonclear))
								.addGroup(jPanel21Layout.createSequentialGroup().addGap(40, 40, 40)
										.addGroup(jPanel21Layout
												.createParallelGroup(GroupLayout.Alignment.LEADING, false)
												.addComponent(jTextFieldPrice, GroupLayout.DEFAULT_SIZE, 124,
														Short.MAX_VALUE)
												.addComponent(jTextFieldDate).addComponent(jTextFieldRoomID)
												.addComponent(jTextFieldEmployeeID).addComponent(jTextFieldBillID)))
								.addGroup(jPanel21Layout.createSequentialGroup().addGap(32, 32, 32).addComponent(exit)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel21Layout.setVerticalGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel21Layout.createSequentialGroup().addGap(26, 26, 26)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabelBill).addComponent(jTextFieldBillID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabelEmployee).addComponent(jTextFieldEmployeeID,
										GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabelRoom).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabelDate).addComponent(jTextFieldDate, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabelPrice).addComponent(jTextFieldPrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(34, 34, 34)
						.addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear))
						.addGap(26, 26, 26).addGroup(jPanel21Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(32, Short.MAX_VALUE)));

		jScrollPane5.setViewportView(jTableBill);

		GroupLayout jPanel22Layout = new GroupLayout(jPanel22);
		jPanel22.setLayout(jPanel22Layout);
		jPanel22Layout.setHorizontalGroup(jPanel22Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel22Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane5, GroupLayout.PREFERRED_SIZE, 402, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel22Layout.setVerticalGroup(jPanel22Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel22Layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane5, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE).addContainerGap()));

		GroupLayout jPanel6Layout = new GroupLayout(jPanel6);
		jPanel6.setLayout(jPanel6Layout);
		jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel6Layout.createSequentialGroup()
						.addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel6Layout.createSequentialGroup().addGap(18, 18, 18)
										.addComponent(jPanel21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(42, 42, 42).addComponent(jPanel22, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel6Layout.createSequentialGroup().addGap(297, 297, 297).addComponent(
										jPanel20, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel6Layout.createSequentialGroup().addGap(30, 30, 30)
						.addComponent(jPanel20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(37, 37, 37)
						.addGroup(jPanel6Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addComponent(jPanel21, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(jPanel22, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 806, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(0, 5, Short.MAX_VALUE).addComponent(jPanel6,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 6, Short.MAX_VALUE))));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 490, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE).addComponent(jPanel6,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE))));

		pack();
	}

	public JTextField getjTextFieldPrice() {
		return jTextFieldPrice;
	}

	public void setjTextFieldPrice(JTextField jTextFieldPrice) {
		this.jTextFieldPrice = jTextFieldPrice;
	}

	public JTextField getjTextFieldBillID() {
		return jTextFieldBillID;
	}

	public void setjTextFieldBillID(JTextField jTextFieldBillID) {
		this.jTextFieldBillID = jTextFieldBillID;
	}

	public JTextField getjTextFieldEmployeeID() {
		return jTextFieldEmployeeID;
	}

	public void setjTextFieldEmployeeID(JTextField jTextFieldEmployeeID) {
		this.jTextFieldEmployeeID = jTextFieldEmployeeID;
	}

	public JTextField getjTextFieldRoomID() {
		return jTextFieldRoomID;
	}

	public void setjTextFieldRoomID(JTextField jTextFieldRoomID) {
		this.jTextFieldRoomID = jTextFieldRoomID;
	}

	public JTextField getjTextFieldDate() {
		return jTextFieldDate;
	}

	public void setjTextFieldDate(JTextField jTextFieldDate) {
		this.jTextFieldDate = jTextFieldDate;
	}

	public JButton getAdd() {
		return add;
	}

	public JButton getjButtonclear() {
		return jButtonclear;
	}

	public JButton getEdit() {
		return edit;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getDelete() {
		return delete;
	}

	public JTable getjTableBill() {
		return jTableBill;
	}

	public void setjTableBill(JTable jTableBill) {
		this.jTableBill = jTableBill;
	}

	public void setTableModel(MBillTable tableModel) {
		jTableBill.setModel(tableModel);
	}

//	 exit
	public void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();
		}
	}
}
