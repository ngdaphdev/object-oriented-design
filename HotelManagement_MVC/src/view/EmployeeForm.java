package view;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

import different.DisplayGUI;
import model.employee.MEmployeeTable;

@SuppressWarnings("serial")
public class EmployeeForm extends JFrame {
	private JButton jButtonclear;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel8;
	private JPanel jPanel3;
	private JPanel jPanel8;
	private JPanel jPanel9;
	private JScrollPane jScrollPane1;
	private JTable jTableEmployee;
	private JTextField jTextFieldJobTitle;
	private JTextField jTextFieldNote;
	private JTextField jTextFieldSex;
	private JTextField jTextFieldSalary;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldBirthday;
	private JTextField jTextFieldEmployeeName;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;
	private MEmployeeTable mEmployeeTable = new MEmployeeTable();

	public EmployeeForm() {
		initComponents();
	}

	private void initComponents() {

		jPanel3 = new JPanel();
		jPanel8 = new JPanel();
		jLabel1 = new JLabel();
		jPanel9 = new JPanel();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();
		jLabel6 = new JLabel();
		jLabel7 = new JLabel();
		jLabel8 = new JLabel();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldEmployeeName = new JTextField();
		jTextFieldJobTitle = new JTextField();
		jTextFieldSalary = new JTextField();
		jTextFieldBirthday = new JTextField();
		jTextFieldSex = new JTextField();
		jTextFieldNote = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jButtonclear = new JButton();
		jScrollPane1 = new JScrollPane();
		jTableEmployee = new JTable();

		DisplayGUI d = new DisplayGUI("Form Employee");
		d.display(this, jPanel3);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel3.setBackground(new Color(0, 170, 255));

		jLabel1.setFont(new Font("Tahoma", 10, 23));
		jLabel1.setText("Employee Management");
		jPanel8.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel8Layout = new GroupLayout(jPanel8);
		jPanel8.setLayout(jPanel8Layout);
		jPanel8Layout.setHorizontalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
				GroupLayout.Alignment.TRAILING,
				jPanel8Layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jLabel1).addGap(87, 87, 87)));
		jPanel8Layout.setVerticalGroup(jPanel8Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel8Layout.createSequentialGroup().addGap(19, 19, 19)
						.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(21, Short.MAX_VALUE)));

		jPanel9.setBorder(BorderFactory.createTitledBorder("Employee Information"));

		jLabel2.setText("Employee ID");

		jLabel3.setText("Employee Name");

		jLabel4.setText("Job Title");

		jLabel5.setText("Salary");

		jLabel6.setText("Birthday       ");

		jLabel7.setText("Sex            ");

		jLabel8.setText("Note           ");

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");

		jButtonclear.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear.setText("Clear");

		GroupLayout jPanel9Layout = new GroupLayout(jPanel9);
		jPanel9.setLayout(jPanel9Layout);
		jPanel9Layout.setHorizontalGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
						.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(jPanel9Layout.createSequentialGroup().addContainerGap()
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
										.addComponent(delete, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(add, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(18, 18, 18)
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(jPanel9Layout.createSequentialGroup().addComponent(exit).addGap(0, 0,
												Short.MAX_VALUE))
										.addGroup(jPanel9Layout.createSequentialGroup().addComponent(edit)
												.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(jButtonclear, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
						.addGroup(GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
								.addGap(19, 19, 19)
								.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addGroup(jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
												.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(GroupLayout.Alignment.TRAILING, jPanel9Layout
																.createSequentialGroup()
																.addGroup(jPanel9Layout
																		.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																		.addComponent(jLabel3).addComponent(jLabel2))
																.addGap(18, 18, 18))
														.addGroup(jPanel9Layout.createSequentialGroup()
																.addComponent(jLabel4).addGap(29, 29, 29)))
												.addGroup(jPanel9Layout.createSequentialGroup().addComponent(jLabel5)
														.addGap(39, 39, 39)))
												.addGroup(jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
														.addComponent(jTextFieldSalary)
														.addComponent(jTextFieldJobTitle, GroupLayout.Alignment.LEADING)
														.addComponent(jTextFieldEmployeeName)
														.addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
																150, GroupLayout.PREFERRED_SIZE)))
										.addGroup(GroupLayout.Alignment.LEADING,
												jPanel9Layout.createSequentialGroup().addGroup(jPanel9Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(jPanel9Layout
																.createParallelGroup(GroupLayout.Alignment.TRAILING,
																		false)
																.addComponent(jLabel7, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(jLabel6, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
														.addComponent(jLabel8)).addGap(39, 39, 39)
														.addGroup(jPanel9Layout
																.createParallelGroup(GroupLayout.Alignment.LEADING)
																.addComponent(jTextFieldSex)
																.addComponent(jTextFieldBirthday)
																.addComponent(jTextFieldNote))))))
						.addContainerGap()));
		jPanel9Layout.setVerticalGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel9Layout.createSequentialGroup().addGap(19, 19, 19)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2).addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel3).addComponent(jTextFieldEmployeeName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel4).addComponent(jTextFieldJobTitle, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel5).addComponent(jTextFieldSalary, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel6).addComponent(jTextFieldBirthday, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel7).addComponent(jTextFieldSex, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel8).addComponent(jTextFieldNote, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear))
						.addGap(18, 18, 18).addGroup(jPanel9Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(37, Short.MAX_VALUE)));

		jTableEmployee.setModel(mEmployeeTable);

		jScrollPane1.setViewportView(jTableEmployee);

		GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup().addGroup(jPanel3Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(49, 49, 49)
								.addComponent(jPanel9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(36, 36, 36).addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 568,
										GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(240, 240, 240).addComponent(jPanel8,
								GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(45, Short.MAX_VALUE)));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup().addGap(21, 21, 21)
						.addComponent(jPanel8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
								.addComponent(jPanel9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addContainerGap(16, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 1005, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 536, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup()
												.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jPanel3, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addContainerGap())));

		pack();
	}

	// exit
	public void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}

	public JButton getjButtonclear() {
		return jButtonclear;
	}

	public JTable getjTableEmployee() {
		return jTableEmployee;
	}

	public JTextField getjTextFieldJobTitle() {
		return jTextFieldJobTitle;
	}

	public JTextField getjTextFieldNote() {
		return jTextFieldNote;
	}

	public JTextField getjTextFieldSalary() {
		return jTextFieldSalary;
	}

	public JTextField getjTextFieldEmployeeID() {
		return jTextFieldEmployeeID;
	}

	public JTextField getjTextFieldBirthday() {
		return jTextFieldBirthday;
	}

	public JTextField getjTextFieldEmployeeName() {
		return jTextFieldEmployeeName;
	}

	public JButton getEdit() {
		return edit;
	}

	public JTextField getjTextFieldSex() {
		return jTextFieldSex;
	}

	public JButton getAdd() {
		return add;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getDelete() {
		return delete;
	}

	public void setTableModel(MEmployeeTable tableModel) {
		jTableEmployee.setModel(tableModel);

	}

}
