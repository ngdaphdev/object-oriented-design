package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.*;

import different.DisplayGUI;
import model.room.MRoomTable;

@SuppressWarnings("serial")
public class RoomForm extends JFrame {
	private JButton jButtonclear3;
	private JLabel jLabel22;
	private JLabel jLabel23;
	private JLabel jLabel24;
	private JLabel jLabel25;
	private JLabel jLabel26;
	private JLabel jLabel27;
	private JLabel jLabel28;
	private JLabel jLabel29;
	private JLabel jLabel30;
	private JPanel jPanel17;
	private JPanel jPanel18;
	private JPanel jPanel5;
	private JScrollPane jScrollPane4;
	private JTable jTableRoom;
	private JTextField jTextFieldRoom;
	private JTextField jTextFieldPrice;
	private JTextField jTextFieldKindOfRoom;
	private JTextField jTextFieldServiceID;
	private JTextField jTextFieldEmployeeID;
	private JTextField jTextFieldRoomID;
	private JTextField jTextFieldRoomName;
	private JTextField jTextFieldCondition;
	private JButton edit;
	private JButton add;
	private JButton exit;
	private JButton delete;
	private MRoomTable mRoomTable = new MRoomTable();

	public RoomForm() {
		initComponents();

	}

	private void initComponents() {

		jPanel5 = new JPanel();
		jPanel17 = new JPanel();
		jLabel22 = new JLabel();
		jScrollPane4 = new JScrollPane();
		jTableRoom = new JTable();
		jPanel18 = new JPanel();
		jLabel23 = new JLabel();
		jLabel24 = new JLabel();
		jLabel25 = new JLabel();
		jLabel26 = new JLabel();
		jLabel27 = new JLabel();
		jLabel28 = new JLabel();
		jTextFieldRoomID = new JTextField();
		jTextFieldRoomName = new JTextField();
		jTextFieldKindOfRoom = new JTextField();
		jTextFieldPrice = new JTextField();
		jTextFieldRoom = new JTextField();
		jTextFieldCondition = new JTextField();
		add = new JButton();
		edit = new JButton();
		delete = new JButton();
		exit = new JButton();
		jLabel29 = new JLabel();
		jLabel30 = new JLabel();
		jTextFieldEmployeeID = new JTextField();
		jTextFieldServiceID = new JTextField();
		jButtonclear3 = new JButton();

		DisplayGUI d = new DisplayGUI("Form Room");
		d.display(this, jPanel17);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		jPanel5.setBackground(new Color(0, 170, 255));

		jLabel22.setFont(new Font("Tahoma", 0, 22));
		jLabel22.setText("Room Management");
		jPanel17.setBackground(new Color(0, 170, 255));

		GroupLayout jPanel17Layout = new GroupLayout(jPanel17);
		jPanel17.setLayout(jPanel17Layout);
		jPanel17Layout.setHorizontalGroup(jPanel17Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING,
						jPanel17Layout.createSequentialGroup()
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jLabel22)
								.addGap(155, 155, 155)));
		jPanel17Layout.setVerticalGroup(jPanel17Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel17Layout.createSequentialGroup().addGap(23, 23, 23)
						.addComponent(jLabel22, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGap(30, 30, 30)));

		jTableRoom.setModel(mRoomTable);

		jScrollPane4.setViewportView(jTableRoom);

		jPanel18.setBorder(BorderFactory.createTitledBorder("Room information"));

		jLabel23.setText("Room ID");

		jLabel24.setText("Room Name");

		jLabel25.setText("Kind Of Room");

		jLabel26.setText("Price");

		jLabel27.setText("Note");

		jLabel28.setText("Condition");

		jLabel29.setText("Employee ID");

		jLabel30.setText("Service ID");

		add.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/add.png")));
		add.setText("Add");

		edit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/edit.png")));
		edit.setText("Edit");

		delete.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/delete.png")));
		delete.setText("Delete");

		exit.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/exit.png")));
		exit.setText("Exit");

		jButtonclear3.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/clear.png")));
		jButtonclear3.setText("Clear");

		GroupLayout jPanel18Layout = new GroupLayout(jPanel18);
		jPanel18.setLayout(jPanel18Layout);
		jPanel18Layout.setHorizontalGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel18Layout.createSequentialGroup().addGap(21, 21, 21).addGroup(jPanel18Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel18Layout.createSequentialGroup()
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(delete).addComponent(add))
								.addGap(6, 6, Short.MAX_VALUE)
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addComponent(edit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(exit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addGap(18, 18, 18).addComponent(jButtonclear3))
						.addGroup(jPanel18Layout.createSequentialGroup()
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jLabel23).addComponent(jLabel24).addComponent(jLabel25)
										.addComponent(jLabel26).addComponent(jLabel27).addComponent(jLabel28)
										.addComponent(jLabel29).addComponent(jLabel30))
								.addGap(71, 71, 71)
								.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
										.addComponent(jTextFieldRoomID, GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
										.addComponent(jTextFieldRoomName).addComponent(jTextFieldKindOfRoom)
										.addComponent(jTextFieldPrice).addComponent(jTextFieldRoom)
										.addComponent(jTextFieldCondition).addComponent(jTextFieldEmployeeID)
										.addComponent(jTextFieldServiceID))))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel18Layout.setVerticalGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel18Layout.createSequentialGroup().addGap(22, 22, 22)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel23).addComponent(jTextFieldRoomID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel24).addComponent(jTextFieldRoomName, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel25).addComponent(jTextFieldKindOfRoom, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel26).addComponent(jTextFieldPrice, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel27).addComponent(jTextFieldRoom, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel28).addComponent(jTextFieldCondition, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel29).addComponent(jTextFieldEmployeeID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel30).addComponent(jTextFieldServiceID, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(25, 25, 25)
						.addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(add)
								.addComponent(edit).addComponent(jButtonclear3))
						.addGap(18, 18, 18).addGroup(jPanel18Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(delete).addComponent(exit))
						.addContainerGap(16, Short.MAX_VALUE)));

		GroupLayout jPanel5Layout = new GroupLayout(jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout.setHorizontalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jScrollPane4, GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE).addContainerGap())
				.addGroup(jPanel5Layout.createSequentialGroup().addGap(342, 342, 342)
						.addComponent(jPanel17, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel5Layout.setVerticalGroup(jPanel5Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup().addGroup(jPanel5Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel5Layout.createSequentialGroup().addContainerGap()
								.addComponent(jPanel17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(49, 49, 49).addComponent(jScrollPane4, GroupLayout.PREFERRED_SIZE, 285,
										GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel5Layout.createSequentialGroup().addGap(99, 99, 99).addComponent(jPanel18,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1094, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(layout
										.createSequentialGroup().addContainerGap().addComponent(jPanel5,
												GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addContainerGap())));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGap(0, 510, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
						GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		pack();
	}

	public JButton getjButtonclear() {
		return jButtonclear3;
	}

	public JTable getjTableRoom() {
		return jTableRoom;
	}

	public JTextField getjTextFieldRoom() {
		return jTextFieldRoom;
	}

	public JTextField getjTextFieldPrice() {
		return jTextFieldPrice;
	}

	public JTextField getjTextFieldKindOfRoom() {
		return jTextFieldKindOfRoom;
	}

	public JTextField getjTextFieldServiceID() {
		return jTextFieldServiceID;
	}

	public JTextField getjTextFieldEmployeeID() {
		return jTextFieldEmployeeID;
	}

	public JTextField getjTextFieldRoomID() {
		return jTextFieldRoomID;
	}

	public JTextField getjTextFieldRoomName() {
		return jTextFieldRoomName;
	}

	public JTextField getjTextFieldCondition() {
		return jTextFieldCondition;
	}

	public JButton getEdit() {
		return edit;
	}

	public JButton getAdd() {
		return add;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getDelete() {
		return delete;
	}
	
	

	// exit
	public void exitActionPerformed(ActionEvent evt) {
		exit.setToolTipText("Click to exit the program");
		int selected = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the program?", "Notify",
				JOptionPane.YES_NO_CANCEL_OPTION);
		if (selected == JOptionPane.YES_NO_OPTION) {
			this.dispose();

		}
	}
	
	public void setTableModel(MRoomTable tableModel) {
		jTableRoom.setModel(tableModel);
	}

}
