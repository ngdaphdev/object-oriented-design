package view;

import java.awt.*;
import javax.swing.*;

import different.DisplayGUI;

@SuppressWarnings("serial")
public class MenuForm extends JFrame {
	private JButton jButtonRoom;
	private JButton jButtonCustomer;
	private JButton jButtonService;
	private JButton jButtonBill;
	private JButton jButtonEmployee;

	public MenuForm() {
		initComponents();
	}

	private void initComponents() {
		JPanel jPanel;
		jPanel = new JPanel();
		jButtonEmployee = new JButton();
		jButtonService = new JButton();
		jButtonRoom = new JButton();
		jButtonCustomer = new JButton();
		jButtonBill = new JButton();
		JLabel lbTitle;
		lbTitle = new JLabel();

		DisplayGUI d = new DisplayGUI("Menu Form");
		d.display(this, jPanel);

		jPanel.setBackground(new Color(0, 170, 255));

		jButtonEmployee.setBackground(new Color(255, 255, 255));
		jButtonEmployee.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/employee.png")));
		jButtonEmployee.setText("Employee");

		jButtonService.setBackground(new Color(255, 255, 255));
		jButtonService.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/service.png")));
		jButtonService.setText("Service");

		jButtonRoom.setBackground(new Color(255, 255, 255));
		jButtonRoom.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/room.png")));
		jButtonRoom.setText("Room");

		jButtonCustomer.setBackground(new Color(255, 255, 255));
		jButtonCustomer.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/customer.png")));
		jButtonCustomer.setText("Customer");

		jButtonBill.setBackground(new Color(255, 255, 255));
		jButtonBill.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/bill.png")));
		jButtonBill.setText("Bill");

		lbTitle.setFont(new Font("Yu Gothic UI Light", 0, 44));
		lbTitle.setText("Hotel Management");

		GroupLayout jPanel1Layout = new GroupLayout(jPanel);
		jPanel.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(26, 26, 26)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(jButtonRoom, GroupLayout.PREFERRED_SIZE, 145,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(jButtonEmployee))
										.addGap(44, 44, 44)
										.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addComponent(jButtonCustomer, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jButtonService, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGap(40, 40, 40))
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(lbTitle, GroupLayout.PREFERRED_SIZE, 375,
												GroupLayout.PREFERRED_SIZE)
										.addGap(8, 8, 8))))
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(115, 115, 115)
						.addComponent(jButtonBill, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(36, 36, 36).addComponent(lbTitle)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
										.addComponent(jButtonEmployee).addGap(18, 18, 18))
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(22, 22, 22)
										.addComponent(jButtonService)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jButtonCustomer).addComponent(jButtonRoom))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jButtonBill, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
						.addContainerGap()));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}

	public JButton getjButtonRoom() {
		return jButtonRoom;
	}

	public JButton getjButtonCustomer() {
		return jButtonCustomer;
	}

	public JButton getjButtonService() {
		return jButtonService;
	}

	public JButton getjButtonBill() {
		return jButtonBill;
	}

	public JButton getjButtonEmployee() {
		return jButtonEmployee;
	}

}
