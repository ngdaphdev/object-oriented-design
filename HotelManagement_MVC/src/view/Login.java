package view;

import java.awt.*;

import javax.swing.*;

import different.DisplayGUI;

@SuppressWarnings("serial")
public class Login extends JFrame{
	private JPasswordField password;
	private JButton jbtnLogin;
	private JTextField txtUser;
	
	public Login() {
		initComponents();
	}

	private void initComponents() {
		JPanel jPanel;
		jPanel = new JPanel();
		txtUser = new JTextField();
		jbtnLogin = new JButton();
		password = new JPasswordField();

		DisplayGUI d = new DisplayGUI("Login");
		d.display(this,jPanel);
		jPanel.setBackground(new Color(255, 255, 255));

//		button login
		jbtnLogin.setFont(new Font("Tw Cen MT Condensed Extra Bold", 0, 18));
		jbtnLogin.setText("LOGIN");
		
//		user
		JLabel lbUser = new JLabel();
		lbUser.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/user.png")));
		lbUser.setText("      User");

//		password
		JLabel lbPassword = new JLabel();
		lbPassword.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/pass.png")));
		lbPassword.setText("   Password");

//		title
		JLabel lbTitle = new JLabel();
		lbTitle.setFont(new Font("Tahoma", 0, 23));
		lbTitle.setText("Nong Lam Hotel");

//		icon hotel
		JLabel lbhotel = new JLabel();
		lbhotel.setIcon(new ImageIcon(getClass().getResource("/qlks/Image/hotel.png")));

		GroupLayout jPanelLayout = new GroupLayout(jPanel);
		jPanel.setLayout(jPanelLayout);
		jPanelLayout
				.setHorizontalGroup(
						jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanelLayout
										.createSequentialGroup().addGap(127, 127, 127)
										.addComponent(jbtnLogin, GroupLayout.PREFERRED_SIZE, 185,
												GroupLayout.PREFERRED_SIZE)
										.addGap(0, 0, Short.MAX_VALUE))
								.addGroup(jPanelLayout.createSequentialGroup().addGap(51, 51, 51)
										.addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
												.addGroup(GroupLayout.Alignment.TRAILING, jPanelLayout
														.createSequentialGroup()
														.addComponent(
																lbhotel, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
														.addGap(18, 18, 18)
														.addComponent(
																lbTitle, GroupLayout.PREFERRED_SIZE, 194,
																GroupLayout.PREFERRED_SIZE)
														.addGap(42, 42, 42))
												.addGroup(jPanelLayout.createSequentialGroup().addGroup(jPanelLayout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addComponent(lbPassword, GroupLayout.PREFERRED_SIZE, 107,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lbUser)).addGap(18, 18, 18)
														.addGroup(jPanelLayout
																.createParallelGroup(GroupLayout.Alignment.LEADING,
																		false)
																.addComponent(password, GroupLayout.DEFAULT_SIZE, 125,
																		Short.MAX_VALUE)
																.addComponent(txtUser))
														.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))));
		jPanelLayout.setVerticalGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanelLayout.createSequentialGroup()
						.addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanelLayout.createSequentialGroup().addGap(36, 36, 36).addComponent(lbTitle,
										GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanelLayout.createSequentialGroup().addContainerGap().addComponent(lbhotel,
										GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)))
						.addGap(18, 18, 18)
						.addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(lbUser)
								.addComponent(txtUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)
						.addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(lbPassword).addComponent(password, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18).addComponent(jbtnLogin).addGap(54, 54, 54)));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}
	
	public JPasswordField getPassword() {
		return password;
	}


	public void setPassword(JPasswordField password) {
		this.password = password;
	}


	public JButton getJbtnLogin() {
		return jbtnLogin;
	}


	public void setJbtnLogin(JButton jbtnLogin) {
		this.jbtnLogin = jbtnLogin;
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public void setTxtUser(JTextField txtUser) {
		this.txtUser = txtUser;
	}
	
}
