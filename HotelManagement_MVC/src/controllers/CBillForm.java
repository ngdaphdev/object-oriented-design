package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JTable;

import different.Noti;
import view.BillForm;
import model.bill.Bill;
import model.bill.MBillForm;
import model.bill.MBillTable;

public class CBillForm {
	private MBillForm mBillForm;
	private BillForm billForm;
	private MBillTable tableModel;
	private Noti noti;

	public CBillForm(BillForm billForm) {
		super();
		this.mBillForm = new MBillForm();
		this.billForm = billForm;
		tableModel = new MBillTable();
		noti = new Noti();
		
		addAction(billForm.getAdd());
		exitAction(billForm.getExit());
		clearAction(billForm.getjButtonclear());
		tableAction(billForm.getjTableBill());
		deleteAction(billForm.getDelete());
		editAction(billForm.getEdit());
		showList();
	}

	public void showList() {
		ArrayList<Bill> listBill = mBillForm.getListBill();
		tableModel.setData(listBill);
		billForm.setTableModel(tableModel);
		billForm.setVisible(true);
	}

	public void addAction(JButton jbtnAdd) {
		jbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mBillForm.addActionPerformed(evt, billForm.getjTextFieldBillID(), billForm.getjTextFieldEmployeeID(),
						billForm.getjTextFieldRoomID(), billForm.getjTextFieldDate(), billForm.getjTextFieldPrice());
				if (mBillForm.isFlag()) {
					noti.showMessage("Add succesfully!");
					tableModel.setData(mBillForm.getListBill());
				} else {
					noti.showMessage("Error in process add!");
				}
			}
		});
	}

	public void editAction(JButton jbtnEdit) {
		jbtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mBillForm.editActionPerformed(evt, billForm.getjTextFieldBillID(), billForm.getjTextFieldEmployeeID(),
						billForm.getjTextFieldRoomID(), billForm.getjTextFieldDate(), billForm.getjTextFieldPrice(),
						billForm.getjTableBill());
				if (mBillForm.isFlag()) {
					noti.showMessage("Edit succesfully!");
					tableModel.setData(mBillForm.getListBill());
				} else {
					noti.showMessage("Error in process edit!");
				}
			}
		});
	}

	public void deleteAction(JButton jbtndelete) {
		jbtndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mBillForm.deleteActionPerformed(evt, billForm.getjTextFieldBillID());
				if (mBillForm.isFlag()) {
					noti.showMessage("Delete succesfully!");
					tableModel.setData(mBillForm.getListBill());
				} else {
					noti.showMessage("Error in process delete!");
				}
			}

		});
	}

	public void exitAction(JButton exit) {
		try {
			exit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					billForm.exitActionPerformed(evt);
				}
			});
		} catch (Exception e) {
		}
	}

	public void clearAction(JButton clear) {
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mBillForm.jButtonclearActionPerformed(evt, billForm.getjTextFieldBillID(),
						billForm.getjTextFieldEmployeeID(), billForm.getjTextFieldRoomID(),
						billForm.getjTextFieldDate(), billForm.getjTextFieldPrice());
			}
		});
	}

	public void tableAction(JTable jTable) {
		jTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				mBillForm.jTableHOADONMouseClicked(evt, billForm.getjTableBill(), billForm.getjTextFieldBillID(),
						billForm.getjTextFieldEmployeeID(), billForm.getjTextFieldRoomID(),
						billForm.getjTextFieldDate(), billForm.getjTextFieldPrice());
			}
		});
	}
}
