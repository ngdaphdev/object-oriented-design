package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JTable;

import different.Noti;
import model.employee.MEmployeeForm;
import model.employee.MEmployeeTable;
import model.employee.Employee;
import view.EmployeeForm;

public class CEmployeeForm {
	private MEmployeeForm mEmployeeForm;
	private EmployeeForm employeeForm;
	private Noti noti;
	private MEmployeeTable tableModel;

	public CEmployeeForm(EmployeeForm employeeForm) {
		super();
		this.mEmployeeForm = new MEmployeeForm();
		this.employeeForm = employeeForm;
		noti = new Noti();
		tableModel = new MEmployeeTable();

		addAction(employeeForm.getAdd());
		exitAction(employeeForm.getExit());
		clearAction(employeeForm.getjButtonclear());
		tableAction(employeeForm.getjTableEmployee());
		deleteAction(employeeForm.getDelete());
		editAction(employeeForm.getEdit());
		showList();
	}

	public void showList() {
		ArrayList<Employee> listEmployee = mEmployeeForm.getListEmployee();
		tableModel.setData(listEmployee);
		employeeForm.setTableModel(tableModel);
		employeeForm.setVisible(true);
	}

	public void addAction(JButton jbtnAdd) {
		jbtnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mEmployeeForm.addActionPerformed(evt, employeeForm.getjTextFieldEmployeeID(),
						employeeForm.getjTextFieldEmployeeName(), employeeForm.getjTextFieldJobTitle(),
						employeeForm.getjTextFieldSalary(), employeeForm.getjTextFieldBirthday(),
						employeeForm.getjTextFieldSex(), employeeForm.getjTextFieldNote());
				if (mEmployeeForm.isFlag()) {
					noti.showMessage("Add succesfully!");
					tableModel.setData(mEmployeeForm.getListEmployee());
				} else {
					noti.showMessage("Error in process Add!");
				}
			}
		});
	}

	public void editAction(JButton jbtnEdit) {
		jbtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mEmployeeForm.editActionPerformed(evt, employeeForm.getjTableEmployee(),
						employeeForm.getjTextFieldEmployeeID(), employeeForm.getjTextFieldEmployeeName(),
						employeeForm.getjTextFieldJobTitle(), employeeForm.getjTextFieldSalary(),
						employeeForm.getjTextFieldBirthday(), employeeForm.getjTextFieldSex(),
						employeeForm.getjTextFieldNote());
				if (mEmployeeForm.isFlag()) {
					noti.showMessage("Edit succesfully!");
					tableModel.setData(mEmployeeForm.getListEmployee());
				} else {
					noti.showMessage("Error in process edit!");
				}
			}
		});
	}

	public void deleteAction(JButton jbtndelete) {
		jbtndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mEmployeeForm.deleteActionPerformed(evt, employeeForm.getjTextFieldEmployeeID());
				if (mEmployeeForm.isFlag()) {
					noti.showMessage("Delete succesfully!");
					tableModel.setData(mEmployeeForm.getListEmployee());
				} else {
					noti.showMessage("Error in process delete!");
				}
			}
		});
	}

	public void exitAction(JButton exit) {
		try {
			exit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					employeeForm.exitActionPerformed(evt);
				}
			});
		} catch (Exception e) {
		}
	}

	public void clearAction(JButton clear) {
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mEmployeeForm.jButtonclearActionPerformed(evt, employeeForm.getjTextFieldEmployeeID(),
						employeeForm.getjTextFieldEmployeeName(), employeeForm.getjTextFieldJobTitle(),
						employeeForm.getjTextFieldSalary(), employeeForm.getjTextFieldBirthday(),
						employeeForm.getjTextFieldSex(), employeeForm.getjTextFieldNote());
			}
		});
	}

	public void tableAction(JTable jTable) {
		jTable.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				mEmployeeForm.jTableEmployeeMouseClicked(evt, employeeForm.getjTextFieldEmployeeID(),
						employeeForm.getjTextFieldEmployeeName(), employeeForm.getjTextFieldJobTitle(),
						employeeForm.getjTextFieldSalary(), employeeForm.getjTextFieldBirthday(),
						employeeForm.getjTextFieldSex(), employeeForm.getjTextFieldNote(),
						employeeForm.getjTableEmployee());
			}
		});
	}
}
