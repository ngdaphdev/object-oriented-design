package controllers;

import javax.swing.JButton;

import model.MLogin;

import java.awt.event.*;

import view.Login;
import view.MenuForm;

public class CLogin {
	private Login login;
	private MLogin mLogin;
	private MenuForm menuForm;
	
	public CLogin (Login login) {
		this.login = login;
		this.mLogin = new MLogin();
		showLogin();
		actionBtnLogin(login.getJbtnLogin());
	}
	
	public void showLogin() {
		login.setVisible(true);
	}

	public void actionBtnLogin(JButton jbtnLogin) {		
		jbtnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mLogin.jButtonActionPerformed(evt, login, login.getTxtUser(), login.getPassword(), menuForm);
			}
		});
	} 
	
	
	
}
