package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JTable;

import different.Noti;
import model.customer.MCustomerForm;
import model.customer.MCustomerTable;
import model.customer.Customer;
import view.CustomerForm;

public class CCustomerForm {
	private MCustomerForm mCustomerForm;
	private CustomerForm customerForm;
	private Noti noti;
	private MCustomerTable tableModel;

	public CCustomerForm(CustomerForm customerForm) {
		super();
		this.mCustomerForm = new MCustomerForm();
		this.customerForm = customerForm;
		tableModel = new MCustomerTable();
		noti = new Noti();
		addAction(customerForm.getAdd());
		exitAction(customerForm.getExit());
		clearAction(customerForm.getjButtonclear());
		tableAction(customerForm.getjTableCustomer());
		deleteAction(customerForm.getDelete());
		editAction(customerForm.getEdit());
		showList();
	}

	public void showList() {
		ArrayList<Customer> listCustomer = mCustomerForm.getListCustomer();
		tableModel.setData(listCustomer);
		customerForm.setTableModel(tableModel);
		customerForm.setVisible(true);
	}

	public void addAction(JButton jbtnAdd) {
		jbtnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mCustomerForm.addActionPerformed(evt, customerForm.getjTextFieldCustomerID(),
						customerForm.getjTextFieldName(), customerForm.getjTextFieldCitizenID(),
						customerForm.getjTextFieldNationality(), customerForm.getjTextFieldSex(),
						customerForm.getjTextFieldAge(), customerForm.getjTextFieldTel(),
						customerForm.getjTextFieldRoomID());
				if (mCustomerForm.isFlag()) {
					noti.showMessage("Add succesfully!");
					tableModel.setData(mCustomerForm.getListCustomer());
				} else {
					noti.showMessage("Error in process Add!");
				}
			}
		});

	}

	public void editAction(JButton jbtnEdit) {
		jbtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mCustomerForm.editActionPerformed(evt, customerForm.getjTableCustomer(),
						customerForm.getjTextFieldCustomerID(), customerForm.getjTextFieldName(),
						customerForm.getjTextFieldCitizenID(), customerForm.getjTextFieldNationality(),
						customerForm.getjTextFieldSex(), customerForm.getjTextFieldAge(),
						customerForm.getjTextFieldTel(), customerForm.getjTextFieldRoomID());
				if (mCustomerForm.isFlag()) {
					noti.showMessage("Edit succesfully!");
					tableModel.setData(mCustomerForm.getListCustomer());
				} else {
					noti.showMessage("Error in process edit!");
				}
			}
		});
	}

	public void deleteAction(JButton jbtndelete) {
		jbtndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mCustomerForm.deleteActionPerformed(evt, customerForm.getjTextFieldCustomerID());
				if (mCustomerForm.isFlag()) {
					noti.showMessage("Delete succesfully!");
					tableModel.setData(mCustomerForm.getListCustomer());
				} else {
					noti.showMessage("Error in process Delete!");
				}
			}
		});
	}

	public void exitAction(JButton exit) {
		try {
			exit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					customerForm.exitActionPerformed(evt);
				}
			});
		} catch (Exception e) {
		}
	}

	public void clearAction(JButton clear) {
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mCustomerForm.jButtonclearActionPerformed(evt, customerForm.getjTextFieldCustomerID(),
						customerForm.getjTextFieldName(), customerForm.getjTextFieldCitizenID(),
						customerForm.getjTextFieldNationality(), customerForm.getjTextFieldSex(),
						customerForm.getjTextFieldAge(), customerForm.getjTextFieldTel(),
						customerForm.getjTextFieldRoomID());
			}
		});
	}

	public void tableAction(JTable jTable) {
		jTable.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				mCustomerForm.jTableCustomerMouseClicked(evt, customerForm.getjTextFieldCustomerID(),
						customerForm.getjTextFieldName(), customerForm.getjTextFieldCitizenID(),
						customerForm.getjTextFieldNationality(), customerForm.getjTextFieldSex(),
						customerForm.getjTextFieldAge(), customerForm.getjTextFieldTel(),
						customerForm.getjTextFieldRoomID(), customerForm.getjTableCustomer());
			}
		});

	}
}
