package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JTable;

import different.Noti;
import model.room.MRoomForm;
import model.room.MRoomTable;
import model.room.Room;
import view.RoomForm;

public class CRoomForm {
	private MRoomForm mRoomForm;
	private RoomForm roomForm;
	private Noti noti;
	private MRoomTable tableModel;

	public CRoomForm(RoomForm roomForm) {
		super();
		this.mRoomForm = new MRoomForm();
		this.roomForm = roomForm;
		tableModel = new MRoomTable();
		noti = new Noti();

		addAction(roomForm.getAdd());
		exitAction(roomForm.getExit());
		clearAction(roomForm.getjButtonclear());
		tableAction(roomForm.getjTableRoom());
		deleteAction(roomForm.getDelete());
		editAction(roomForm.getEdit());
		showList();
	}

	public void showList() {
		ArrayList<Room> listRoom = mRoomForm.getListRoom();
		tableModel.setData(listRoom);
		roomForm.setTableModel(tableModel);
		roomForm.setVisible(true);
	}

	public void addAction(JButton jbtnAdd) {
		jbtnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mRoomForm.addActionPerformed(evt, roomForm.getjTextFieldRoomID(), roomForm.getjTextFieldRoomName(),
						roomForm.getjTextFieldKindOfRoom(), roomForm.getjTextFieldPrice(), roomForm.getjTextFieldRoom(),
						roomForm.getjTextFieldCondition(), roomForm.getjTextFieldEmployeeID(),
						roomForm.getjTextFieldServiceID());
				if (mRoomForm.isFlag()) {
					noti.showMessage("Add succesfully!");
					tableModel.setData(mRoomForm.getListRoom());
				} else {
					noti.showMessage("Error in process add!");
				}
			}
		});
	}

	public void editAction(JButton jbtnEdit) {
		jbtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mRoomForm.editActionPerformed(evt, roomForm.getjTextFieldRoomID(), roomForm.getjTextFieldRoomName(),
						roomForm.getjTextFieldKindOfRoom(), roomForm.getjTextFieldPrice(), roomForm.getjTextFieldRoom(),
						roomForm.getjTextFieldCondition(), roomForm.getjTextFieldEmployeeID(),
						roomForm.getjTextFieldServiceID(), roomForm.getjTableRoom());
				if (mRoomForm.isFlag()) {
					noti.showMessage("Edit succesfully!");
					tableModel.setData(mRoomForm.getListRoom());
				} else {
					noti.showMessage("Error in process edit!");
				}
			}
		});
	}

	public void deleteAction(JButton jbtndelete) {
		jbtndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mRoomForm.deleteActionPerformed(evt, roomForm.getjTextFieldRoomID());
				if (mRoomForm.isFlag()) {
					noti.showMessage("Delete succesfully!");
					tableModel.setData(mRoomForm.getListRoom());
				} else {
					noti.showMessage("Error in process Delete!");
				}
			}
		});
	}

	public void exitAction(JButton exit) {
		try {
			exit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					roomForm.exitActionPerformed(evt);
				}
			});
		} catch (Exception e) {
		}
	}

	public void clearAction(JButton clear) {
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mRoomForm.jButtonclearActionPerformed(evt, roomForm.getjTextFieldRoomID(),
						roomForm.getjTextFieldRoomName(), roomForm.getjTextFieldKindOfRoom(),
						roomForm.getjTextFieldPrice(), roomForm.getjTextFieldRoom(), roomForm.getjTextFieldCondition(),
						roomForm.getjTextFieldEmployeeID(), roomForm.getjTextFieldServiceID());
			}
		});
	}

	public void tableAction(JTable jTable) {
		jTable.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				mRoomForm.jTableRoomMouseClicked(evt, roomForm.getjTextFieldRoomID(), roomForm.getjTextFieldRoomName(),
						roomForm.getjTextFieldKindOfRoom(), roomForm.getjTextFieldPrice(), roomForm.getjTextFieldRoom(),
						roomForm.getjTextFieldCondition(), roomForm.getjTextFieldEmployeeID(),
						roomForm.getjTextFieldServiceID(), roomForm.getjTableRoom());
			}
		});
	}

}
