package controllers;

import java.awt.event.*;

import javax.swing.JButton;

import model.MMenuForm;
import view.MenuForm;

public class CMenuForm {
	private MenuForm menuForm;
	private MMenuForm mMenuForm;
	
	public CMenuForm (MenuForm menuForm) {
		this.menuForm = menuForm;
		this.mMenuForm = new MMenuForm();
		showMenuForm();
		actionBtnEmployee(menuForm.getjButtonEmployee());
		actionBtnService(menuForm.getjButtonService());
		actionBtnRoom(menuForm.getjButtonRoom());
		actionBtnCustomer(menuForm.getjButtonCustomer());
		actionBtnBill(menuForm.getjButtonBill());
	}
	
	public void showMenuForm() {
		menuForm.setVisible(true);
	}

	public void actionBtnEmployee(JButton jButtonEmployee) {
		jButtonEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mMenuForm.jButtonEmployeeActionPerformed(evt);
			}
		});
	}

	public void actionBtnService(JButton jButtonService) {
		jButtonService.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mMenuForm.jButtonServiceActionPerformed(evt);
			}
		});
	}

	public void actionBtnRoom(JButton jButtonRoom) {
		jButtonRoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mMenuForm.jButtonRoomActionPerformed(evt);
			}
		});
	}

	public void actionBtnCustomer(JButton jButtonCustomer) {
		jButtonCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mMenuForm.jButtonCustomerActionPerformed(evt);
			}
		});
	}

	public void actionBtnBill(JButton jButtonBill) {
		jButtonBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mMenuForm.jButtonBillActionPerformed(evt);
			}
		});
	}
}
