package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import different.Noti;
import model.service.MServiceTable;
import model.service.MSeviceForm;
import model.service.Service;
import view.ServiceForm;

public class CSeviceForm {
	private MSeviceForm mSeviceForm;
	private ServiceForm serviceForm;
	private MServiceTable tableModel;
	private Noti noti;

	public CSeviceForm(ServiceForm serviceForm) {
		super();
		this.mSeviceForm = new MSeviceForm();
		this.serviceForm = serviceForm;
		tableModel = new MServiceTable();
		noti = new Noti();

		addAction(serviceForm.getAdd());
		exitAction(serviceForm.getExit());
		clearAction(serviceForm.getjButtonclear());
		tableAction(serviceForm.getjTableDichvu());
		deleteAction(serviceForm.getDelete());
		editAction(serviceForm.getEdit());
		showList();
	}

	public void showList() {
		ArrayList<Service> listService = mSeviceForm.getListService();
		tableModel.setData(listService);
		serviceForm.setTableModel(tableModel);
		serviceForm.setVisible(true);
	}

	public void addAction(JButton jbtnAdd) {
		jbtnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mSeviceForm.addActionPerformed(evt, serviceForm.getjTextFieldServiceID(),
						serviceForm.getjTextFieldServiceName(), serviceForm.getjTextFieldServicePrice());
				if (mSeviceForm.isFlag()) {
					noti.showMessage("Add succesfully!");
					tableModel.setData(mSeviceForm.getListService());
				} else {
					noti.showMessage("Error in process add!");
				}
			}
		});
	}

	public void editAction(JButton jbtnEdit) {
		jbtnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mSeviceForm.editActionPerformed(evt, serviceForm.getjTableDichvu(),serviceForm.getjTextFieldServiceID(),
						serviceForm.getjTextFieldServiceName(), serviceForm.getjTextFieldServicePrice());
				if (mSeviceForm.isFlag()) {
					noti.showMessage("Edit succesfully!");
					tableModel.setData(mSeviceForm.getListService());
				} else {
					noti.showMessage("Error in process edit!");
				}
			}
		});
	}

	public void deleteAction(JButton jbtndelete) {
		jbtndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mSeviceForm.deleteActionPerformed(evt, serviceForm.getjTextFieldServiceID());
				if (mSeviceForm.isFlag()) {
					noti.showMessage("Delete succesfully!");
					tableModel.setData(mSeviceForm.getListService());
				} else {
					noti.showMessage("Error in process delete!");
				}
			}
		});
	}

	public void exitAction(JButton exit) {
		try {
			exit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent evt) {
					serviceForm.exitActionPerformed(evt);
				}
			});
		} catch (Exception e) {
		}
	}

	public void clearAction(JButton clear) {
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				mSeviceForm.jButtonclearActionPerformed(evt, serviceForm.getjTextFieldServiceID(),
						serviceForm.getjTextFieldServiceName(), serviceForm.getjTextFieldServicePrice());
			}
		});
	}

	public void tableAction(JTable jTable) {
		jTable.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				mSeviceForm.jTableServiceMouseClicked(evt, serviceForm.getjTableDichvu(),
						serviceForm.getjTextFieldServiceID(), serviceForm.getjTextFieldServiceName(),
						serviceForm.getjTextFieldServicePrice());
			}
		});
	}

	public void notiEdit() {
		if (mSeviceForm.isFlag()) {
			JOptionPane.showMessageDialog(null, "Edit succesfully!", "Notify Edit", JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, "Error in process edit!", "Notify Edit", JOptionPane.ERROR_MESSAGE);

		}
	}

}
