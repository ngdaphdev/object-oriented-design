package model.bill;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import model.MyConnection;

public class MBillForm {
	Connection con = null;
	Statement st;
	private MyConnection myConn;
	private boolean flag = false;

	public MBillForm() {
		myConn = new MyConnection();
	}

//	get list Bill
	public ArrayList<Bill> getListBill() {
		ArrayList<Bill> listBill = new ArrayList<Bill>();
		con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM hoadon";
			ResultSet rs = st.executeQuery(sql);

			Bill bill;
			while (rs.next()) {
				bill = new Bill(rs.getString("MAHD"), rs.getString("MANV"), rs.getString("MAPHONG"), rs.getDate("NGAY"),
						rs.getDouble("GIAHD"));

				listBill.add(bill);
			}
			st.close();
			rs.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			myConn.closeConnection(con);
		}
		return listBill;
	}
	
	public ArrayList<Bill> setListBill(ArrayList<Bill> oList) {
		getListBill().removeAll(getListBill());
		getListBill().addAll(oList);
		return getListBill();
	}

	// Add
	public void addActionPerformed(ActionEvent evt, JTextField jTextFieldBillID, JTextField jTextFieldEmployeeID,
			JTextField jTextFieldRoomID, JTextField jTextFieldDate, JTextField jTextFieldPrice) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "INSERT INTO hoadon(MAHD,MANV, MAPHONG, NGAY, GIAHD) VALUES(?,?,?,?,?)";

			PreparedStatement insertStatement = con.prepareStatement(sql);
			insertStatement.setString(1, jTextFieldBillID.getText());
			insertStatement.setString(2, jTextFieldEmployeeID.getText());
			insertStatement.setString(3, jTextFieldRoomID.getText());
			insertStatement.setString(4, jTextFieldDate.getText());
			insertStatement.setString(5, jTextFieldPrice.getText());

			insertStatement.executeUpdate();
			insertStatement.close();
			setFlag(true);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			myConn.closeConnection(con);
		}

	}

//		 Edit
	public void editActionPerformed(ActionEvent evt, JTextField jTextFieldBillID, JTextField jTextFieldEmployeeID,
			JTextField jTextFieldRoomID, JTextField jTextFieldDate, JTextField jTextFieldPrice, JTable model) {
		Connection con = myConn.getConnection();
		if (model.getSelectedRow() == -1) {
			flag = false;
		} else {
			try {
				st = (Statement) con.createStatement();
				String sql = "UPDATE HOADON SET MAHD=?,MANV=?,MAPHONG=?,NGAY=?,GIAHD=? WHERE MAHD = ?";

				PreparedStatement insertStatement = con.prepareStatement(sql);
				insertStatement.setString(1, jTextFieldBillID.getText());
				insertStatement.setString(2, jTextFieldEmployeeID.getText());
				insertStatement.setString(3, jTextFieldRoomID.getText());
				insertStatement.setString(4, jTextFieldDate.getText());
				insertStatement.setString(5, jTextFieldPrice.getText());
				insertStatement.setString(6, jTextFieldBillID.getText());

				insertStatement.executeUpdate();
				insertStatement.close();

				model.setValueAt(jTextFieldBillID.getText(), model.getSelectedRow(), 0);
				model.setValueAt(jTextFieldEmployeeID.getText().toString(), model.getSelectedRow(), 1);
				model.setValueAt(jTextFieldRoomID.getText(), model.getSelectedRow(), 2);
				model.setValueAt(jTextFieldDate.getText(), model.getSelectedRow(), 3);
				model.setValueAt(jTextFieldPrice.getText(), model.getSelectedRow(), 4);
				setFlag(true);
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				myConn.closeConnection(con);
			}

		}
	}

//		 Delete
	public void deleteActionPerformed(ActionEvent evt, JTextField jTextFieldBillID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "DELETE FROM hoadon WHERE MAHD = '" + jTextFieldBillID.getText() + "'";
			st.executeUpdate(query);
			setFlag(true);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			myConn.closeConnection(con);
		}
	}

	public void jButtonclearActionPerformed(ActionEvent evt, JTextField jTextFieldBillID,
			JTextField jTextFieldEmployeeID, JTextField jTextFieldRoomID, JTextField jTextFieldDate,
			JTextField jTextFieldPrice) {
		jTextFieldBillID.setText("");
		jTextFieldEmployeeID.setText("");
		jTextFieldRoomID.setText("");
		jTextFieldDate.setText("");
		jTextFieldPrice.setText("");
		jTextFieldBillID.requestFocus();

	}

	public void jTableHOADONMouseClicked(MouseEvent evt, JTable jTableBill, JTextField jTextFieldBillID,
			JTextField jTextFieldEmployeeID, JTextField jTextFieldRoomID, JTextField jTextFieldDate,
			JTextField jTextFieldPrice) {
		int i = jTableBill.getSelectedRow();
		TableModel model = jTableBill.getModel();
		jTextFieldBillID.setText(model.getValueAt(i, 0).toString());
		jTextFieldEmployeeID.setText(model.getValueAt(i, 1).toString());
		jTextFieldRoomID.setText(model.getValueAt(i, 2).toString());
		jTextFieldDate.setText(model.getValueAt(i, 3).toString());
		jTextFieldPrice.setText(model.getValueAt(i, 4).toString());
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
