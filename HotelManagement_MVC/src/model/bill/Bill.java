package model.bill;

import java.util.Date;

public class Bill {
	private String billID;
	private String employeeID;
	private String roomID;
	private Date date;
	private double price;

	public Bill(String billID, String employeeID, String roomID, Date date, double price) {
		super();
		this.billID = billID;
		this.employeeID = employeeID;
		this.roomID = roomID;
		this.date = date;
		this.price = price;
	}

	public Bill() {
	}

	public String getBillID() {
		return billID;
	}

	public void setBillID(String billID) {
		this.billID = billID;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Bill [billID=" + billID + ", employeeID=" + employeeID + ", roomID=" + roomID + ", date=" + date
				+ ", price=" + price + "]";
	}

}
