package model.bill;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MBillTable extends AbstractTableModel {
	private String colTitle[] = new String[] { "Bill ID", "Employee ID", "Room ID", "Date", "Price" };
	private ArrayList<Bill> listBill;

	public MBillTable() {
		listBill = new ArrayList<>();
	}

	public void setData(ArrayList<Bill> listBill) {
		this.listBill = listBill;
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return colTitle[column];
	}

	@Override
	public int getRowCount() {
		return listBill.size();
	}

	@Override
	public int getColumnCount() {
		return colTitle.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Bill bill = listBill.get(rowIndex);
		switch (columnIndex) {
		case 0: {

			return bill.getBillID();
		}
		case 1: {

			return bill.getEmployeeID();
		}

		case 2: {

			return bill.getRoomID();
		}
		case 3: {

			return bill.getDate();
		}
		case 4: {

			return bill.getPrice();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}

}
