package model;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;

import controllers.CBillForm;
import controllers.CCustomerForm;
import controllers.CEmployeeForm;
import controllers.CRoomForm;
import controllers.CSeviceForm;
import view.BillForm;
import view.CustomerForm;
import view.EmployeeForm;
import view.RoomForm;
import view.ServiceForm;

public class MMenuForm {
	public void jButtonEmployeeActionPerformed(ActionEvent evt) {
		EmployeeForm nvf = new EmployeeForm();
		new CEmployeeForm(nvf);
		nvf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void jButtonCustomerActionPerformed(ActionEvent evt) {

		CustomerForm khf = new CustomerForm();
		new CCustomerForm(khf);
		khf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	public void jButtonRoomActionPerformed(ActionEvent evt) {
		RoomForm pf = new RoomForm();
		new CRoomForm(pf);
		pf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	public void jButtonBillActionPerformed(ActionEvent evt) {
		BillForm billForm = new BillForm();
		new CBillForm(billForm);
		billForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void jButtonServiceActionPerformed(ActionEvent evt) {
		ServiceForm serviceForm = new ServiceForm();
		new CSeviceForm(serviceForm);
		serviceForm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
