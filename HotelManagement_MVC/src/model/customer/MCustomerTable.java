package model.customer;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MCustomerTable extends AbstractTableModel {
	private String colTitle[] = new String[] { "MAKH", "TENKH", "CMND","QUOCTICH","GIOITINH","TUOI","SDT","MAPHONG"};
	private ArrayList<Customer> listCustomer;

	public MCustomerTable() {
		listCustomer = new ArrayList<>();
	}

	public void setData(ArrayList<Customer> listCustomer) {
		this.listCustomer = listCustomer;
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return colTitle[column];
	}

	@Override
	public int getRowCount() {
		return listCustomer.size();
	}

	@Override
	public int getColumnCount() {
		return colTitle.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Customer service = listCustomer.get(rowIndex);
		switch (columnIndex) {
		case 0: {

			return service.getCustomerID();
		}
		case 1: {

			return service.getName();
		}

		case 2: {

			return service.getCitizenID();
		}case 3: {

			return service.getNationality();
		}case 4: {

			return service.getSex();
		}case 5: {

			return service.getAge();
		}case 6: {

			return service.getTel();
		}case 7: {

			return service.getRoomID();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}

}
