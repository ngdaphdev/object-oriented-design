package model.customer;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import model.MyConnection;

public class MCustomerForm {
	Connection con = null;
	Statement st;
	private MyConnection myConn;
	private boolean flag = false;

	public MCustomerForm() {
		myConn = new MyConnection();
	}

	public ArrayList<Customer> getListCustomer() {
		ArrayList<Customer> dskh = new ArrayList<Customer>();
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM khachhang";
			ResultSet rs = st.executeQuery(sql);

			Customer kh;
			while (rs.next()) {
				kh = new Customer(rs.getString("MAKH"), rs.getString("TENKH"), rs.getString("CMND"),
						rs.getString("QUOCTICH"), rs.getString("GIOITINH"), rs.getInt("TUOI"), rs.getString("SDT"),
						rs.getString("MAPHONG"));

				dskh.add(kh);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dskh;
	}

	// Add
	public void addActionPerformed(ActionEvent evt, JTextField jTextFieldCustomerID, JTextField jTextFieldName,
			JTextField jTextFieldCitizenID, JTextField jTextFieldNationality, JTextField jTextFieldSex,
			JTextField jTextFieldAge, JTextField jTextFieldRoomID, JTextField jTextFieldTel) {

		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "INSERT INTO khachhang(MAKH,TENKH, CMND, QUOCTICH, GIOITINH, TUOI, SDT, MAPHONG) VALUES('"
					+ jTextFieldCustomerID.getText() + "'," + "'" + jTextFieldName.getText() + "','"
					+ jTextFieldCitizenID.getText() + "', '" + jTextFieldNationality.getText() + "', '"
					+ jTextFieldSex.getText() + "', '" + jTextFieldAge.getText() + "', '" + jTextFieldTel.getText()
					+ "', '" + jTextFieldRoomID.getText() + "')";

			st.execute(query);
			flag = true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Edit
	public void editActionPerformed(ActionEvent evt, JTable jTableCustomer, JTextField jTextFieldCustomerID,
			JTextField jTextFieldName, JTextField jTextFieldCitizenID, JTextField jTextFieldNationality,
			JTextField jTextFieldSex, JTextField jTextFieldAge, JTextField jTextFieldRoomID, JTextField jTextFieldTel) {

		Connection con = myConn.getConnection();

		if (jTableCustomer.getSelectedRow() == -1) {
			flag = false;
		} else {
			try {
				st = (Statement) con.createStatement();
				String sql = "UPDATE KHACHHANG SET MAKH=?,TENKH=?, CMND=?, QUOCTICH=?, GIOITINH=?, TUOI=?, SDT=?, MAPHONG=? WHERE  MAKH=?";

				PreparedStatement insertStatement = con.prepareStatement(sql);
				insertStatement.setString(1, jTextFieldCustomerID.getText());
				insertStatement.setString(2, jTextFieldName.getText());
				insertStatement.setString(3, jTextFieldCitizenID.getText());
				insertStatement.setString(4, jTextFieldNationality.getText());
				insertStatement.setString(5, jTextFieldSex.getText());
				insertStatement.setString(6, jTextFieldAge.getText());
				insertStatement.setString(7, jTextFieldRoomID.getText());
				insertStatement.setString(8, jTextFieldTel.getText());
				insertStatement.setString(9, jTextFieldCustomerID.getText());

				insertStatement.executeUpdate();
				insertStatement.close();

				jTableCustomer.setValueAt(jTextFieldCustomerID.getText(), jTableCustomer.getSelectedRow(), 0);
				jTableCustomer.setValueAt(jTextFieldName.getText().toString(), jTableCustomer.getSelectedRow(), 1);
				jTableCustomer.setValueAt(jTextFieldCitizenID.getText(), jTableCustomer.getSelectedRow(), 2);
				jTableCustomer.setValueAt(jTextFieldNationality.getText(), jTableCustomer.getSelectedRow(), 3);
				jTableCustomer.setValueAt(jTextFieldSex.getText(), jTableCustomer.getSelectedRow(), 4);
				jTableCustomer.setValueAt(jTextFieldAge.getText(), jTableCustomer.getSelectedRow(), 5);
				jTableCustomer.setValueAt(jTextFieldTel.getText(), jTableCustomer.getSelectedRow(), 6);
				jTableCustomer.setValueAt(jTextFieldRoomID.getText(), jTableCustomer.getSelectedRow(), 7);
				flag = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	// Delete
	public void deleteActionPerformed(ActionEvent evt, JTextField jTextFieldCustomerID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "DELETE FROM khachhang WHERE MAKH = '" + jTextFieldCustomerID.getText() + "'";
			st.executeUpdate(query);
			flag = true;

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Clear
	public void jButtonclearActionPerformed(ActionEvent evt, JTextField jTextFieldCustomerID, JTextField jTextFieldName,
			JTextField jTextFieldCitizenID, JTextField jTextFieldNationality, JTextField jTextFieldSex,
			JTextField jTextFieldAge, JTextField jTextFieldRoomID, JTextField jTextFieldTel) {
		jTextFieldCustomerID.setText("");
		jTextFieldName.setText("");
		jTextFieldCitizenID.setText("");
		jTextFieldNationality.setText("");
		jTextFieldSex.setText("");
		jTextFieldAge.setText("");
		jTextFieldTel.setText("");
		jTextFieldRoomID.setText("");
		jTextFieldCustomerID.requestFocus();

	}

	public void jTableCustomerMouseClicked(MouseEvent evt, JTextField jTextFieldCustomerID, JTextField jTextFieldName,
			JTextField jTextFieldCitizenID, JTextField jTextFieldNationality, JTextField jTextFieldSex,
			JTextField jTextFieldAge, JTextField jTextFieldRoomID, JTextField jTextFieldTel, JTable jTableCustomer) {
		int i = jTableCustomer.getSelectedRow();
		TableModel model = jTableCustomer.getModel();
		jTextFieldCustomerID.setText(model.getValueAt(i, 0).toString());
		jTextFieldName.setText(model.getValueAt(i, 1).toString());
		jTextFieldCitizenID.setText(model.getValueAt(i, 2).toString());
		jTextFieldNationality.setText(model.getValueAt(i, 3).toString());
		jTextFieldSex.setText(model.getValueAt(i, 4).toString());
		jTextFieldAge.setText(model.getValueAt(i, 5).toString());
		jTextFieldTel.setText(model.getValueAt(i, 6).toString());
		jTextFieldRoomID.setText(model.getValueAt(i, 7).toString());
	}

	public boolean isFlag() {
		return flag;
	}

}
