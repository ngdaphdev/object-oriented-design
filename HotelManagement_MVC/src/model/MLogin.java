package model;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controllers.CMenuForm;
import view.MenuForm;

public class MLogin {	
	public void jButtonActionPerformed(ActionEvent evt,JFrame login, JTextField txtUser,JTextField password, MenuForm ql) {
		if (txtUser.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please login your account");
		} else if (password.getText().equals("")) {
			JOptionPane.showMessageDialog(login, "Please login password");
		} else if (txtUser.getText().equals("admin")) {
			if (password.getText().equals("admin")) {
				JOptionPane.showMessageDialog(login, "Logged in successfully");
				ql = new MenuForm();
				new CMenuForm(ql);
			} else {
				JOptionPane.showMessageDialog(login, "Wrong password!!!");
			}
		} else {
			JOptionPane.showMessageDialog(login, "Login failed");
		}
	}
	
}
