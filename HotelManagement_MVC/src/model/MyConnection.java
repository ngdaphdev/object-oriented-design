package model;

import java.sql.DriverManager;
import java.sql.Connection;

public class MyConnection {
	public Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			String url = "jdbc:sqlserver://localhost:1433;databaseName=QL_KHACHSAN;integratedSecurity=true;";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String username = "sa";
			String password = "sa";
			conn = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Can't Connect go to database");

		}
		return conn;
	}

	public void closeConnection(java.sql.Connection c) {
		try {
			c.close();
		} catch (Exception e) {
			System.out.println("Can't close go to database");
		}
	}
}
