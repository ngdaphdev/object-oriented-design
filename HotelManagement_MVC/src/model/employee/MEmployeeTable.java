package model.employee;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MEmployeeTable extends AbstractTableModel {
	private String colTitle[] = new String[] { "MANV", "TENNV", "CHUCVU","LUONGNV","NGAYSINH","GIOITINH","CHUTHICH"};
	private ArrayList<Employee> listEmployee;

	public MEmployeeTable() {
		listEmployee = new ArrayList<>();
	}

	public void setData(ArrayList<Employee> listEmployee) {
		this.listEmployee = listEmployee;
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return colTitle[column];
	}

	@Override
	public int getRowCount() {
		return listEmployee.size();
	}

	@Override
	public int getColumnCount() {
		return colTitle.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Employee service = listEmployee.get(rowIndex);
		switch (columnIndex) {
		case 0: {

			return service.getEmployeeID();
		}
		case 1: {

			return service.getEmployeeName();
		}

		case 2: {

			return service.getJobTitle();
		}case 3: {

			return service.getSalary();
		}case 4: {

			return service.getBirthday();
		}case 5: {

			return service.getSex();
		}case 6: {

			return service.getNote();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}

}
