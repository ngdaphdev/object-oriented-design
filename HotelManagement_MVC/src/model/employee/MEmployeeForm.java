package model.employee;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.MyConnection;

public class MEmployeeForm {
	Connection con = null;
	Statement st;
	private MyConnection myConn;
	private boolean flag = false;

	public MEmployeeForm() {
		myConn = new MyConnection();
	}

	public ArrayList<Employee> getListEmployee() {
		ArrayList<Employee> dsnv = new ArrayList<Employee>();
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM nhanvien";
			ResultSet rs = st.executeQuery(sql);

			Employee nv;
			while (rs.next()) {
				nv = new Employee(rs.getString("MANV"), rs.getString("TENNV"), rs.getString("CHUCVU"),
						rs.getDouble("LUONGNV"), rs.getDate("NGAYSINH"), rs.getString("GIOITINH"),
						rs.getString("CHUTHICH"));

				dsnv.add(nv);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsnv;
	}

//	table
	public void jTableEmployeeMouseClicked(MouseEvent evt, JTextField jTextFieldEmployeeID,
			JTextField jTextFieldEmployeeName, JTextField jTextFieldJobTitle, JTextField jTextFieldSalary,
			JTextField jTextFieldBirthday, JTextField jTextFieldSex, JTextField jTextFieldNote, JTable jTableEmployee) {
		int i = jTableEmployee.getSelectedRow();
		TableModel model = jTableEmployee.getModel();
		jTextFieldEmployeeID.setText(model.getValueAt(i, 0).toString());
		jTextFieldEmployeeName.setText(model.getValueAt(i, 1).toString());
		jTextFieldJobTitle.setText(model.getValueAt(i, 2).toString());
		jTextFieldSalary.setText(model.getValueAt(i, 3).toString());
		jTextFieldBirthday.setText(model.getValueAt(i, 4).toString());
		jTextFieldSex.setText(model.getValueAt(i, 5).toString());
		jTextFieldNote.setText(model.getValueAt(i, 6).toString());
	}

	// Clear
	public void jButtonclearActionPerformed(ActionEvent evt, JTextField jTextFieldEmployeeID,
			JTextField jTextFieldEmployeeName, JTextField jTextFieldJobTitle, JTextField jTextFieldSalary,
			JTextField jTextFieldBirthday, JTextField jTextFieldSex, JTextField jTextFieldNote) {
		jTextFieldEmployeeID.setText("");
		jTextFieldEmployeeName.setText("");
		jTextFieldJobTitle.setText("");
		jTextFieldSalary.setText("");
		jTextFieldBirthday.setText("");
		jTextFieldSex.setText("");
		jTextFieldNote.setText("");
		jTextFieldEmployeeID.requestFocus();
	}

	// Delete
	public void deleteActionPerformed(ActionEvent evt, JTextField jTextFieldEmployeeID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "DELETE FROM NhanVien WHERE MANV = '" + jTextFieldEmployeeID.getText() + "'";
			st.executeUpdate(query);
			flag = true;

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Edit
	public void editActionPerformed(ActionEvent evt, JTable jTableEmployee, JTextField jTextFieldEmployeeID,
			JTextField jTextFieldEmployeeName, JTextField jTextFieldJobTitle, JTextField jTextFieldSalary,
			JTextField jTextFieldBirthday, JTextField jTextFieldSex, JTextField jTextFieldNote) {
		Connection con = myConn.getConnection();

		if (jTableEmployee.getSelectedRow() == -1) {
			if (jTableEmployee.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			String sql = "UPDATE nhanvien SET MANV=?,TENNV=?, CHUCVU=?, LUONGNV=?, NGAYSINH=?, GIOITINH=?, CHUTHICH=?";

			PreparedStatement insertStatement;
			try {
				insertStatement = con.prepareStatement(sql);
				insertStatement.setString(1, jTextFieldEmployeeID.getText());
				insertStatement.setString(2, jTextFieldEmployeeName.getText());
				insertStatement.setString(3, jTextFieldJobTitle.getText());
				insertStatement.setString(4, jTextFieldSalary.getText());
				insertStatement.setString(5, jTextFieldBirthday.getText());
				insertStatement.setString(6, jTextFieldSex.getText());
				insertStatement.setString(7, jTextFieldNote.getText());

				insertStatement.executeUpdate();
				insertStatement.close();

				DefaultTableModel model = (DefaultTableModel) jTableEmployee.getModel();
				model.setValueAt(jTextFieldEmployeeID.getText(), jTableEmployee.getSelectedRow(), 0);
				model.setValueAt(jTextFieldEmployeeName.getText().toString(), jTableEmployee.getSelectedRow(), 1);
				model.setValueAt(jTextFieldJobTitle.getText(), jTableEmployee.getSelectedRow(), 2);
				model.setValueAt(jTextFieldSalary.getText(), jTableEmployee.getSelectedRow(), 2);
				model.setValueAt(jTextFieldBirthday.getText(), jTableEmployee.getSelectedRow(), 2);
				model.setValueAt(jTextFieldSex.getText(), jTableEmployee.getSelectedRow(), 2);
				model.setValueAt(jTextFieldNote.getText(), jTableEmployee.getSelectedRow(), 2);
				flag = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

//	add
	public void addActionPerformed(ActionEvent evt, JTextField jTextFieldEmployeeID, JTextField jTextFieldEmployeeName,
			JTextField jTextFieldJobTitle, JTextField jTextFieldSalary, JTextField jTextFieldBirthday,
			JTextField jTextFieldSex, JTextField jTextFieldNote) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "INSERT INTO NhanVien(MANV,TENNV,CHUCVU, LUONGNV, NGAYSINH, GIOITINH, CHUTHICH) VALUES('"
					+ jTextFieldEmployeeID.getText() + "'," + "'" + jTextFieldEmployeeName.getText() + "','"
					+ jTextFieldJobTitle.getText() + "','" + jTextFieldSalary.getText() + "','"
					+ jTextFieldBirthday.getText() + "','" + jTextFieldSex.getText() + "','" + jTextFieldNote.getText()
					+ "')";

			st.execute(query);
			flag = true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public boolean isFlag() {
		return flag;
	}

}
