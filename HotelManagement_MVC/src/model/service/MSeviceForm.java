package model.service;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.MyConnection;

public class MSeviceForm {
	Connection con = null;
	Statement st;
	private MyConnection myConn;
	private boolean flag = false;
	
	public MSeviceForm() {
		myConn = new MyConnection();
	}

	public ArrayList<Service> getListService() {
		ArrayList<Service> dsdv = new ArrayList<Service>();
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM dichvu";
			ResultSet rs = st.executeQuery(sql);

			Service dv;
			while (rs.next()) {
				dv = new Service(rs.getString("MADV"), rs.getString("TENDV"), rs.getDouble("GIADV"));

				dsdv.add(dv);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsdv;
	}

//	 Add
	public void addActionPerformed(ActionEvent evt, JTextField jTextFieldServiceID, JTextField jTextFieldServiceName,
			JTextField jTextFieldServicePrice) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "INSERT INTO DichVu(MADV,TENDV, GIADV) VALUES('" + jTextFieldServiceID.getText() + "'," + "'"
					+ jTextFieldServiceName.getText() + "','" + jTextFieldServicePrice.getText() + "')";

			st.execute(query);
			flag = true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Edit
	public void editActionPerformed(ActionEvent evt, JTable jTableDichvu,JTextField jTextFieldServiceID,
			JTextField jTextFieldServiceName, JTextField jTextFieldServicePrice) {
		if (jTableDichvu.getSelectedRow() == -1) {
			flag = false;
		} else {
			String sql = "UPDATE dichvu SET MADV=?,TENDV=?, GIADV=?";
			Connection con = myConn.getConnection();

			PreparedStatement insertStatement;
			
			try {
				insertStatement = con.prepareStatement(sql);
				insertStatement.setString(1, jTextFieldServiceID.getText());
				insertStatement.setString(2, jTextFieldServiceName.getText());
				insertStatement.setString(3, jTextFieldServicePrice.getText());


				insertStatement.executeUpdate();
				insertStatement.close();
				
				DefaultTableModel model = (DefaultTableModel) jTableDichvu.getModel();
				model.setValueAt(jTextFieldServiceID.getText(), jTableDichvu.getSelectedRow(), 0);
				model.setValueAt(jTextFieldServiceName.getText().toString(), jTableDichvu.getSelectedRow(), 1);
				model.setValueAt(jTextFieldServicePrice.getText(), jTableDichvu.getSelectedRow(), 2);
				flag = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

	// Delete
	public void deleteActionPerformed(ActionEvent evt, JTextField jTextFieldServiceID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "DELETE FROM dichvu WHERE MADV = '" + jTextFieldServiceID.getText() + "'";
			st.executeUpdate(query);
			flag = true;

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	// Clear
	public void jButtonclearActionPerformed(ActionEvent evt,  JTextField jTextFieldServiceID,
			JTextField jTextFieldServiceName, JTextField jTextFieldServicePrice) {
		jTextFieldServiceID.setText("");
		jTextFieldServiceName.setText("");
		jTextFieldServicePrice.setText("");
		jTextFieldServiceID.requestFocus();
	}

	public void jTableServiceMouseClicked(MouseEvent evt,JTable jTableDichvu, JTextField jTextFieldServiceID,
			JTextField jTextFieldServiceName, JTextField jTextFieldServicePrice) {
		int i = jTableDichvu.getSelectedRow();
		TableModel model = jTableDichvu.getModel();
		jTextFieldServiceID.setText(model.getValueAt(i, 0).toString());
		jTextFieldServiceName.setText(model.getValueAt(i, 1).toString());
		jTextFieldServicePrice.setText(model.getValueAt(i, 2).toString());

	}

	public boolean isFlag() {
		return flag;
	}

}
