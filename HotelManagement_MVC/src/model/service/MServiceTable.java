package model.service;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MServiceTable extends AbstractTableModel {
	private String colTitle[] = new String[] { "MADV", "TENDV", "GIADV"};
	private ArrayList<Service> listService;

	public MServiceTable() {
		listService = new ArrayList<>();
	}

	public void setData(ArrayList<Service> listService) {
		this.listService = listService;
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return colTitle[column];
	}

	@Override
	public int getRowCount() {
		return listService.size();
	}

	@Override
	public int getColumnCount() {
		return colTitle.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Service service = listService.get(rowIndex);
		switch (columnIndex) {
		case 0: {

			return service.getServiceID();
		}
		case 1: {

			return service.getServiceName();
		}

		case 2: {

			return service.getServicePrice();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}

}
