package model.room;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MRoomTable extends AbstractTableModel {
	private String colTitle[] = new String[] { "MAPHONG", "TENPHONG", "LOAIPHONG","GIAPHONG","CHUTHICH","TINHTRANG","MANV","MADV"};
	private ArrayList<Room> listRoom;

	public MRoomTable() {
		listRoom = new ArrayList<>();
	}

	public void setData(ArrayList<Room> listRoom) {
		this.listRoom = listRoom;
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column) {
		return colTitle[column];
	}

	@Override
	public int getRowCount() {
		return listRoom.size();
	}

	@Override
	public int getColumnCount() {
		return colTitle.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Room service = listRoom.get(rowIndex);
		switch (columnIndex) {
		case 0: {

			return service.getRoomID();
		}
		case 1: {

			return service.getRoomName();
		}

		case 2: {

			return service.getKindOfRoom();
		}case 3: {

			return service.getPrice();
		}case 4: {

			return service.getNote();
		}case 5: {

			return service.getCondition();
		}case 6: {

			return service.getEmployeeID();
		}case 7: {

			return service.getServiceID();
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + columnIndex);
		}
	}

}
