package model.room;

public class Room {
	private String roomID;
	private String roomName;
	private String kindOfRoom;
	private double price;
	private String note;
	private String condition;
	private String employeeID;
	private String serviceID;

	public Room(String roomID, String roomName, String kindOfRoom, double price, String note, String condition,
			String employeeID, String serviceID) {
		super();
		this.roomID = roomID;
		this.roomName = roomName;
		this.kindOfRoom = kindOfRoom;
		this.price = price;
		this.note = note;
		this.condition = condition;
		this.employeeID = employeeID;
		this.serviceID = serviceID;
	}

	public Room() {
	}

	Room(String string, String string0, String string1, double aDouble, String string2, String string3) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getKindOfRoom() {
		return kindOfRoom;
	}

	public void setKindOfRoom(String kindOfRoom) {
		this.kindOfRoom = kindOfRoom;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	@Override
	public String toString() {
		return "Room [roomID=" + roomID + ", roomName=" + roomName + ", kindOfRoom=" + kindOfRoom + ", price=" + price
				+ ", note=" + note + ", condition=" + condition + ", employeeID=" + employeeID + ", serviceID="
				+ serviceID + "]";
	}

}
