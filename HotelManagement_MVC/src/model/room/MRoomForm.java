package model.room;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.MyConnection;

public class MRoomForm {
	Connection con = null;
	Statement st;
	private MyConnection myConn;
	private boolean flag = false;

	public MRoomForm() {
		myConn = new MyConnection();
	}

	public ArrayList<Room> getListRoom() {
		ArrayList<Room> dsp = new ArrayList<Room>();
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String sql = "SELECT * FROM phong";
			ResultSet rs = st.executeQuery(sql);

			Room p;
			while (rs.next()) {
				p = new Room(rs.getString("MAPHONG"), rs.getString("TENPHONG"), rs.getString("LOAIPHONG"),
						rs.getDouble("GIAPHONG"), rs.getString("CHUTHICH"), rs.getString("TINHTRANG"),
						rs.getString("MANV"), rs.getString("MADV"));

				dsp.add(p);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return dsp;
	}

//	table
	public void jTableRoomMouseClicked(MouseEvent evt, JTextField jTextFieldRoomID, JTextField jTextFieldRoomName,
			JTextField jTextFieldKindOfRoom, JTextField jTextFieldPrice, JTextField jTextFieldRoom,
			JTextField jTextFieldCondition, JTextField jTextFieldEmployeeID, JTextField jTextFieldServiceID,
			JTable jTableRoom) {
		int i = jTableRoom.getSelectedRow();
		TableModel model = jTableRoom.getModel();
		jTextFieldRoomID.setText(model.getValueAt(i, 0).toString());
		jTextFieldRoomName.setText(model.getValueAt(i, 1).toString());
		jTextFieldKindOfRoom.setText(model.getValueAt(i, 2).toString());
		jTextFieldPrice.setText(model.getValueAt(i, 3).toString());
		jTextFieldRoom.setText(model.getValueAt(i, 4).toString());
		jTextFieldCondition.setText(model.getValueAt(i, 5).toString());
		jTextFieldEmployeeID.setText(model.getValueAt(i, 6).toString());
		jTextFieldServiceID.setText(model.getValueAt(i, 7).toString());
	}

	// Add
	public void addActionPerformed(ActionEvent evt, JTextField jTextFieldRoomID, JTextField jTextFieldRoomName,
			JTextField jTextFieldKindOfRoom, JTextField jTextFieldPrice, JTextField jTextFieldRoom,
			JTextField jTextFieldCondition, JTextField jTextFieldEmployeeID, JTextField jTextFieldServiceID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "INSERT INTO phong(MAPHONG,TENPHONG, LOAIPHONG, GIAPHONG, CHUTHICH, TINHTRANG, MANV, MADV) VALUES('"
					+ jTextFieldRoomID.getText() + "'," + "'" + jTextFieldRoomName.getText() + "','"
					+ jTextFieldKindOfRoom.getText() + "', '" + jTextFieldPrice.getText() + "', '"
					+ jTextFieldRoom.getText() + "', '" + jTextFieldCondition.getText() + "', '"
					+ jTextFieldEmployeeID.getText() + "', '" + jTextFieldServiceID.getText() + "')";

			st.execute(query);
			flag = true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Edit
	public void editActionPerformed(ActionEvent evt, JTextField jTextFieldRoomID, JTextField jTextFieldRoomName,
			JTextField jTextFieldKindOfRoom, JTextField jTextFieldPrice, JTextField jTextFieldRoom,
			JTextField jTextFieldCondition, JTextField jTextFieldEmployeeID, JTextField jTextFieldServiceID,
			JTable jTableRoom) {
		if (jTableRoom.getSelectedRow() == -1) {
			if (jTableRoom.getRowCount() == 0) {
				// lblError.setText("Table is empty");
			} else {
				// lblError.setText("You must select a Tennis Player");
			}
		} else {
			String sql = "UPDATE phong SET MAPHONG=?,TENPHONG=?, LOAIPHONG=?, GIAPHONG=?, CHUTHICH=?, TINHTRANG=?, MANV=?, MADV=?";
			Connection con = myConn.getConnection();

			PreparedStatement insertStatement;
			try {
				insertStatement = con.prepareStatement(sql);
				insertStatement.setString(1, jTextFieldEmployeeID.getText());
				insertStatement.setString(2, jTextFieldRoomName.getText());
				insertStatement.setString(3, jTextFieldKindOfRoom.getText());
				insertStatement.setString(4, jTextFieldPrice.getText());
				insertStatement.setString(5, jTextFieldRoom.getText());
				insertStatement.setString(6, jTextFieldCondition.getText());
				insertStatement.setString(7, jTextFieldEmployeeID.getText());
				insertStatement.setString(8, jTextFieldServiceID.getText());


				insertStatement.executeUpdate();
				insertStatement.close();
				
				DefaultTableModel model = (DefaultTableModel) jTableRoom.getModel();
				model.setValueAt(jTextFieldRoomID.getText(), jTableRoom.getSelectedRow(), 0);
				model.setValueAt(jTextFieldRoomName.getText().toString(), jTableRoom.getSelectedRow(), 1);
				model.setValueAt(jTextFieldKindOfRoom.getText(), jTableRoom.getSelectedRow(), 2);
				model.setValueAt(jTextFieldPrice.getText(), jTableRoom.getSelectedRow(), 3);
				model.setValueAt(jTextFieldRoom.getText(), jTableRoom.getSelectedRow(), 4);
				model.setValueAt(jTextFieldCondition.getText(), jTableRoom.getSelectedRow(), 5);
				model.setValueAt(jTextFieldEmployeeID.getText(), jTableRoom.getSelectedRow(), 6);
				model.setValueAt(jTextFieldServiceID.getText(), jTableRoom.getSelectedRow(), 7);
				flag = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
	}

	// Delete
	public void deleteActionPerformed(ActionEvent evt, JTextField jTextFieldRoomID) {
		Connection con = myConn.getConnection();
		try {
			st = (Statement) con.createStatement();
			String query = "DELETE FROM phong WHERE MAPHONG = '" + jTextFieldRoomID.getText() + "'";
			st.executeUpdate(query);
			flag = true;

		} catch (Exception ex) {

			ex.printStackTrace();
		}

	}

	// Clear
	public void jButtonclearActionPerformed(ActionEvent evt, JTextField jTextFieldRoomID, JTextField jTextFieldRoomName,
			JTextField jTextFieldKindOfRoom, JTextField jTextFieldPrice, JTextField jTextFieldRoom,
			JTextField jTextFieldCondition, JTextField jTextFieldEmployeeID, JTextField jTextFieldServiceID) {
		jTextFieldRoomID.setText("");
		jTextFieldRoomName.setText("");
		jTextFieldKindOfRoom.setText("");
		jTextFieldPrice.setText("");
		jTextFieldRoom.setText("");
		jTextFieldCondition.setText("");
		jTextFieldEmployeeID.setText("");
		jTextFieldServiceID.setText("");
		jTextFieldRoomID.requestFocus();

	}

	public boolean isFlag() {
		return flag;
	}

}
