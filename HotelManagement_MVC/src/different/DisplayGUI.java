package different;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class DisplayGUI {
	private String title;
	public DisplayGUI (String title) {
		this.title = title;
	}
	
	public void display(JFrame frame,JPanel jPanel) {
		frame.setTitle(title);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setResizable(false);
	}

}
